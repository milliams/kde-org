---
aliases:
- ../../kde-frameworks-5.63.0
date: 2019-10-14
layout: framework
libCount: 70
---

### Breeze Icons

- Improve KFloppy icon (bug 412404)
- Add format-text-underline-squiggle actions icons (bug 408283)
- Add colorful preferences-desktop-filter icon (bug 406900)
- Add app icon for the Kirogi Drone control app
- Added scripts to create a webfont out of all breeze action icons
- Add enablefont and disablefont icon for kfontinst KCM
- Fix large system-reboot icons rotating in an inconsistent direction (bug 411671)

### Extra CMake Modules

- new module ECMSourceVersionControl
- Fix FindEGL when using Emscripten
- ECMAddQch: add INCLUDE_DIRS argument

### Framework Integration

- ensure winId() not called on non-native widgets (bug 412675)

### kcalendarcore

New module, previously known as kcalcore in kdepim

### KCMUtils

- Suppress mouse events in KCMs causing window moves
- adjust margins of KCMultiDialog (bug 411161)

### KCompletion

- [KComboBox] Properly disable Qt's builtin completer [regression fix]

### KConfig

- Fix generating properties that start with an uppercase letter

### KConfigWidgets

- Make KColorScheme compatible with QVariant

### kcontacts

New module, previously part of KDE PIM

### KCoreAddons

- Add KListOpenFilesJob

### KDeclarative

- Delete QQmlObjectSharedEngine context in sync with QQmlObject
- [KDeclarative] Port from deprecated QWheelEvent::delta() to angleDelta()

### KDELibs 4 Support

- Support NetworkManager 1.20 and do actually compile the NM backend

### KIconThemes

- Deprecate the global [Small|Desktop|Bar]Icon() methods

### KImageFormats

- Add files for testing bug411327
- xcf: Fix regression when reading files with "unsupported" properties
- xcf: Properly read image resolution
- Port HDR (Radiance RGBE) image loader to Qt5

### KIO

- [Places panel] Revamp the Recently Saved section
- [DataProtocol] compile without implicit conversion from ascii
- Consider the usage of WebDAV methods sufficient for assuming WebDAV
- REPORT also supports the Depth header
- Make QSslError::SslError &lt;-&gt; KSslError::Error conversion reusable
- Deprecate the KSslError::Error ctor of KSslError
- [Windows] fix listing the parent dir of C:foo, that's C: and not C:
- Fix crash on exit in kio_file (bug 408797)
- Add == and != operators to KIO::UDSEntry
- Replace KSslError::errorString with QSslError::errorString
- Move/copy job: skip stat'ing sources if the destination dir isn't writable (bug 141564)
- Fixed interaction with DOS/Windows executables in KRun::runUrl
- [KUrlNavigatorPlacesSelector] Properly identify teardown action (bug 403454)
- KCoreDirLister: fix crash when creating new folders from kfilewidget (bug 401916)
- [kpropertiesdialog] add icons for the size section
- Add icons for "Open With" and "Actions" menus
- Avoid initializing an unnecessary variable
- Move more functionality from KRun::runCommand/runApplication to KProcessRunner
- [Advanced Permissions] Fix icon names (bug 411915)
- [KUrlNavigatorButton] Fix QString usage to not use [] out of bounds
- Make KSslError hold a QSslError internally
- Split KSslErrorUiData from KTcpSocket
- Port kpac from QtScript

### Kirigami

- always cache just the last item
- more z (bug 411832)
- fix import version in PagePoolAction
- PagePool is Kirigami 2.11
- take into account dragging speed when a flick ends
- Fix copying urls to the clipboard
- check more if we are reparenting an actual Item
- basic support for ListItem actions
- introduce cachePages
- fix compatibility with Qt5.11
- introduce PagePoolAction
- new class: PagePool to manage recycling of pages after they're popped
- make tabbars look better
- some margin on the right (bug 409630)
- Revert "Compensate smaller icon sizes on mobile in the ActionButton"
- don't make list items look inactive (bug 408191)
- Revert "Remove scaling of iconsize unit for isMobile"
- Layout.fillWidth should be done by the client (bug 411188)
- Add template for Kirigami application development
- Add a mode to center actions and omit the title when using a ToolBar style (bug 402948)
- Compensate smaller icon sizes on mobile in the ActionButton
- Fixed some undefined properties runtime errors
- Fix ListSectionHeader background color for some color schemes
- Remove custom content item from ActionMenu separator

### KItemViews

- [KItemViews] Port to non-deprecated QWheelEvent API

### KJobWidgets

- cleanup dbus related objects early enough to avoid hang on program exit

### KJS

- Added startsWith(), endsWith() and includes() JS String functions
- Fixed Date.prototype.toJSON() called on non-Date objects

### KNewStuff

- Bring KNewStuffQuick to feature parity with KNewStuff(Widgets)

### KPeople

- Claim Android as a supported platform
- Deploy default avatar via qrc
- Bundle plugin files on Android
- Disable DBus pieces on Android
- Fix crash when monitoring a contact that gets removed on PersonData (bug 410746)
- Use fully qualified types on signals

### KRunner

- Consider UNC paths as NetworkShare context

### KService

- Move Amusement to Games directory instead of Games &gt; Toys (bug 412553)
- [KService] Add copy constructor
- [KService] add workingDirectory(), deprecate path()

### KTextEditor

- try to avoid artifacts in text preview
- Variable expansion: Use std::function internally
- QRectF instead of QRect solves clipping issues, (bug 390451)
- next rendering artifact goes away if you adjust the clip rect a bit (bug 390451)
- avoid the font choosing magic and turn of anti aliasing (bug 390451)
- KadeModeMenuList: fix memory leaks and others
- try to scan for usable fonts, works reasonable well if you use no dumb scaling factor like 1.1
- Status bar mode menu: Reuse empty QIcon that is implicitly shared
- Expose KTextEditor::MainWindow::showPluginConfigPage()
- Replace QSignalMapper with lambda
- KateModeMenuList: use QString() for empty strings
- KateModeMenuList: add "Best Search Matches" section and fixes for Windows
- Variable expansion: Support QTextEdits
- Add keyboard shortcut for switching Input modes to edit menu (bug 400486)
- Variable expansion dialog: properly handle selection changes and item activation
- Variable expansion dialog: add filter line edit
- Backup on save: Support time and date string replacements (bug 403583)
- Variable expansion: Prefer return value over return argument
- Initial start of variables dialog
- use new format API

### KWallet Framework

- HiDPI support

### KWayland

- Sort files alphabetically in cmake list

### KWidgetsAddons

- Make OK button configurable in KMessageBox::sorry/detailedSorry
- [KCollapsibleGroupBox] Fix QTimeLine::start warning at runtime
- Improve naming of KTitleWidget icon methods
- Add QIcon setters for the password dialogs
- [KWidgetAddons] port to non-deprecated Qt API

### KWindowSystem

- Set XCB to required if building the X backend
- Make less use of deprecated enum alias NET::StaysOnTop

### KXMLGUI

- Move "Full Screen Mode" item from Settings menu to View menu (bug 106807)

### NetworkManagerQt

- ActiveConnection: connect stateChanged() signal to correct interface

### Plasma Framework

- Export Plasma core lib log category, add a category to a qWarning
- [pluginloader] Use categorized logging
- make editMode a corona global property
- Honor global animation speed factor
- properly install whole plasmacomponent3
- [Dialog] Apply window type after changing flags
- Change reveal password button logic
- Fix crash on teardown with Applet's ConfigLoader (bug 411221)

### QQC2StyleBridge

- Fix several build system errors
- take margins from qstyle
- [Tab] Fix sizing (bug 409390)

### Syntax Highlighting

- Add syntax highlighting for RenPy (.rpy) (bug 381547)
- WordDetect rule: detect delimiters at the inner edge of the string
- Highlight GeoJSON files as if they were plain JSON
- Add syntax highlighting for SubRip Text (SRT) Subtitles
- Fix skipOffset with dynamic RegExpr (bug 399388)
- bitbake: handle embedded shell and python
- Jam: fix identifier in a SubRule
- Add syntax definition for Perl6 (bug 392468)
- support .inl extension for C++, not used by other xml files at the moment (bug 411921)
- support *.rej for diff highlighting (bug 411857)

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
