---
qtversion: 
date: 2021-12-11
layout: framework
libCount: 83
draft: true
---


### BluezQt

* Add functions that allow setting the discovery filter based on Bluez Adapter APIs

### Breeze Icons

* Install dark icons via execute_process to use copy command (bug 445489)
* Link application-menu -> open-menu-symbolic
* Add icon for Virtual Box disk images
* fix keyboard-caps-locked icon use now correct color
* BUG 110673 media-write icon fix
* BUG 412029 add preferences-desktop-feedback icon
* BUG 423937 add edit-copy-path icon for dolphin and konsole
* Add RTL half rating star
* BUG: 414215 add tabs colorful icon (bug 414215 add tabs colorful icon)
* BUG: 414213 add preferences-scroll icon (bug 414213 add preferences-scroll icon)
* BUG: 437999 add veracrypt app icon (bug 437999 add veracrypt app icon)
* Add preferences-git and preferences-document icons for kate
* BUG: 407048 add online account icon for the KCM (bug 407048 add online account icon for the KCM)
* Fix view-history icons
* Use application-certificate icons for Kleopatra's certificate groups
* Add "Invalid = -1" element to the KIconLoaderDummy::Context enum
* BUG: 409022 update disk-quota icons (bug 409022 update disk-quota icons)
* BUG: 372461 update rar mimetype icon (bug 372461 update rar mimetype icon)
* BUG: 405654 Update input-keyboard-virtual-off (bug 405654 Update input-keyboard-virtual-off)
* BUG 110673 add media-write-dvd icon
* BUG 396124 add Crow Translate app icons
* BUG 392534 add missing kmail breeze icons
* Bug 348446 update document-open icons
* Dolphin preferences dialog use colorfull icons only
* Okular sidebar use now colorful icons
* Bug: 444548 Add Gnome todo symbolic icons
* BUG 429737 update view-media-playlist icon and other playlist icons
* BUG 418864 add gajim app icon. status icons can't be used from breeze
* BUG 394023 add gpodder application icon

### Extra CMake Modules

* Include QtVersionOption before all Qt5 find_package calls
* ECMGeneratePkgConfigFile: Add an URL parameter
* KDEInstallDirs5: set up KSERVICESDIR alias for KSERVICES5DIR
* KDEInstallDirs6: comment out call to query_qmake, not defined
* KDEInstallDirs6: don't set deprecated variable names anymore
* PLUGINDIR is based upon QTPLUGINDIR, so only define it after QTPLUGINDIR
* Split KDEInstallDirs into a 5 and a 6 variant
* AddAppIcon: clarify ksvg2icns role for @2x icons
* Add support for specifying private dependencies for pkg-config files
* FindEGL add 'lib' prefix for windows

### KActivitiesStats

* Fixed the singleton access mutex locking

### KArchive

* K7Zip: use functions instead of macros

### KBookmarks

* Add source file back

### KCalendarCore

* Compare the objects managed by a QSharedPointer
* Avoid update calls if nothing changed
* Consistency and range checking for GEO data
* Check validity of priority value

### KCMUtils

* Set initial page using initialPage property, rather than pushing (bug 417636)
* Do not reset KPluginModel if there are no plugins
* Implement KPluginSelector replacement not based around KPluginInfo
* Do not try to unload static plugins
* Support static plugins for X-KDE-ConfigModule property

### KConfig

* KDesktopFile: deprecate resource()
* Use innerVarStr instead of varStr for choices
* Add a proper test for the enumms within groups
* Unbreak kconfig with enums, mutators and properties
* Demonstrate build failure with kconfig generated code
* Copy ConfigPropertyMap from KDeclarative to new KConfig QML module

### KConfigWidgets

* Add color scheme saving and loading
* Fix hamburger menu opening as a window on Wayland (bug 442375)
* [KCommandBar] Add placeholder text for empty view
* [KCommandBar] Fix rendering of chord shortcuts
* [KCommandBar] Dynamic column widths + shortcuts alignment

### KContacts

* Fix address formatting for country-only addresses
* Deprecate countryToISO/ISOToCountry in favor of KCountry
* Use local country names when formatting addresses

### KCoreAddons

* KF5CoreAddonsMacros: Replace "-" with "_" for KPLUGINFACTORY_PLUGIN_CLASS_INTERNAL_NAME compile definition
* Compile against Qt6
* Allow reading enabled state from KPluginMetaData using templated method
* KF5CoreAddonsMacros: Build static plugins as static libs
* Store KDE specific static plugin in seperate map
* Make KPluginMetaData::isStaticPlugin public
* kcoreaddons_add_plugin: Set property for file name
* Add assertion to make sure we have a d-ptr in KPluginMetaData::getStaticPlugin
* Create kcoreaddons_target_static_plugins cmake function
* Allow KPluginMetaData::findPlugins to return static plugins
* kcoreaddons_add_plugin: Allow creation of static plugins
* Allow KPluginMetaData to represent static plugins
* Create KPluginFactory::create method for loading KPart without keyword
* Deprecate KPluginFactory::create overload taking plugin keyword
* Deprecate KPluginFactory::registerPlugin(QString, CreateInstance*) methods
* Deprecate KPluginFactory::registerPlugin overloads taking keyword
* KPluginFactory: Use overload instead of default arg for registerPlugin keyword parameter
* Deprecate KPluginMetaData::serviceTypes

### KDBusAddons

* Link against X11Extras when building statically

### KDeclarative

* KeySequenceItem: Make sure we record on the correct window (bug 445277)
* Deprecate KRun KIO plugin
* Allow KCM kpackages to use metadata of C++ plugin
* Add takeLast() slot to ConfigModule
* Fix overflowing text properly

### KDELibs 4 Support

* Move Esperanto flag icon to kdelibs (bug 445077)

### KFileMetaData

* KFileMetaData::Property: Deprecate toVariantMap and toPropertyMap methods
* Port deprecated QMap methods to QMultiMap
* Put code of public header depending on deprecated Qt API in deprecation wrapper
* [PopplerExtractor] extract pageCount

### KGlobalAccel

* Make XCB XTEST optional requirement

### KDE GUI Addons

* Add KSystemClipboard to use wlr-data-control transparently in Wayland

### KI18n

* Document how KLazyLocalizedString is tied to a translation domain
* Consider TRANSLATION_DOMAIN when converting to a KLocalizedString
* KLazyLocalizedString: add KLocalizedString forward API for convenience
* KLazyLocalizedString: allow implicit default constructor
* Add KLazyLocalizedString::isEmpty() const
* Fix MSVC warning about inconsistent export macro in forward declaration
* Port Kuit keymap away from I18N_NOOP macro usage
* Deprecate the I18N_NOOP macros in favor of KLazyLocalizedString
* Add KLazyLocalizedString as a replacement for the I18N_NOOP macros
* Fix loading a KCatalog with a different language on Windows

### KIconThemes

* KIconLoader: prefer icons from current theme before falling back to other themes (bug 445804)
* [KIconDialog] Add Ctrl+F shortcut for focussing search line
* [KIconButton] Pre-select current icon when opening dialog
* [KIconDialog] Add API for pre-selecting an icon

### KImageFormats

* avif: limit scope of variables
* Add JXL to the list of supported formats
* Add plugin for JPEG XL (JXL)

### KIO

* Add new signals and logic to handle open directories in new windows or tabs
* Fix KRun::runApplication when xdg activation is involved (bug 446272)
* Explicitly remove the event filters in classes managed by a unique_ptr
* [KACLEditWidget] Load list of users/groups on demand
* [KPropertiesDialog] Reuse existing KUser instance
* Do not create thumbnails when it requires to copy the file to /tmp (bug 368104)
* Fix directory thumbnailer loading
* [CopyJob] Emit moving while renaming
* [KPropertiesDialog] Improve presentation of file type configuration
* [KPropertiesDialog] Add extra fields value in correct column
* Allow loading of Thumbnailers using embedded json metadata
* KDirOperator: get the icon sizes from user settings
* [KACLEditWidget] Remove redundancy and add icons to buttons
* [KPropertiesDialog] Use Filelight icon and name from desktop file
* [KPropertiesDialog] Hide time labels in multiple mode
* [KPropertiesDialog] Also hide label_left for timestamps
* Fix copying between different filesystems on Linux < 5.3
* [KDynamicJobTracker] Support KUiServerV2JobTracker

### Kirigami

* PageRowGlobalToolBarUI: use open-menu-symbolic
* Heading: Deprecate headerPointSize(int level) method
* Let escape key close pushed dialog layers
* AboutItem: fix the view for narrow dialogs
* AboutItem: Report its implicit size so it can scroll
* Increase passive notification opacity (bug 440390)
* Bold labels for narrow FormLayout
* Add Dialog Components
* add columns on empty width
* DefaultCardBackground improvements
* Expose internal icon source in the Avatar component
* Preconfigure kconfig and kaboutdata on template

### KJS

* Don't call functions on pointers that may not point to objects

### KNewStuff

* Improve presentation of multi-file download sheet
* Use RTL half-star rating icon when in RTL mode (bug 440650)

### KNotification

* [Tester] Support urgency for inline reply notification

### KPackage Framework

* Utilize type-safe KPluginMetaData API methods
* Utilize KPluginMetaData::value overloads

### KParts

* Remove calls to KIO::Scheduler::publishSlaveOnHold

### KRunner

* Port to KPluginMetaData::isEnabled instead of custom string magic

### KService

* KPluginInfo: Fix mimetypes that are wildcards not appearing in MimeTypes json value
* Deprecate KToolInvokation::kdeinitExecWait
* Deprecate E-Mail and terminal related methods in KToolInvocation

### KTextEditor

* Bring back git via QProcess
* When reloading, don't override auto-detected hl mode if not set by user
* If user didn't set them, don't override file type and highlighting modes detection
* Pass KTextEditor::Cursor by value everywhere
* Remove selected text if preedit is not empty
* Disable "tail-available-on-undo" in ViInputMode
* Improve built-in search performance
* Remove duplicated lua.js indent file
* Create a separate path for fetching lineLength
* Use iterators for iterating over blocks
* Completion in two edits feature describing comment
* Re-add "undo removed tail" on completion

### KUnitConversion

* [Currency] Don't complain about missing connection if we didn't download

### KWallet Framework

* MAC_USE_OSXKEYCHAIN: Link against CoreFoundation

### KWidgetsAddons

* KAcceleratorManager: Take into account actions with Alt+X shortcuts

### KWindowSystem

* Make KWindowSystem a bit easier to integrate in QML

### Plasma Framework

* wallpaperinterface: Don't double delete action (bug 446195)
* Use `open-menu-symbolic` instead of `application-menu`
* Make Breeze plasma scrollbar/slider/progressbar color match application theme
* Heading: Deprecate headerPointSize(int level) method
* Don't reload renderers for empty paths
* PageStack: Replace == equality with === strict equality
* Reload shared renderers when a file changed on disk (bug 445516)
* PlasmoidHeading: Turn location into an alias for position property
* Replace variant property type with var and other appropriate types
* org.kde.plasma.extras: Follow Qt style of implicitWidth/implicitHeight declaration
* Title: Replace implementation with Heading {}
* Title: Lower size (again)
* QQuickWindow::event after setupwaylandintegration
* Do updateTheme after QQuickWindow::event (bug 305247)
* Breeze slider.svg: Fix bad SVG code making shadow invisible
* set all roles that can be mapped from dialog types to plasma protocol role
* smaller mask to hide glitches (bug 438644)
* KF5PlasmaMacros: Actually install desktop files if they exist
* make the OSD an actual wayland OSD (bug 428859)

### QQC2StyleBridge

* Fix syntax highlighting

### Solid

* Allow udisks2 and upower backends to be disabled with environment variables
* Add device description for storage medium without size
* Config.cmake: Lookup LibMount when building statically
* Add support for graphics tablet batteries
* [upower] Consider audio devices and generic bluetooth devices to be batteries (bug 445141)
* [upower] Add new device types

### Sonnet

* don't load plugins from current working directory, that is even a security issue
* export all known languages
* massive speedup, avoid loading the same huge dictionaries several times
* only load one instance of a plugin
* core: attempt to load local plugins first, makes development easier

### Syntax Highlighting

* markdown.xml: Fix highlighting of headers (include last char) (bug 445918)
* Update orgmode.xml
* use the same way as emacs to highlight priority cookie
* support verbatim since it seems used more often than inline code
* support single line comment, babel call, priority cookie
* Update context.xml version
* Update ConTeXt test references
* Corrected syntax for ConTeXt TABLE commands (bug 445820)
* ConTeXt: Verbatim highlighting allows for nesting (bug 445818)
* Added ConTeXt test references
* Update context.xml version number
* Added highlighting for ConTeXt "\m" command
* Fixed current math command highlighting
* misc: Add `Pipfile` to create Python venv for generators
* cmake.xml: Updates for CMake 3.22
* fix: Add `Loader` parameter in call to `yaml.load()`
* update test for inline code
* support inline code and another type of number list
* support block and number list
* add orgmode.xml
* add racket.xml

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
