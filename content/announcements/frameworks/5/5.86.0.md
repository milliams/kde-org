---
qtversion: 
date: 2021-09-11
layout: framework
libCount: 83
---


### Breeze Icons

* Add language-chooser symlinks for Maliit keyboard
* Add keyboard icons for maliit Breeze theme
* Add symlinks using reverse domain name for Haguichi app and status icons
* Add new bookmark-new and bookmark-edit icons
* Re-organize bookmark icon symlinks
* kdenlive icons: Simplify xml and try to fix breeze-dark
* Use a rotating gear for the process-working animation
* Remove itinerary icons
* Add Xfce compatibility symlinks (bug 432333)

### Extra CMake Modules

* ECMSetupQtPluginMacroNames: avoid adding duplicates to vars in config code
* Enable KDEGitCommitHooks on Windows

### KDE Doxygen Tools

* Mention this updates api.kde.org, use standard kde/src paths, improve language

### KBookmarks

* Add support for static builds

### KCodecs

* KCharsets: add custom US-ASCII QTextCodec as workaround for QTBUG-83081

### KConfig

* Add Ctrl+Alt+Comma shortcut for "Configure Keyboard Shortcuts" (bug 441537)
* Make enum value visible to QML when using GlobalEnum mode
* KDesktopFile::isAuthorizedDesktopFile: reduce warning to info a log

### KConfigWidgets

* Add support for static builds
* Hide KHamburgerMenu in QMenus when a native menu bar is used (bug 439997)

### KCoreAddons

* KFileSystemType: Fix hurd build
* Python bindings: skip KPluginFactory::loadFactory and KNetworkMount
* KStringHandler: insert ZWSP between camelCase sub-words in preProcessWrap
* Create utility methods for plugin loading
* Deprecate KPluginMetaData::instantiate method
* Deprecate KPluginLoader class
* KPluginLoader: move static methods to KPluginMetaData
* Fix issue when building KCrash static (bug 440416)
* Downgrade the "invalid plugin" warning message to a debug message
* KFileSystemType: add a method to return filesystem type name

### KDAV

* Adapt to KIO DavJob factory methods removing QDomDocument from their API

### KDeclarative

* Try to fix Android build via linking GLESv3
* qtquicksettings: Add workaround for Nvidia Wayland (bug 432062)
* GridDelegate: Fix non-existant context property
* AbstractKCM: Fix padding when header/footer is not visible
* Don't use the KIO AccessManager on Android

### KDocTools

* Move ksnake to deprecated and add ksnakeduel entity
* Add an entity for Kigo

### KGlobalAccel

* Fix launching commands with args using kstart5 (bug 433362)
* Start processes with kstart5 if available (bug 433362)

### KHTML

* Be more lenient in reading QImages

### KIconThemes

* Add support for static builds

### KImageFormats

* SGIImage::writeImage: Properly fail if the image is too big
* exr: Port to std::log/pow
* PCXHandler::write: Properly fail if the image is too big

### KIO

* Inline kssld operators to avoid duplicate symbol errors (bug 428807)
* KFilePlaceEditDialog: Make the trash url non-editable
* KFilePlaceEditDialog: Always preserve previous icon (bug 439622)
* Check for "PrefersNonDefaultGPU" desktop entry (bug 433714)
* DBus activation with filemanager iface
* Build on Android without D-Bus
* kio_http: wrap a long error message (bug 160399)
* Port internal usage of KDesktopFileActions::executeService
* Remove unneeded org::kde::KDirNotify::emitFilesChanged part from deprecation message
* Deprecate KDesktopFileActions::userDefinedServices overload
* Fix wrong deprecation of method in previewjob
* KMountPoint: match by device ID (dev_t) in findByPath()
* KMountPoint: check for more network filesystems
* Don't include FUSE support on Android
* Don't include the internal/discrete GPU switching support on Android
* Don't use KAuth on Android
* Exclude various non-functional components from the Android build
* kio_file: detect failure to create symlinks due to filesystem support (bug 253678)
* Add icons for XDG publicshare and templates dirs
* Change KIO's default behavior to fork slaves rather than use klauncher
* [JobUiDelegate] Ensure button icon name and label aren't empty
* Also exclude KAutoMount on Android
* Don't build KDynamicJobTracker on Android
* CommandLauncherJob: don't use a shell when not needed
* Allow using scheme handler in OpenURLJob if kio slave exists too
* Deprecate the last bit of QDomDocument in the public KIO API
* CopyJob: ask users about replacing invalid characters in file/folder names (bug 199063)
* SkipDialog: ask users about replacing invalid chars when copying files/dirs
* KCoreDirLister: un-anchor the regex pattern in setFilter()
* [kpropertiesdialog] Allow editing icons for root-owned desktop files
* KDirOperator: expand to url only in detail tree view (bug 440475)

### Kirigami

* Switch many loaders to asynchronous
* Don't use DropShadow for overlay drawer handles
* Avoid creating then immediately discarding a page's ActionButton instance
* Add "Report Bug" action to the about page
* Support horizontal scrolling in ScrollablePage
* Use text as tooltip only for icon-only buttons (bug 441810)
* Fix Page.rightAction being labeled as leftAction in API docs
* [ActionTextField] Use small icon sizing for actions
* Add settings components
* Make the static build build again
* Add Attached property used as a hint for application to enabled spellchecking
* Handle KirigamiPluginFactor::createPlatformTheme returning nullptr
* Use rate limiting to display warnings for deprecated Units properties
* Remove iconScaleFactor and related code from Units
* Only load the first style plugin that we find
* Rewrite Units in c++
* Load theme definition when Kirigami is built with bundled resources
* fix isCurrentPage
* controls/AbstractApplicationHeader: disable hiding on touch scrolling behaviour by default (bug 439923)
* controls/AbstractApplicationHeader: make it possible to disable hiding on touch scrolling behaviour

### KItemModels

* Enable static build of QML plugin
* Don't force QML plugin to be shared library

### KJobWidgets

* Build without D-Bus on Android

### KNewStuff

* Port from KMountPoint to QStorageInfo
* Set KLocalizedContext for QtQuickDialogWrapper
* Use QToolButton for reset button as we have the same style

### KPackage Framework

* Deprecate unused virtual functions in PackageLoader
* Try loading package structures by name
* kpackagetool: Fix small typo in its output

### KParts

* Don't use D-Bus on Android

### KQuickCharts

* Refactor LineChartNode to reuse LineSegmentNode
* Increase minimum size for line chart interpolation
* Implement batching for Line chart material
* Add a property to BarChart that renders it as horizontal bars
* Support specifying a corner radius for bar chart bars
* Support setting a background color for bar charts
* Refactor BarChart to use individual nodes per bar and an sdf shader

### KRunner

* Output warning if unknown X-Plasma-API is requested
* Deprecate constructor overload which can not be used with KPluginFactory
* cmake: Remove intermediate copy target
* Use the pointer-to-member-function QObject::connect() signal/slot syntax
* Add snippet on how to port deprecated AbstractRunner action methods
* abstractrunner: Deprecate utility methods for actions
* dbusrunner: Add Teardown and Config methods to D-Bus interface

### KService

* Add support for PrefersNonDefaultGPU desktop key
* Deprecate plugin instanciation methods in KService/KServiceTypeTrader
* Point docs to KPluginMetaData/KPluginFactory instead of KPluginLoader

### KTextEditor

* Python indentation: decrease indent when appropriate keyword is typed (bug 392732)
* indentation: add Julia indentation script
* KateModeManager: unify wildcardsFind() and mimeTypesFind()
* KateModeManager::mimeTypesFind: consider types with negative priorities
* Simplify presentation of word wrapping options
* KateModeManager::wildcardsFind: consider types with negative priorities
* KateWildcardMatcher => KSyntaxHighlighting::WildcardMatcher
* KateWildcardMatcher: treat unmatched filename prefixes as no match
* tidy up text area config appearance widget
* Only process BOM for valid encodings (UTF-8/16/32)
* Do not show encoding error when file only contains BOM (bug 272579)

### KUnitConversion

* Fix initialization of currency values (bug 441337)
* Fix automatic currency file sync after 24h (bug 441337)

### KWayland

* Implement set_frost in contrast protocol
* Implement org_kde_plasma_window_management::send_to_output
* Decouple activation feedback from plasma window management global
* plasma-window-management: Support new activation concepts

### KWidgetsAddons

* KTitleWidget: Set frame backgroundRole to Window
* KFontChooser: add a default constructor that takes a QWidget* first arg
* KFontChooser: add a new shorter constructor, and setter methods
* KFontChooser: deprecate "relative font size" methods
* KMessageDialog: add buttons by default

### KWindowSystem

* Add blur and frost to fiddly thingy
* Implement setting frost
* Provide methods to translate to Qt types
* Provide constructors for compatibility with Qt types
* Read uncomplete WM_CLASS string (bug 441005)
* Don't link publicly against XCB (bug 441266)
* Add find_dependency for X11 and XCB
* Remove unused xrender dependency
* Make link against X11 and XCB public
* Use imported targets for X11 libs
* Support static builds on Linux as well

### KXMLGUI

* Add support for static builds

### Oxygen Icons

* Import icons from KAlarm

### Plasma Framework

* ExpandableListitem: Implement the expanded button view in a better way
* Fix wide button content alignment
* ExpandableListItem: Fix overlapping entries with many expanded items (bug 428102)
* ExpandableListitem: Fix stylus input for buttons (bug 426079)
* CMake: do not expand variables beforehand
* IconItem: No need to disconnect before deleting
* avoid holding onto old Svg object when changing source of an IconItem
* don't make duplicate connections to ThemePrivate::onAppExitCleanup
* [PC3&PC2/TextField] Consistent sizing of clear button with kirigami
* Plasma::PluginLoader cache: Only store and return single plugin for given id
* Applet & Containment: Take KPluginMetaData in constructor
* Add top line to plasmoidheading similarly to titlebar
* Fix Plasma (non-Qt) icon scaling with integer scale factors
* [calendar] Don't rely on ServiceType for distinguishing between old and new plugins
* Define new KPackageStructure property in kpackage structures
* Consistently name kpackage structures
* Implement KirigamiPluginFactory::createPlatformTheme
* Correct include location of Kirigami Units
* Port Kirigami Plasma plugin Units to use a C++ implementation
* [Calendar] Fix pinned calendar displaying the wrong number of dots (bug 440627)
* Deprecate unused virtual methods in Plasma::PluginLoader
* [PC3/TextField] Fix text color with certain themes (bug 438854)

### Purpose

* [imgur plugin] Add Delete Url support
* Port Telegram plugin to KIO::ApplicationLauncherJob

### QQC2StyleBridge

* Fix combobox popup in RTL mode (bug 441178)
* Label: set horizontalAlignment explicitly
* Vertically center slider (bug 410672)
* MenuItem: make it look more like Breeze QStyle
* Menu: make it look more like Breeze QStyle
* Add C++ Units implementation
* ScrollBar: less scuffed implementation of reading from QStyle (bug 418327)
* SpinBox: less scuffed implementation of reading from QStyle

### Syntax Highlighting

* cpp.xml: Add two QFlags-related macros
* markdown.xml: End nested sub-headers fold regions when parent header is closed (bug 441278)
* Python: Add "yield from" keyword (bug 441540)
* Julia highlighting fixes
* Simplify helpers of Repository::definition[s]For*()
* Optimize Repository::definitionFor(FileName|MimeType)
* Repository: unify definitionsForFileName() and definitionsForMimeType()
* add extra resource path lookups for app files
* Remove caseSensitive parameter to optimize WildcardMatcher
* Export WildcardMatcher
* Accept both Definition objects and definition names
* Move the default repository management out of KQuickSyntaxHighlighter
* Add a QObject wrapper for Repository
* Don't hardcode the default theme, but pick one based on the palette
* Import Eike's QtQuick syntax highlighter and adapt the build system
* WildcardMatcher: treat unmatched filename prefixes as no match
* Port QStringRef (deprected) to QStringView
* Add rbreak and watch keywords for GDB highlighting
* Properly highlight RR keywords containing a dash
* PL/I: Also recognise "pl1" extension and MIME type
* default themes: Add contrast between String and VerbatimString
* breeze-light.theme: Add contrast between strings and raw strings
* isocpp.xml: Highlight raw string delimeters (bug 440038)
* Update Kconfig highlighter to Linux 5.13

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
