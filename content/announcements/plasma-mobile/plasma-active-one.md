---
aliases:
- ../plasma-active-one
title: "Plasma Active One Arrives!"
date: "2011-10-09"
description: KDE Releases First Version of Cross-Device UX.
---

<p align="justify"><em>
A mobile device should be more than a collection of applications. It should reflect who you are. Plasma Active infuses your tablet with the smarts to support what you are doing, when you are doing it with the all-new, touch-based Activities user experience.
</em></p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/activity.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/activity.png" class="img-fluid" alt="A vacation planning Activity">
	</a> <br/>
	<em>A vacation planning Activity</em>
</div>
<br/>

<p align="justify">
On the 9th of October, 2011 (9.10.11), the first release of the <a href="http://plasma-active.org">Plasma Active</a> tablet user experience was made publicly available. Plasma Active One’s touchscreen interface is more than just an application launcher. As soon as the device is turned on, rather than the traditional grid of applications, you see the Activities view showing your current project, task or idea. With Activities, you can collect all of the documents, people, web sites, media and widgets related to a topic in one place, building personalized and interactive views of your life.
</p>

<p align="justify">
With Plasma Active, the possibilities are unlimited. You can add as many things to an Activity as you wish with its “infinite scroll” feature. You can create as many Activities as you like and move between them using the touch-friendly Activity Switcher.
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/activity-add-content.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/activity-add-content.png" class="img-fluid" alt="Add content to your Activity">
	</a> <br/>
	<em>Add content to your Activity</em>
</div>
<br/>

<p align="justify">
The theme of an Activity is up to you: planning a vacation, working on a business or school project, collecting photos from that amazing party last weekend, keeping tabs on your social scene, collecting your favorite news sites on the Internet. Create an Activity, give it a name and a background image, and start using it immediately by adding things to it, either directly using the Add button or by using the Share Like Connect interface.
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/share-like-connect.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/share-like-connect.png" class="img-fluid" alt="Sharing a picture using Share Like Connect">
	</a> <br/>
	<em>Sharing a picture using Share Like Connect</em>
</div>
<br/>

<p align="justify">
Share Like Connect is a group of three small icons that are always available in the top panel. They let you quickly connect what you are doing to any Activity—websites or images you are viewing, documents. Use Share Like Connect to add bookmarks. Email what you are viewing without delay to people in your address book.
</p>
<p align="justify">
Complementing Activities, Plasma Active provides a fast and efficient application launcher and includes a number of applications for you to use and enjoy out of the box. Simply pulling down on the top panel makes the Application Peek area visible. Peek shows every application that is currently running, so you can quickly switch or close applications. Pulling Peek down further exposes the launcher. It has a simple icon listing similar to other tablet and phone interfaces. The integrated search bar lets you quickly search through applications that are installed on the system.
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/peek-and-launch.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/peek-and-launch.png" class="img-fluid" alt="Peek and Launch allow you to switch between apps and open new ones">
	</a> <br/>
	<em>Peek and Launch allow you to switch between apps and open new ones</em>
</div>
<br/>

<p align="justify">
When an application is launched, it automatically connects to the current Activity. Related applications are kept together and out of your way when you switch to a different Activity.

You can be productive with Plasma Active immediately. Several applications are included by default—a web browser, image viewer, media player (Bangarang Active), office document viewer with simple editing features (Calligra Active [beta]), full-featured mail and calendaring (Kontact Touch) and several great games. These applications are optimized for the touch interface. Thousands of others are available for free from secure on-line repositories, although most likely they will not be as touch-friendly.

</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/kontact-touch.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/kontact-touch.png" class="img-fluid" alt="Kontact Touch adds scalable email and groupware to Plasma Active">
	</a> <br/>
	<em>Kontact Touch adds scalable email and groupware to Plasma Active</em>
</div>
<br/>

<p align="justify">
Plasma Active is designed and built in the open and released as free, open source software. Behind the ideas and the software is a vibrant team of community participants and companies working together with a common goal: creating amazing open software for devices. There are no proprietary lock-ins or iron curtains in front of the design and development efforts, just a terrific user experience that you can use, enjoy and participate in.
</p>
<h2>How To Get Plasma Active One</h2>
<p align="justify">
Both installable and live images of Plasma Active are <a href="http://community.kde.org/Plasma/Active/Installation#Live_Images">available for download</a>. Detailed installation instructions can be found on the <a href="http://community.kde.org/Plasma/Active/Installation">Plasma Active Installation</a> wiki page. Typically installation is a simple matter of inserting a bootable USB stick into the tablet, rebooting and following the on-screen instructions.
</p>

<p align="justify">
It is also possible to run Plasma Active in a virtual machine for testing and review purposes only. For more information, as well as caveats with this approach, please consider the <a href="http://community.kde.org/Plasma/Active/Installation#Running_a_Plasma_Active_in_a_Virtual_Machine">Plasma Active in a Virtual Machine</a> information on the wiki. We strongly recommend running Plasma Active on a tablet for the full experience, however.
</p>

<h2>Our Path Forward</h2>
<p align="justify">
<strong>Plasma Active One</strong> is one more step in the long journey the KDE Plasma team has been on since the project was started in 2007, originally to create a new desktop shell. It is not the end of that journey. Plasma Active Two will be released in mid-December, 2011 with Plasma Active Three set for release Summer 2012.
</p>
<p align="justify">
<strong>Plasma Active Two</strong> will gain automated recommendations support for Activities, which allows Plasma Active to aid you in collecting and adding relevant information and documents to Activities. We will also be adding enhanced collection viewing, filtering and sorting for all types of media and information accessible in Plasma Active. More capabilities will also be developed for the plugin-based Share Like Connect system. This release will also include improvements to stability and performance as well as feature fixes.
</p>
<p align="justify">
<strong>Plasma Active Three</strong> will be another significant release where we will be focussing on security challenges un devices to keep your data safe and under your control. With release Three, we will begin implementing additional device form factors such as settop boxes or handhelds. Plasma is built with the philosophy of "highly componentized, re-usable and re-composable interfaces". This allows us to move from device type to device type nimbly with sensible user experiences for each. We intend to exploit that flexibility to the fullest in coming development cycles.
</p>
<p align="justify">
In future development cycles, we will be adding to the number of Active applications and widgets that are available. Existing application developers are invited to join us in "Activating" their applications for touch and other device form factors. The best and most useful of these applications will be shipped with future Plasma Active images. An easy to use application downloading service will be provided for other applications.
</p>
<p align="justify">
We plan to add ARM as an officially supported architecture target next to the Intel platforms already supported. This effort is running in tandem with those intended to slim down the footprint of the base OS required to run a Plasma Active interface. In addition, we are in the process of developing a clearly documented, comprehensive specification for the underlying OS requirements.
</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/web-browser.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/web-browser.png" class="img-fluid" alt="Plasma Active's touch-friendly web browser">
	</a> <br/>
	<em>Plasma Active's touch-friendly web browser</em>
</div>
<br/>

<h2>How To Get Involved</h2>
<p align="justify">
Plasma Active is an open project: all design, planning and development is done in the open in a consensus-based working environment. Community contributors and companies creating devices or software for them are welcome and able to participate on their own terms. If you want to create new widgets, add-ons for Share Like Connect, new applications, target new devices or create entire new user experiences using Plasma Active, there is room for your efforts. Testing, stabilization, usability efforts and graphic design for the existing software are also areas for potential participation.
</p>
<p align="justify">
The libraries that Plasma Active uses are licensed under the permissive LGPL license, allowing for a variety of licensing options and customization or add-on possibilities.

You can contact the Plasma Active team on our <a href="https://mail.kde.org/mailman/listinfo/active">mailing list</a> or on irc.freenode.net in #active. There is a <a href="http://forum.kde.org/viewforum?f=211">KDE forum for user support</a>, and a German user support forum at <a href="http://forum.open-slx.de/forum/kde-plasma-active/">open-slx</a>. Private and press inquiries can be made to Sebastian K&uuml;gler (sebas@kde.org) and Eva Brucherseifer (eva.brucherseifer@basyskom.de).

</p>

<div class="text-center">
	<a href="/announcements/plasma-mobile/plasma-active-one/blinken.png">
	<img src="/announcements/plasma-mobile/plasma-active-one/thumbs/blinken.png" class="img-fluid" alt="Blinken is a fun and touch-friendly game for children">
	</a> <br/>
	<em>Blinken is a fun and touch-friendly game for children</em>
</div>
<br/>

<h2>Hardware Compatibility</h2>

<p align="justify">
Plasma Active One images have been tested and are supported on the WeTab, ViewSonic ViewPad, ExoPC tablets and the Lenovo Ideapad. More information on device compatibility can be found <a href="http://community.kde.org/Plasma/Active/Devices">here</a>. Plasma Active may install and work on other Intel-based devices as well.

ARM device support is still in development. However, Plasma Active has been successfully installed and runs on Nokia N900 and other ARM devices.

</p>

<h2>Software Compatibility</h2>

<p align="justify">
Virtually any software that is available for Linux platforms can be run on Plasma Active. We recommend KDE and Qt-based software as they provide the best integration with Plasma Active One, which itself is based on KDE Platform 4.7 and Qt 4.7. Other software, however, runs and works as it would on any other Linux-based system. This includes command line software which is accessible via the touch-ready terminal application included with Plasma Active.
</p>
<p align="justify">
Plasma Active is compatible with the <a href="https://www.kde.org/workspaces/plasmadesktop/">Plasma Desktop</a> and <a href="https://www.kde.org/workspaces/plasmanetbook/">Plasma Netbook</a> Workspaces. They are all based on the same framework, sharing more than 95% of code. This is a radically different approach to interface commonality across devices. Most other tablet products are either unique to themselves (sharing little or nothing in common with other device interfaces), or attempts to use desktop interfaces on smaller displays. But Plasma-based interfaces are crafted for specific device types, and are able to do so while having nearly all their code and engineering efforts in common. As a result, widgets and applications written for Plasma Desktop or Netbook are able to run seamlessly on Plasma Active devices.
</p>

<h2>Thanks and Acknowledgements</h2>

<p align="justify">
Plasma Active One would not have been possible without the incredible support of the Plasma community and the development and financial support of <a href="http://www.basyskom.com">basysKom</a> and <a href="http://www.open-slx.de/">open-slx</a>. Plasma Active is built on top of the excellent KDE and Qt frameworks which gave us a huge head start. We are humbled to stand alongside (and on the shoulders of) the thousands of people who have been involved in the KDE and Qt communities over the last 15 years. We also acknowledge the efforts that went into creating both the MeeGo OS and the <a href="http://openbuildservice.org/">Open Build Service</a> which were instrumental in helping to get Plasma Active One on its feet and out the door.
</p>
