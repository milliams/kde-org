---
aliases:
- ../announce-3.5.4
custom_about: true
custom_contact: true
date: '2006-08-02'
title: KDE 3.5.4
---

<h2 align="center"><span class="mw-headline" id="El_Projecte_KDE_llan.C3.A7a_la_quarta_versi.C3.B3_que_millora_les_traduccions.0Ai_serveis_de_l.27escriptori_l.C3.ADder_de_programari_lliure.">El Projecte KDE llança la quarta versió que millora les traduccions
i serveis de l'escriptori líder de programari lliure.</span></h2>
<p><br>
</p>
<p align="justify">
  <strong>
    El KDE 3.5.4 incorpora traduccions a 65 idiomes, 
    millora el funcionament amb dispositius extraïbles i 
    afegeix altres novetats al motor d'HTML (KHTML).
  </strong>
</p>
<p><br>
</p>
<p align="justify">
   El <a rel="nofollow" class="external text" href="http://www.kde.org/">Projecte KDE</a> anuncia avui
  la disponibilitat immediata del KDE 3.5.4, una nova versió de manteniment
  per l'última generació de l'escriptori <em>lliure</em> més avançat per GNU/Linux
  i altres UNIXes. El KDE té ara traducció a 65 idiomes, fent-lo disponible a més 
  persones que la majoria de programari no lliure, i pot ser fàcilment estès
  a altres comunitats que vulguin contribuir al projecte.
</p>
<p><br>
</p>
<p align="justify">
  Millores significatives:
</p>
<ul>
  <li>Compatibilitat amb dispositius extraïbles sota Linux (els usuaris poden muntar qualsevol dispositiu compatible amb <a rel="nofollow" class="external text" href="http://www.freedesktop.org/wiki/Software/hal">HAL de FreeDesktop</a> i controlar com es farà)
  </li>
  <li>Optimitzacions de velocitat a <a rel="nofollow" class="external text" href="http://konsole.kde.org/">Konsole</a> i <a rel="nofollow" class="external text" href="http://kate.kde.org/">Kate</a></li>
  <li>Ara pot començar més d'una festivitat a la mateixa data a <a rel="nofollow" class="external text" href="http://korganizer.kde.org/">KOrganizer</a></li>
  <li>Moltes correccions a <a rel="nofollow" class="external text" href="http://www.khtml.info/">KHTML</a>, el motor HTML de <a rel="nofollow" class="external text" href="http://www.konqueror.org/">Konqueror</a></li>
  <li>El diàleg per enviar certificats SSL per part del client és ara més usable</li>
  <li><a rel="nofollow" class="external text" href="http://knetworkconf.sourceforge.net/">KNetworkConf</a> és compatible amb Fedora Core 5 i gestiona millor les claus WEP</li>
</ul>
<p><br>
</p>
<p align="justify">
  Hi ha més de 10 característiques noves i s'ha corregit més de 100 errors. Per una llista més detallada de les millores des de la versió KDE 3.5.3 consulteu <a rel="nofollow" class="external text" href="/announcements/changelogs/changelog3_5_3to3_5_4">la pàgina de canvis del KDE 3.5.4</a>.
</p>
<p><br>
</p>
<p align="justify">
  El KDE 3.5.4 està format per un escriptori bàsic i quinze paquets extra(gestió d'informació personal, administració,
  xarxes, educació, utilitats vàries, multimèdia, jocs,
  gràfics, desenvolupament web i més).
</p>
<p><br>
</p>
<h3><span class="mw-headline" id="Com_col.C2.B7laborar_amb_el_KDE">Com col·laborar amb el KDE</span></h3>
<p align="justify">
El KDE és un projecte de <a rel="nofollow" class="external text" href="http://www.gnu.org/philosophy/free-sw.html">Programari Lliure</a> que existeix i creix
gràcies a molts voluntaris que donen el seu temps i esforç. El KDE
sempre busca nous voluntaris i contribucions, ja sigui per programar, corregir o informar d'errors, escriure documentació, traduïr, ajudar en la promoció, aportar diners, etc. Totes les contribucions s'accepten amb gratitud. Si us plau llegiu <a rel="nofollow" class="external text" href="http://www.kde.org/support">Com col·laborar amb el KDE</a> per més informació.</p>
<p><br>
</p>
<h3><span class="mw-headline" id="Quant_al_KDE">Quant al KDE</span></h3>
<p align="justify">
  KDE és un projecte <a rel="nofollow" class="external text" href="http://www.kde.org/community/awards">premiat</a> i independent format per <a rel="nofollow" class="external text" href="http://www.kde.org/people/">centenars</a>
  de desenvolupadors, traductors, artistes i altres professionals d'arreu del món, col·laborant gràcies a Internet
  per crear un entorn d'escriptori i oficina de lliure distribució, sofisticat, personalitzable i estable
  que utilitza una arquitectura basada en components, transparent a la xarxa i ofereix una magnífica plataforma
  de desenvolupament.</p>
<p><br>
</p>
<p align="justify">
  KDE proporciona un escriptori madur i estable que inclou un navegador 
  (<a rel="nofollow" class="external text" href="http://konqueror.kde.org/">Konqueror</a>), un conjunt d'utilitats de gestió
  d'informació personal (<a rel="nofollow" class="external text" href="http://kontact.org/">Kontact</a>), un conjunt d'aplicacions
  d'oficina complet (<a rel="nofollow" class="external text" href="http://www.koffice.org/">KOffice</a>), una gran quantitat
  d'aplicacions i utilitats de xarxa i un eficient i intuitiu entorn integrat
  de desenvolupament (<a rel="nofollow" class="external text" href="http://www.kdevelop.org/">KDevelop</a>).</p>
<p><br>
</p>
<p align="justify">
  KDE és la prova real que el model de desenvolupament de programari lliure "estil Basar"
  pot crear tecnologies de primer nivell a la par o superiors que el més complex del programari comercial.
</p>
<p><br>
</p>
<hr width="98%">
<p align="justify">
  <font size="2">
  <em>Notes en quant a marques registrades.</em>
  KDE<sup>®</sup> i el logo K Desktop Environment<sup>®</sup> són marques registrades de KDE e.V.

  Linux és una marca registrada de Linus Torvalds.

  UNIX és una marca registrada de The Open Group als Estats Units i d'altres països.

  Totes les altres marques registrades i copyrights esmentats en aquest anuncis són propietat dels respectius propietaris.
  </font>
</p>
<hr width="98%">
<p><br>
</p>
<h3><span class="mw-headline" id="Contactes_de_premsa">Contactes de premsa</span></h3>
<h4><span class="mw-headline" id="Contacte_local">Contacte local</span></h4>
<table cellpadding="10">
<tbody><tr valign="top">
<td>
   Albert Astals Cid<br>
   Telèfon: +34 - 635 21 44 41<br>
   aacid@kde.org

</td>
</tr>
</tbody></table>
<p><br>
</p>
<h4><span class="mw-headline" id="Contactes_oficials">Contactes oficials</span></h4>
<table cellpadding="10"><tbody><tr valign="top">
<td>
<p><b>Àfrica</b><br>
</p>
   Uwe Thiem<br>
   P.P.Box 30955<br>
   Windhoek<br>
   Namibia<br>
   Telèfon: +264 - 61 - 24 92 49<br>
   info-africa@kde.org

</td>

<td>
<p><b>Àsia</b><br>
</p>
    Pradeepto Bhattacharya<br>
    A-4 Sonal Coop. Hsg. Society<br>
    Plot-4, Sector-3, New Panvel, Maharashtra.<br>
    India 410206<br>
    Telèfon: +91-9821033168<br>
    info-asia@kde.org

</td>

</tr>
<tr valign="top">

<td>
<p><b>Europa</b><br>
</p>
   Matthias Kalle Dalheimer<br>
   Rysktorp<br>
   S-683 92 Hagfors<br>
   Sweden<br>
   Telèfon: +46-563-540023<br>
   info-europe@kde.org

</td>

<td>
<p><b>Nord Amèrica</b><br>
</p>
   George Staikos <br>
   889 Bay St. #205 <br>
   Toronto, ON, M5S 3K5 <br>
   Canada<br>
   Telèfon: (416)-925-4030 <br>
   info-northamerica@kde.org

</td>

</tr>

<tr>
<td>
<p><b>Oceania</b><br>
</p>
   Hamish Rodda<br>
   11 Eucalyptus Road<br>
   Eltham VIC 3095<br>
   Australia<br>
   Telèfon: (+61)402 346684<br>
   info-oceania@kde.org

</td>

<td>
<p><b>Sud Amèrica</b><br>
</p>
   Helio Chissini de Castro<br>
   R. José de Alencar 120, apto 1906<br>
   Curitiba, PR 80050-240<br>
   Brazil<br>
   Telèfon: +55(41)262-0782 / +55(41)360-2670<br>
   info-southamerica@kde.org

</td>

</tr></tbody></table>
<p><br>
</p>
<h4><span class="mw-headline" id="Contactes_regionals">Contactes regionals</span></h4>
<p><a rel="nofollow" class="external free" href="http://www.kde.org/contact/representatives.php#local_representatives">http://www.kde.org/contact/representatives#local_representatives</a>
</p><p>