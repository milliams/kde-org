---
title: Updated KDE Applications Bring Many Exciting Features
date: "2011-07-27"
hidden: true
---

<p>
KDE is happy to announce the release of new versions of many of our most popular applications. From games through education and entertainment, these applications are more powerful, yet remain easy to use as they continue to mature in this version. Below are a few highlights from some of the updated applications released today.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/dolphin-gwenview.png">
	<img src="/announcements/4/4.7.0/thumbs/dolphin-gwenview.png" class="img-fluid" alt="KDE Applications 4.7">
	</a> <br/>
	<em>KDE Applications 4.7</em>
</div>
<br/>

<p>

<strong>Kontact</strong>, KDE’s groupware solution, rejoins the rest of the KDE software for the 4.7 release. With most components now ported to Akonadi, there is increased stability, better connection to new services and sharing of communication information between more applications. The biggest change is the introduction of KMail 2. This has the familiar interface, while under the surface, all mail storage and retrieval has been ported to Akonadi.

</p><p>
<strong>Dolphin</strong>, KDE’s file manager, has a cleaner default appearance. The menu bar is hidden, but easy to reach and restore. The file searching interface has been improved. In addition, Dolphin now has much <a href="http://vishesh-yadav.com/blog/2011/07/03/mercurial-plugin-for-dolphin-work-progress-part-1-2/">deeper integration with source code management</a> systems, including the ability to create and clone repositories, push and pull changes, view diffs and much more. Dolphin and Konqueror both benefit from a new plugin that provides a rating and an annotation menu action for files and folders, leveraging Nepomuk capabilities.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/konqueror-dolphin.png">
	<img src="/announcements/4/4.7.0/thumbs/konqueror-dolphin.png" class="img-fluid" alt="Konqueror and Dolphin in 4.7">
	</a> <br/>
	<em>Konqueror and Dolphin in 4.7</em>
</div>
<br/>

<p>
<strong>Marble</strong>, the virtual globe application from KDE Edu, has gained many improvements over the past six months. It now has voice navigation support, a map creation wizard, and new plug-ins. Following the Voice of Marble contest, voice navigation is now available in several languages, with voices provided by the KDE community. For more details, see <a href="http://edu.kde.org/marble/current_1.2">Marble's  visual changelog</a>.
</p>

<div class="text-center">
	<a href="/announcements/4/4.7.0/marble.png">
	<img src="/announcements/4/4.7.0/thumbs/marble.png" class="img-fluid" alt="Marble shines in 4.7">
	</a> <br/>
	<em>Marble shines in 4.7</em>
</div>
<br/>

<p>
Image management has become easier with <strong>Gwenview</strong>, the KDE image viewer, now offering the ability to compare two or more pictures side by side. From the browse mode, select two or more pictures, and then switch to the view mode or to the full-screen mode. In the view mode, you can add more pictures from the thumbnail bar.

<div class="text-center">
	<a href="/announcements/4/4.7.0/gwenview.png">
	<img src="/announcements/4/4.7.0/thumbs/gwenview.png" class="img-fluid" alt="Comparing images in Gwenview 4.7">
	</a> <br/>
	<em>Comparing images in Gwenview 4.7</em>
</div>
<br/>

</p><p>
Comic fans will be pleased that Okular, the universal file viewer, gains support for reading a directory as a comicbook.
</p><p>
KStars, the essential KDE application for stargazers around the world, has gained a feature to predict a star hopping route and dynamic switching between its OpenGL and native rendering backends. Labels can now be assigned to points on celestial lines; comet trails are rendered in OpenGL mode.
Mathematicians and scientists can now explore higher order functions in Kalgebra and get information on oxidation states for elements in Kalzium.
</p><p>
For software developers, KDevelop has gained support for predefined indentation styles and a Python interpreter using Kross. Improved Python auto-completion and support for lex/yacc file extensions are also included in this release.
</p><p>
KDE’s Advanced Text Editor, Kate, shines with plugin improvements and stability. Detailed information is available on the <a href="http://kate-editor.org/2011/07/09/kate-in-kde-4-7/">Kate website</a>.
</p>

<h2>
DigiKam 2.0 brings face detection and recognition, image versioning support, geotagging and much more
</h2>
<p>
Today also features the release of <strong>DigiKam 2.0</strong>. The road to version 2 took more than a year of heavy development. The team proudly announces the first release of the new generation of DigiKam. This version features long awaited face detection and recognition, image versioning support, XMP metadata sidecar files support, big improvements in tagging and marking photos, reversed geotagging and many other improvements, including a total of 219 fixed bugs.

<div class="text-center">
	<a href="/announcements/4/4.7.0/digikam.png">
	<img src="/announcements/4/4.7.0/thumbs/digikam.png" class="img-fluid" alt="Digikam 2.0">
	</a> <br/>
	<em>Digikam 2.0</em>
</div>
<br/>

</p><p>
Close companion Kipi-plugins is released along with DigiKam 2.0.0. This release features new export tools to three web services - Yandex.Fotki, MediaWiki and Rajce. The GPSSync plugin now has the ability to do reverse-geocoding. And as usual, lots of bugfixes.
The many other KDE Applications updated today also have new features and numerous bug fixes, and all benefit from the latest improvements in the KDE Frameworks.
</p>

<h4>Installing KDE Applications</h4>

<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.7.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.7.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
  Some of these binary packages are available for free download from KDE's <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.7.0/">http://download.kde.org</a>.
  Additional binary packages, as well as updates to the packages now available,
will become available over the coming weeks.
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.7.0">4.7 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.7.0 may be <a href="/info/4.7.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.7.0
  are available from the <a href="/info/4.7.0#binary">4.7.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we strongly recommend to use the latest version of Qt, as of today 4.7.4. This is necessary in order to assure a stable experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>




<h2>Also Announced Today:</h2>

<h3>
<a href="../plasma">
Plasma Workspaces Become More Portable Thanks to KWin
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/4/4.7.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.7" />
</a>

The Plasma Workspaces gain from extensive work to KDE’s compositing window manager, KWin, and from the leveraging of new Qt technologies such as Qt Quick. For full details, read the <a href="../plasma">Plasma Desktop and Plasma Netbook 4.7 release announcement</a>.
<br />
<br/>

</p>

<h3>
<a href="../platform">
Improved Multimedia, Instant Messaging and Semantic Capabilities in the KDE Platform
</a>
</h3>

<p>
<a href="../platform">
<img src="/announcements/4/4.7.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.7"/>
</a>

A wide range of KDE and third party software will be able to take advantage of extensive work in Phonon and major improvements to the semantic desktop components, with enriched APIs and improved stability. The new KDE Telepathy framework offers integration of instant messaging directly into workspaces and applications. Performance and stability improvements in nearly all components lead to a smoother user experience and a reduced footprint of applications using the KDE Platform 4.7. For full details, read the <a href="../platform">KDE Platform 4.7 release announcement</a>.
<br />

</p>
