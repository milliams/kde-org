---
aliases:
- ../4.10
- ../4.10.et
custom_about: true
custom_contact: true
date: '2013-02-06'
title: KDE väljalase 4.10
---

{{<figure src="/announcements/4/4.10.0/plasma-4.10.png" class="text-center" caption="The KDE Plasma Workspaces 4.10" >}}

KDE-l on rõõm teatada uusimatest Plasma töötsoonide, rakenduste ja arendusplatvormi väljalasetest. 4.10 väljalaskes on kodu- ja ärikasutajale mõeldud suurepärane vaba tarkvara täiustanud paljusid rakendusi ja pakub kasutajatele uusimat tehnoloogiat.

<h2> <a href="./plasma"><img src="/announcements/4/4.10.0/images/plasma.png" class="app-icon float-left mr-3" alt="The KDE Plasma Workspaces 4.10" />  Plasma töötsoonid 4.10 parandavad mobiilsete seadmete toetust ja näevad kaunimad välja
</a></h2>

Mitmed Plasma töötsoonide komponendid porditi Qt Quick/QML-i raamistikule. Stabiilsus ja kasutatavus on paranenud. Lisandunud on uus trükkimishaldur ja värvihalduse toetus.

<h2> <a href="./applications"><img src="/announcements/4/4.10.0/images/applications.png" class="app-icon float-left mr-3" alt="The KDE Applications 4.10"/>
KDE rakendused: hõlpsamad kasutada, parema jõudlusega ja viivad lausa Marsile
</a></h2>

KDE rakendustes on eriti tuntavaid täiendusi saanud Kate, KMail ja Konsool. KDE õpirakendustes on põhjalikult muudetud KTouchi, aga muutusi on teisigi. KDE mängud pakuvad uut mängu Picmi ning mitmeid mängimist parandamist täiustusi.

<h2> <a href="./platform"><img src="/announcements/4/4.10.0/images/platform.png" class="app-icon float-left mr-3" alt="The KDE Development Platform 4.10"/> 
KDE platvorm 4.10 viib rohkem API-sid üle Qt Quickile
</a></h2>

KDE rakendustes on eriti tuntavaid täiendusi saanud Kate, KMail ja Konsool. KDE õpirakendustes on põhjalikult muudetud KTouchi, aga muutusi on teisigi. KDE mängud pakuvad uut mängu Picmi ning mitmeid mängimist parandamist täiustusi.

<br/>

KDE kvaliteedikontrolli meeskond korraldas väljalaske huvides <a href="http://www.sharpley.org.uk/blog/kde-testing">testimisprogrammi</a> aitamaks arendajatel tuvastada tegelikke vigu ja katsetamaks põhjalikult paljusid rakendusi. Nii võib öelda, et KDE puhul käivad uuenduslikkus ja kvaliteet käsikäes. Kui tuunete huvi kvaliteedikontrolli meeskonna tegemiste vastu, uurige <a href="http://community.kde.org/Get_Involved/Quality">meeskonna veebilehekülge</a>.<br />

</p>

## Levitage sõna ja jälgige toimuvat: silt "KDE" on oluline

KDE julgustab kõiki levitama sõna sotsiaalvõrgustikes. Edastage lugusid uudistesaitidele, kasutage selliseid kanaleid, nagu delicious, digg, reddit, twitter, identi.ca. Laadige ekraanipilte üles sellistesse teenustesse, nagu Facebook, Flickr, ipernity ja Picasa, ning postitage neid sobivatesse gruppidesse. Looge ekraanivideoid ja laadige need YouTube'i, Blip.tv-sse, Vimeosse või mujale. Ärge unustage lisada üleslaaditud materjalile silti "KDE", et kõik võiksid vajaliku materjali hõlpsamini üles leida ja KDE meeskond saaks koostada aruandeid KDE 4.10 väljalaske kajastamise kohta.

Kõike seda, mis toimub seoses väljalaskega sotsiaalvõrgustikes, võite jälgida KDE kogukonna otsevoos. See sait koondab reaalajas kõik, mis toimub identi.ca-s, twitteris, youtube'is, flickris, picasawebis, ajaveebides ja paljudes teistes sotsiaalvõrgustikes. Otsevoo leiab aadressilt <a href="http://buzz.kde.org">buzz.kde.org</a>.

</p>

## Väljalaskepeod

Nagu ikka, korraldavad KDE kogukonna liikmed kõikjal maailmas väljalaskepidusid. Mõned on juba praegu ette teada, aga neid tuleb kindlasti rohkemgi. Nende kohta annab aimu meie <a href="http://community.kde.org/Promo/Events/Release_Parties/4.10">pidude nimekiri</a>. Kõik on oodatud osalema! Pidudelt leiab nii head seltskonda, vaimustavaid kõnesid kui ka veidi süüa-juua. See on suurepärane võimalus saada teada, mis toimub KDE-s, lüüa ise kaasa või ka lihtsalt kohtuda teiste kasutajate ja arendajatega.

Me innustame inimesi pidusid korraldama. Neil on lõbus viibida nii peoperemehe kui ka külalisena! Uuri lähemalt <a href="http://community.kde.org/Promo/Events/Release_Parties">näpunäiteid, kuidas pidu korraldada</a>.

## Väljalasketeadetest

Käesolevad väljalasketeated koostasid Devaja Shah, Jos Poortvliet, Carl Symons, Sebastian Kügler ja teised KDE propageerimismeeskonna ning laiemagi kogukonna liikmed. Eesti keelde tõlkis need Marek Laane. Väljalasketeated tõstavad esile kõige tähtsamaid muudatusi suures arendustöös, mida KDE tarkvara on viimasel poolel aastal üle elanud.

#### KDE toetamine

<a href="http://jointhegame.kde.org/"><img src="/announcements/4/4.9.0/images/join-the-game.png"
class="img-fluid float-left mr-3" alt="Join the Game"/> </a>

KDE e.V. uus <a href="http://jointhegame.kde.org/">toetajaliikme programm</a> on
nüüd avatud. 25 euro eest kvartalis võite omalt poolt kindlustada,
et KDE rahvusvaheline kogukond kasvab ja areneb ning suudab valmistada
maailmaklassiga vaba tarkvara.

<p>&nbsp;</p>
