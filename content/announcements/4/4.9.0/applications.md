---
title: KDE Applications 4.9 Are More Polished and Stable
date: "2012-12-05"
hidden: true
---

<p>
KDE is happy to announce the release of improved versions of several popular applications. This includes many important basic tools and applications like Okular, Kopete, KDE PIM, educational applications and games.
</p>
<p>
KDE's document viewer Okular can now store and print annotations with PDF documents. Search, bookmarking and text selection have been improved. Okular can be set up so that a laptop won't sleep or turn off its screen during a presentation, and it can now play embedded movies in PDF files. Image viewer Gwenview brings a new full-screen browse option and a number of bug fixes and small improvements.
</p>
<p>
Juk, KDE's default music player, brings last.fm support with scrobbling and cover-fetching as well as MPRIS2 support. It will read cover art embedded in MP4 and AAC files. Dragon, KDE's video player, also works with MPRIS2 now.
</p>
<p>
The versatile chat client Kopete can group all off-line users in a single "Offline Users" group and show contact status change in chat windows. It has a new 'rename' contact option and allows custom display name changes inline.
</p>
<p>
The Lokalize translation application has improved fuzzy search and a better file search tab. It can now also handle .TS files. Umbrello has auto-layout support for diagrams and can export graphviz dot drawings. Okteta introduces view profiles, including an editor/manager.
</p>
<h2>Kontact Suite</h2>
<p>
The world’s most complete PIM suite Kontact received many bugfixes and performance improvements. This release introduces an import wizard to get settings, mails, filters, calendar and addressbook entries from Thunderbird and Evolution into KDE PIM. There is a tool that can backup and restore email, configuration and metadata. KTnef, the standalone TNEF attachment viewer, has been brought back to life from the KDE 3 archives. Google resources can be integrated with KDE PIM, giving users access to their Google contacts and calendar data.
</p>
<h2>KDE Education</h2>

<p>
KDE-Edu introduces Pairs, a new memory game. Rocs, the graph theory application for students and teachers, gained a number of improvements. Algorithms can now be executed step-wise, the undo and cancel-construction system works better, and overlay graphs are now supported. Kstars has improved sorting by meridian transit time / observation time and better Digital Sky Survey <a href="http://en.wikipedia.org/wiki/Digitized_Sky_Survey">image retrieval</a>.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-pairs.png">
	<img src="/announcements/4/4.9.0/kde49-pairs-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

</p>
<p>
Marble received speed optimizations and threading support, and its user interface has been enhanced. Marble routing extensions now include OSRM (Open Source Routing Machine), support for bicycle and pedestrian routing, and an offline data model to manage offline routing and offline search data. Marble can now show positions of aircraft in the FlightGear simulator.
</p>
<h2>KDE Games</h2>
<p>
KDE Games have been upgraded. There has been a lot of polish to Kajongg, KDE's Mahjongg game, including tooltip playing hints, improved robot AI and chat if players are on the same server (kajongg.org now has one!). KGoldrunner has a number of new levels (a contribution by Gabriel Miltschitzky) and KPatience retains game history upon saving. KSudoku has seen small improvements such as better hints, as well as seven new two-dimensional puzzle shapes and three new 3-D shapes.

<div class="text-center">
	<a href="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai.png">
	<img src="/announcements/4/4.9.0/kde49-ksudoku-3d-samurai-thumb.png" class="img-fluid">
	</a> <br/>
</div>
<br/>

<h4>Installing KDE Applications</h4>
<p align="justify">
KDE software, including all its libraries and its applications, is available for free under Open Source licenses. KDE software runs on various hardware configurations and CPU architectures such as ARM and x86, operating systems and works with any kind of window manager or desktop environment. Besides Linux and other UNIX based operating systems you can find Microsoft Windows versions of most KDE applications on the <a href="http://windows.kde.org">KDE software on Windows</a> site and Apple Mac OS X versions on the <a href="http://mac.kde.org/">KDE software on Mac site</a>. Experimental builds of KDE applications for various mobile platforms like MeeGo, MS Windows Mobile and Symbian can be found on the web but are currently unsupported. <a href="http://plasma-active.org">Plasma Active</a> is a user experience for a wider spectrum of devices, such as tablet computers and other mobile hardware.
<br />
KDE software can be obtained in source and various binary formats from <a
href="http://download.kde.org/stable/4.9.0/">http://download.kde.org</a> and can
also be obtained on <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
or with any of the <a href="http://www.kde.org/download/distributions">major
GNU/Linux and UNIX systems</a> shipping today.
</p>
<p align="justify">
  <a id="packages"><em>Packages</em></a>.
  Some Linux/UNIX OS vendors have kindly provided binary packages of 4.9.0 
for some versions of their distribution, and in other cases community volunteers
have done so. <br />
<a id="package_locations"><em>Package Locations</em></a>.
For a current list of available binary packages of which the KDE's Release Team has
been informed, please visit the <a href="/info/4.9.0">4.9 Info
Page</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  The complete source code for 4.9.0 may be <a href="/info/4.9.0">freely downloaded</a>.
Instructions on compiling and installing KDE software 4.9.0
  are available from the <a href="/info/4.9.0#binary">4.9.0 Info Page</a>.
</p>

<h4>
System Requirements
</h4>
<p align="justify">
In order to get the most out of these releases, we recommend to use a recent version of Qt, either 4.7.4 or 4.8.0. This is necessary in order to assure a stable and performant experience, as some improvements made to KDE software have actually been done in the underlying Qt framework.<br />
In order to make full use of the capabilities of KDE's software, we also recommend to use the latest graphics drivers for your system, as this can improve the user experience substantially, both in optional functionality, and in overall performance and stability.
</p>




<h2>Also Announced Today:</h2>

<h2><a href="../plasma"><img src="/announcements/4/4.9.0/images/plasma.png" class="app-icon float-left m-3" alt="The KDE Plasma Workspaces 4.9" /> Plasma Workspaces 4.9 – Core Improvements</a></h2>
<p>
Highlights for Plasma Workspaces include substantial improvements to the Dolphin File Manager, Konsole X Terminal Emulator, Activities, and the KWin Window Manager. Read the complete <a href="../plasma">'Plasma Workspaces Announcement'.</a>
</p>

<h2><a href="../platform"><img src="/announcements/4/4.9.0/images/platform.png" class="app-icon float-left m-3" alt="The KDE Development Platform 4.9"/> KDE Platform 4.9</a></h2>
<p>
Today’s KDE Platform release includes bugfixes, other quality improvements, networking, and preparation for Frameworks 5
</p>
