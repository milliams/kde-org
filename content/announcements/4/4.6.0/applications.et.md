---
title: KDE Dolphinile lisandus täpsustav otsing
date: "2011-01-26"
hidden: true
---

<h2>KDE laskis välja rakendused 4.6.0</h2>
<p>
 KDE-l on rõõm teatada paljude rakenduste uute versioonide väljalaskest. Olgu tegemist mängude, õpirakenduste või ka pisikeste tööriistadega, on nad uues versioonis senisest võimsamad, aga ometi veel lihtsamini kasutatavad. Allpool on ära toodud vaid mõned olulised näited täna välja lastud rakenduste uute omaduste kohta.
</p>
<h2>Dolphinile lisandus täpsustav otsing</h2>
<p>
Kfindi ja Dolphini otsinguliidesed ühtlustati uueks lihtsamaks otsinguribaks. Uus täpsustav otsing ehk nõndanimetatud fassettsirvimine avaldub filtripaneelina, mis võimaldab väga lihtsalt sirvida indekseeritud faile nende metaandmeid ära kasutades. See uus külgriba täiustab tunduvalt traditsioonilise failide sirvimise võimalusi.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a05.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a05.png" class="img-fluid" alt="Dolphin's faceted browsing lets you use multiple filters to find files by metadata">
	</a> <br/>
	<em>Dolphini fassettsirvimisel saab failide leidmiseks metaandmete abil kasutada mitmeid filtreid</em>
</div>
<br/>

<ul>
    <li>veeruvaate kasutamist hõlbustavad parandused. Veergude laius kohandub nüüd automaatselt, aga seda võib ka teha kasutaja; faile saab hõlpsasti valida ka vajalike failide ümber kasti tõmmates; naaberveergudesse liikumiseks ei kasutata enam rõhtsat kerimisriba.</li>
    <li>et KDE arendus on hakanud üle minema SVN-ist Gitti, pakub Dolphin nüüd uut Giti pluginat, mis lubab otse failihalduris sooritada uuendamist ja sissekandmist. Loomulikult on endiselt olemas ka SVN-i plugin.</li>
    <li>mitmesuguseid parandusi on saanud teenustemenüüd (<a href="http://ppenz.blogspot.com/2010/11/improved-service-menus.html">http://ppenz.blogspot.com/2010/11/improved-service-menus.html</a>)</li>
</ul>

<h2>Katel on nüüd SQL-i klient</h2>
Võimas tekstiredaktor Kate on saanud tublisti lihvi ja uusi omadusi, mille seast eriti tasub ära märkida järgmisi:
<ul>
    <li>kohalike failide salvestamata andmete taastamine Kate järgmisel käivitamisel (puhverfailide toetus).</li>
    <li>alati laaditakse pluginad.</li>
    <li>skriptide lisamine menüüsse.</li>
    <li>Kate <a href="http://kate-editor.org/2010/07/29/katesql-a-new-plugin-for-kate/">uus SQL-i päringute plugin</a> annab redaktorile lihtsa SQL-i kliendi omadused, sealjuures on Qt SQL-i mooduli vahendusel toetatud väga mitmesugused andmebaasid.</li>
    <li>uus GNU siluri (GDB) plugin.</li>
    <li>uus valitud teksti esiletõstmise plugin.</li>
</ul>

<h2>Graafikarakendused muutusid sotsiaalseks</h2>
<p>
KDE pildinäitaja Gwenview sai nupu "Jaga", mis võimaldab eksportida pilte levinud fotode jagamise saitidele või sotsiaalvõrgustikesse.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a03.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a03.png" class="img-fluid" alt="Gwenview võib laadida pilte paljudesse levinud sotsiaalvõrgustikesse">
	</a> <br/>
	<em>Gwenview võib laadida pilte paljudesse levinud sotsiaalvõrgustikesse</em>
</div>
<br/>

<p>
<b>KSnapshot</b> võimaldab nüüd vabalt valida ekraani piirkonna, millest pilt teha, kaasata pilti ka hiirekursori ning pakub samuti võimalust saata tehtud pilt otsekohe mõnda sotsiaalvõrgustikku.</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a04.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a04.png" class="img-fluid" alt="KSnapshot võib saata ekraanipildi otse veebi">
	</a> <br/>
	<em>KSnapshot võib saata ekraanipildi otse veebi</em>
</div>
<br/>

Paljud teisedki täna uuendatud KDE rakendused on saanud uusi omadusi ja arvukalt veaparandusi, samal ajal võimaldab KDE platvormi uus versioon neil olla senisest kiirem ja stabiilsem.

<h2>Marble viib teid koju, KStars näitab tähti kiiremini tänu OpenGL-ile</h2>
<p>
KDE virtuaalne gloobus Marble jätkas teekondade kavandamise täiustamist, pakkudes nüüd ka võimalust teekonnaandmeid alla laadida. Mobiilse versiooni <b>MarbleToGo</b> võib juba tunnistada väga võimekaks teekonnajuhiseid pakkuvaks navigaatoriks. Pilte ja videoid leiab saidilt <a href="http://dot.kde.org/2010/11/10/kdes-marble-team-holds-first-contributor-sprint">dot.kde.org</a>, illustreeritud tutvustuse aga <a href="http://edu.kde.org/marble/current_1.0">siit</a>.
</p>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a01.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a01.png" class="img-fluid" alt="Marble’s advanced routing can use a variety of configurable backends">
	</a> <br/>
	<em>Marble täiustatud teekonna kavandamine võib kasutada mitmeid seadistatavaid taustaprogramme</em>
</div>
<br/>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a06.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a06.png" class="img-fluid" alt="The mobile version of Marble is a capable personal navigation tool">
	</a> <br/>
	<em>Marble mobiilne versioon on usaldusväärne personaalne navigaator</em>
</div>
<br/>

<p>
KDE töölauaplanetaarium KStars suudab nüüd renderdamiseks kasutada OpenGL-i, mis parandab tunduvalt jõudlust riistvara korral, mis toetab OpenGL-i võimalusi.</s></li>
</p>

<h2>KDE mängud</h2>
<ul>
    <li><b>Klickety</b>, Clickomania mängu mugandus, on tagasi, asendades või õigemini liidendades ühtlasi mängu KSame.</li>
    <li>KGameRendereri raamistik ühtlustab teemade kasutamist eri mängudes, tagades ühtlasema ja viimistletuma välimuse. Üle kümne KDE mängu tarvitab juba seda uut arhitektuuri, millega kaasneb mälukasutuse vähenemine ning võimalus ära kasutada mitme protsessori eeliseid.</li>
    <li>Palapeli uus puslelõigustaja lõigub pildid just sellisteks tükkideks, nagu kasutaja soovib, kusjuures servasüvenditega on võimalik anda pusletükkidele ruumiline välimus. Omajagu lihtsamalt kasutatavaks on muudetud Palapeli uue pusle loomise dialoog.</li>
    <li><b>Kajonggi</b> täiendatud käsiraamat selgitab mängu olemust ka algajatele. Nutikam robotmängija ning paranenud mängitavus tänu senisest etemale mängukivide käitlemisele muudavad selle traditsioonilise mängu mängimise tõeliseks lõbuks.</li>
</ul>

<div class="text-center">
	<a href="/announcements/4/4.6.0/46-a02.png">
	<img src="/announcements/4/4.6.0/thumbs/46-a02.png" class="img-fluid" alt="KDE puslemänguga Palapeli saab hõlpsasti pusleks muuta ka enda pilte">
	</a> <br/>
	<em>KDE puslemänguga Palapeli saab hõlpsasti pusleks muuta ka enda pilte</em>
</div>
<br/>

<h4>KDE rakenduste paigaldamine</h4>
<p align="justify">
KDE tarkvara, sealhulgas kõik teegid ja rakendused, on vabalt saadaval vastavalt avatud lähtekoodiga tarkvara litsentsidele. KDE tarkvara töötab väga mitmesugusel riistvaral, operatsioonisüsteemides ning igasuguste aknahaldurite ja töökeskkondadega. Lisaks Linuxile ja teistele UNIX-il põhinevatele süsteemidele leiab enamiku KDE rakenduste Microsoft Windowsi versioonid leheküljelt <a href="http://windows.kde.org">KDE software on Windows</a> ja Apple Mac OS X versioonid leheküljelt <a href="http://mac.kde.org/">KDE software on Mac</a>. Veebist võib leida KDE rakenduste eksperimentaalseid versioone mitmele mobiilsele platvormile, näiteks MeeGo, MS Windows Mobile ja Symbian, kuid need on esialgu ametliku toetuseta.
<br />
KDE tarkvara saab hankida lähtekoodina või mitmesugustes binaarvormingutes aadressilt <a
href="http://download.kde.org/stable/4.6.0/">http://download.kde.org</a>, samuti
<a href="http://www.kde.org/download/cdrom">CD-ROM-il</a>
või ka mis tahes tänapäevasest <a href="http://www.kde.org/download/distributions">
GNU/Linuxi ja UNIX-i süsteemist</a>.
</p>
<p align="justify">
  <a id="packages"><em>Paketid</em></a>.
  Mõned Linux/UNIX OS-i tootjad on lahkelt valmistanud 4.6.0 
binaarpaketid mõnele oma distributsiooni versioonile, mõnel juhul on sama teinud
kogukonna vabatahtlikud. <br />
  Mõned binaarpaketid on vabalt allalaaditavad KDE saidilt <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.6.0/">http://download.kde.org</a>.
  Lähinädalatel võib lisanduda teisigi binaarpakette, samuti praegu
saadaolevate pakettide uuendusi.
<a id="package_locations"><em>Pakettide asukohad</em></a>.
Praegu saadaolevate binaarpakettide nimekirja, millest KDE väljalaskemeeskond on teadlik,
näeb vastaval <a href="/info/4.6.0">4.6infoleheküljel</a>.
</p>
<p align="justify">
  <a id="source_code"></a>
  Täieliku 4.6.0 lähtekoodi võib vabalt alla laadida <a
href="http://download.kde.org/stable/4.6.0/src/">siit</a>.
Juhiseid KDE tarkvarakomplekti 4.6.0 kompileerimiseks ja paigaldamiseks
  leiab samuti <a href="/info/4.6.0#binary">4.6.0infoleheküljelt</a>.
</p>

<h4>
Nõuded süsteemile
</h4>
<p align="justify">
Võimaldaks ära kasutada väljalasete täit võimsust, soovitame tungivalt pruukida Qt uusimat versiooni, milleks praegu on 4.7.2. See on hädavajalik stabiilsuse kindlustamiseks, sest mõnedki KDE tarkvara parandused toetuvad tegelikult aluseks olevale Qt raamistikule.<br />
Graafikadraiverid võivad teatud olukorras komposiitvõimaluste kasutamisel eelistada OpenGL-ile hoopis XRenderit. Kui täheldate tõsiseid graafikaprobleeme, võib usutavasti aidata töölauaefektide väljalülitamine, sõltuvalt küll konkreetsest graafikadraiverist ja selle seadistustest. KDE tarkvara kõigi võimaluste täielikuks ärakasutamiseks soovitame kasutada ka uusimaid süsteemile mõeldud graafikadraivereid, mis võivad tunduvalt parandada süsteemi kasutamist nii funktsioonide mõttes kui ka eriti üldise jõudluse mõttes. 
</p>

<h4>
Autorid
</h4>
<p>
Käesoleva väljalasketeate koostasid Vivek Prakash, Stefan Majewsky, Guillaume De Bure, Nikhil Marathe, Markus Slopianka, Stuart Jarvis, Jos Poortvliet, Nuno Pinheiro, Carl Symons, Marco Martin, Sebastian Kügler, Nick P ja veel paljud KDE propageerimise meeskonna liikmed. Eesti keelde tõlkis väljalasketeate Marek Laane.
</p>





<h2>Täna ilmusid veel:</h2>

<h3>
<a href="../plasma">
Plasma töötsoonid annavad juhtohjad kasutaja kätte
</a>
</h3>

<p>
<a href="../plasma">
<img src="/announcements/4/4.6.0/images/plasma.png" class="app-icon float-left m-3" alt="KDE Plasma töötsoonid 4.6.0" />
</a>

<b>KDE Plasma töötsoonidele</b> lisandus uus tegevuste süsteem, mille abil on palju hõlpsam seostada rakendusi konkreetse tegevusega, näiteks tööülesannete või koduste tegemistega. Uuendatud toitehaldus pakub uusi võimalusi, aga ka senisest lihtsamat seadistamist. Plasma töötsooni aknahaldur KWin sai skriptide kasutamise võimaluse ning töötsoonid tervikuna mitmeid visuaalseid parandusi. Mobiilsetele seadmetele kohandatud <b>Plasma Netbook</b> on senisest tunduvalt kiirem ning seda on lihtsam kasutada ka puuteekraaniga seadmete puhul. Täpsemalt kõneleb kõigest <a href="../plasma">KDE Plasma töötsoonide 4.6 teadaanne</a>.

</p>

<h3>
<a href="../platform">
Suund mobiilsusele kahandab KDE platvormi kaalu
</a>
</h3>

<p>

<a href="../platform">
<img src="/announcements/4/4.6.0/images/platform.png" class="app-icon float-left m-3" alt="KDE arendusplatvorm 4.6.0"/>
</a>

<b>KDE platvorm</b>, millele tuginevad Plasma töötsoonid ja KDE rakendused, sai uusi omadusi, millest võidavad kõik KDE rakendused. Uus suund mobiilsusele muudab hõlpsamaks mobiilsetele seadmetele mõeldud rakenduste arendamise. Plasma raamistik toetab nüüd töölauavidinate kirjutamist Qt deklaratiivses programmeerimiskeeles QML ning pakub uusi JavaScripti liideseid andmete kasutamiseks. KDE rakendustele metaandmete ja otsinguvõimalusi pakkuv Nepomuki tehnoloogia sai graafilise liidese, mille abil andmeid varundada ja taastada. Iganenud HAL-i asemel on nüüd võimalik kasutada UPowerit, UDevi ja UDisksi. Paranes Bluetoothi toetus. Oxygeni vidina- ja stiilikomplekt sai mitmeid täiustusi ning uus GTK rakendustele mõeldud Oxygeni teema võimaldab neid sujuvalt ühendada Plasma töötsoonidega, nii et nad näevad välja nagu KDE rakendused. Täpsemalt kõneleb kõigest <a href="../platform">KDE platvormi 4.6 teadaanne</a>.

</p>
