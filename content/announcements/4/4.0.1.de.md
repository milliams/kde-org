---
aliases:
- ../announce-4.0.1
- ../announce-4.0.1.de
date: '2008-02-05'
title: KDE 4.0.1 - Pressemitteilung
---

<h3 align="center">
   Das KDE-Projekt veröffentlicht die erste Übersetzungs- und Wartungsversion der führenden, freien Arbeitsumgebung.
</h3>

<p align="justify">
Die Gemeinschaft der KDE-Entwickler veröffentlichen die erste Übersetzungs- und Wartungsversion der 4.0-Entwicklungslinie der freien Arbeitsumgebung, welche zahlreiche Fehlerbereinigungen, Verbesserungen in puncto Geschwindigkeit und Aktualisierungen der Übersetzungen enthält.
</p>

<p align="justify">
  Die <a href="http://www.kde.org/">Gemeinschaft der KDE-Entwickler</a> gab heute die sofortige Verfügbarkeit von KDE 4.0.1, der ersten Fehlerbehebungs- und Wartungsversion der letzten Generation der am weitesten entwickelten und umfangreichsten, freien Arbeitsumgebung bekannt. KDE 4.0.1 kommt von Haus aus mit einer Standard-Arbeitsumgebung und vielen weiteren Paketen, für Dinge wie Administration, Netzwerke, Bildung, Werkzeuge, Multimedia, Spiele, Grafik und Gestaltung, Web-Entwicklung und noch vielen mehr. Die Programme von KDE wurden bereits mit zahlreichen Preisen ausgezeichnet, und sind in beinahe 50 verschiedenen Sprachen verfügbar.
</p>
<p align="justify">
	KDE, inklusive aller Bibliotheken und Anwendungen ist gratis unter freien Lizenzen (&bdquo;Open Source&ldquo;) verfügbar. KDE kann im Quelltext und in verschiedenen Binärformaten von <a
href="http://download.kde.org/stable/4.0.1/">http://download.kde.org</a> heruntergeladen, oder aber auch auf <a href="http://www.kde.org/download/cdrom">CD-ROM</a>
bestellt werden. Außderem ist KDE in jedem <a href="http://www.kde.org/download/distributions">führenden GNU/Linux- und UNIX-System</a>, das heutzutage ausgeliefert wird, enthalten.
</p>

<h4>
  <a id="changes">Verbesserungen</a>
</h4>
<p align="justify">

KDE 4.0.1 ist eine Wartungsversion, die zahlreiche Korrekturen von Problemen die mittels dem <a href="http://bugs.kde.org/">KDE-System zur Fehlerverfolgung</a> berichtet wurden bietet, sowie Verbesserungen im Bezug auf existierende und vollständig neue Übersetzungen.
<p />
Diese Verbesserungen beinhalten unter anderem:

<ul>
	<li>
		Konqueror, der Webbrowser von KDE erfuhr zahlreiche Verbesserungen in Sachen Stabilität und Geschwindigkeit in seiner Komponente zur Anzeige von HTML-Dateien, dem Programm zum Laden der Flash-Ertweiterung und in KJS, der JavaScript-Komponente.
	</li>
	<li>
    Stabilitätsprobleme in Komponenten die in der gesamten Programmierung von KDE verwendet wurden wurden behoben. Weiter sind die Übersetzungen in dieser Version vollständiger.
    </li>
    <li>
    KWin, der KDE-Fenstermanager erhielt Verbesserungen im Bezug auf die automatische Erkennung der Systemfähigkeiten zum Aktivieren der erweiterten, grafischen Effekte. Außderem wurden Probleme mit einigen Effekten behoben.
    </li>
</ul>
</p>

<p align="justify">
	Abgesehen von diesen grundlegenen Dingen wurde auch Hand an viele Anwendungen gelegt, wie zum Beispiel Okular, &bdquo;Systemeinstellungen&ldquo; und KStars. Neue Übersetzungen ins Dänische, Friesische, Kasachische und Tschechische sind außderem verfügbar.
 </p>

<p align="justify">
	Für eine detaillierte Liste der Änderungen seit der Veröffentlichung von KDE 4.0 im Januar werfen Sie bitte eine Blick auf die <a
href="/announcements/changelogs/changelog4_0to4_0_1">Liste der Änderungen in KDE 4.0.1</a>.
</p>

<p align="justify">
 Zusätzliche Informationen über die Neuerungen und Verbesserungen in der 4.0.x-Serie von KDE finden sich in der <a href="/announcements/4.0/">Pressemitteilung zur Veröffentlichung von KDE 4.0</a>.
</p>
<h4>
  Installation von KDE-4.0.1-Binärpaketen
</h4>
<p align="justify">
  <em>Paketanbieter</em>.
  Einige Linux- und Unix-Betriebssystemhersteller haben freundlicherweise Binärpakete von KDE 4.0.1 für ihre Distrbutionen erstellt, in anderen Fällen haben dies Freiwillige erledigt. 
  Einige dieser Binärpakete stehen gratis zum Herunterladen auf <a
href="http://download.kde.org/binarydownload.html?url=/stable/4.0.1/">http://
download.kde.org</a> zur Verfügung.
  Zusätzliche Binärpakete, sowie Aktualisierungen der derzeit verfügbaren Pakete werden voraussichtlich im Laufe der Woche verfügbar sein.
</p>

<p align="justify">
  <a id="package_locations"><em>Binärpakete finden</em></a>.
  Für eine Liste der derzeit verfügbaren Binärpakete, über deren Verfügbarkeit das KDE-Projekt in Kenntnis gesetzt wurde, werfen Sie bitte einen Blick auf die <a href="/info/4.0.1">KDE-4.0.1-Infoseite</a>.
</p>

<h4>
  KDE 4.0.1 kompilieren
</h4>
<p align="justify">
  <a id="source_code"></a><em>Quelltext</em>.
  Der vollständige Quelltext von KDE 4.0.1 kann <a
href="http://download.kde.org/stable/4.0.1/src/">gratis heruntergeladen</a> werden.
Anweisungen zum Kompilieren und Installieren von KDE 4.0.1 sind auf der <a href="/info/4.0.1#binary">KDE-4.0.1-Infoseite</a> verfügbar.
</p>

<h4>
  KDE unterstützen
</h4>
<p align="justify">
	KDE ist ein <a href="http://www.gnu.org/philosophy/free-sw.html">freies Softwareprojekt</a>, dass nur existiert und wächst, weil unzählige Freiwillige ihre Zeit und Arbeit investieren. KDE ist immer auf der Suche nach neuen Freiwilligen, die etwas beitragen möchten. Unabhängig davon ob es ums Programmieren, das Beheben oder Berichten von Fehlern, das Schreiben von Dokumentationen, Übersetzungen, Werbung und Öffentlichkeitsarbeit, Geld, usw. geht. Jegliche Beiträge sind willkommen und werden mit Freuden entgegengenommen. Bitte lesen Sie die Seite <a
href="/community/donations/">&bdquo;KDE unterstützen&ldquo;</a> für nähere Informationen. </p>

<p align="justify">
	Wir würden uns freuen bald von Ihnen zu hören!
</p>

<h4>Über KDE 4</h4>
<p>
	KDE 4.0 ist die innovative, freie Arbeitsumgebung mit zahlreichen Anwendungen, sowohl für den täglichen Gebrauch, als auch für spezielle Zwecke. Plasma ist die neue Benutzerschnittstelle für die Arbeitsoberfläche von KDE 4 und bietet eine intuitive Oberfläche um mit der Arbeitsoberfläche und Anwendungen zu interagieren. Der Konqueror-Webbrowser integriert das Internet in die Arbeitsumgebung. Der Dolphin-Dateimanager, der Okular-Dokumentbetrachter und das „Systemeinstellungen“-Kontrollzentrum komplettieren den Standard-Desktop. <br />
	KDE basiert auf den KDE-Bibliotheken, welche einfachen Zugriff auf Netzwerkressourcen mittels der KIO-Technologie und erweiterte, grafische Effekte durch die Möglichkeiten von Qt4 bieten. Phonon und Solid, welche auch Teil der KDE-Bibliotheken sind, erweitern alle KDE-Anwendungen um ein Multimedia-System und ermöglichen bessere Integration von Hardware.
</p>




