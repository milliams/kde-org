---
aliases:
- ../../plasma-5.10.1
changelog: 5.10.0-5.10.1
date: 2017-06-06
layout: plasma
youtube: VtdTC2Mh070
figure:
  src: /announcements/plasma/5/5.10.0/plasma-5.10.png
  class: text-center mt-4
asBugfix: true
---

- Fix screen locker incorrect behaviour after pressing enter key with empty password. <a href="https://commits.kde.org/kscreenlocker/23fa33cedfa55cbac83bbdcc514b988d721552dc">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380491">#380491</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6091">D6091</a>
- Make QuickShare plasmoid compatible with Purpose 1.1. <a href="https://commits.kde.org/kdeplasma-addons/27efc1c2abc9c56b7161b77fc558fb77c591d4fe">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380883">#380883</a>
- Fixed crash when dropping files on desktop with KDeclarative from KDE Frameworks 5.35. <a href="https://commits.kde.org/plasma-desktop/77f1e675178ac995f7eb74c0410b5028ca1d74de">Commit.</a> Fixes bug <a href="https://bugs.kde.org/380806">#380806</a>. Phabricator Code review <a href="https://phabricator.kde.org/D6088">D6088</a>
