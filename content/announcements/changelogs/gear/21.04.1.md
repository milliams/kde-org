---
aliases:
- ../../fulllog_releases-21.04.1
- ../releases/21.04.1
title: KDE Gear 21.04.1 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi-calendar" title="akonadi-calendar" link="https://commits.kde.org/akonadi-calendar" >}}
+ Remove some compilation warnings. [Commit.](http://commits.kde.org/akonadi-calendar/1ff4ca3a0c59b062dbe08dfde29b535179cb3932) 
{{< /details >}}
{{< details id="cantor" title="cantor" link="https://commits.kde.org/cantor" >}}
+ Typo. [Commit.](http://commits.kde.org/cantor/95fee6514672cabd3daa1f04aa646b948cb29aae) 
+ Plugins: fix path to server on Windows. [Commit.](http://commits.kde.org/cantor/39f5e2faf7dce569f51bbf7f266e62dbd27d0e00) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Fix padding in places view. [Commit.](http://commits.kde.org/dolphin/56888a567fc741713b6c905aeed3842a7fa230c7) Fixes bug [#435731](https://bugs.kde.org/435731)
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Correct ContextView's footer. [Commit.](http://commits.kde.org/elisa/253a32f0fdbf68a1ef3b8b1f5d46cd87e823b5fa) 
+ [ContextView] Fix visibility of "Show In Folder" button. [Commit.](http://commits.kde.org/elisa/c36a00c149a5117f7da4afeb4998ed60b76ce1f2) 
+ Make contextView's footer as tall as playlistView's one. [Commit.](http://commits.kde.org/elisa/4ecec1a59321bc918aedc28d416a7d295c53a573) 
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Tidy up TimelineView's interface, some day. [Commit.](http://commits.kde.org/eventviews/6f9cb2674a9035a3f3becfcdc83a3ea84bc7450f) 
+ Adjust layout and resizing of the timeline view. [Commit.](http://commits.kde.org/eventviews/9b08cf3d721f84f9bdd2372f5427c1720d4af452) 
+ Initialize the view's preferences. [Commit.](http://commits.kde.org/eventviews/55d91c645ef3457148cff70d92a7a9a73373f169) 
+ Don't crash if an agenda item is deleted while it is being moved. [Commit.](http://commits.kde.org/eventviews/57d3025b146bb9cd3bef2c6eaef77f4970361c1a) Fixes bug [#435352](https://bugs.kde.org/435352)
{{< /details >}}
{{< details id="ffmpegthumbs" title="ffmpegthumbs" link="https://commits.kde.org/ffmpegthumbs" >}}
+ Restore the compatiblity with ffmpeg 3. [Commit.](http://commits.kde.org/ffmpegthumbs/26c1d3b790d621f89cf1cbf56bdc0b8c97a95151) Fixes bug [#430266](https://bugs.kde.org/430266)
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Hide kdoctools_install() behind KF5DocTools_FOUND conditional. [Commit.](http://commits.kde.org/filelight/0db4a8c6b941800d0616e726ebf3f2e751f2293d) 
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ List image/avif among supported mimetypes. [Commit.](http://commits.kde.org/gwenview/acf6a0b483d62699c2e05e9704a828d2ce4d491b) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Exclude kde5_xml_mimetypes.qm from the APK. [Commit.](http://commits.kde.org/itinerary/c38db6a33bdbb89e976febf7834afcdec943149d) 
+ Wrap overly long event venue names. [Commit.](http://commits.kde.org/itinerary/0056160651d8910dc98cac6dff79eb8a9b463646) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ If time format is AM/PM, allow zero hours as intermediate value in time edits. [Commit.](http://commits.kde.org/kalarm/d157febaf3d91ec917aa84d102cd943b49d4cb33) 
+ Constify. [Commit.](http://commits.kde.org/kalarm/7a2db4b7e909310632418364bd09faaaaff1f6a5) 
+ Bug 436558: In alarm edit dialog, enable OK button when date is edited. [Commit.](http://commits.kde.org/kalarm/6992238a3bb1dc51e72f2d057f58524cc06ac876) 
+ Bug 436434: Fix date sometimes being hidden for selected alarms. [Commit.](http://commits.kde.org/kalarm/a70a2aea022840b52967b710ba202af3a020b661) 
+ Bug 436434: In alarm list, don't hide display alarm colour for selected alarms. [Commit.](http://commits.kde.org/kalarm/093949ddb6a037e27813493c7ad86e74bfcc14b1) 
{{< /details >}}
{{< details id="kapman" title="kapman" link="https://commits.kde.org/kapman" >}}
+ Split off KapmanMainWindow::handleGameOver from KapmanMainWindow::newGame. [Commit.](http://commits.kde.org/kapman/3a8f67333152648eb10e34ba3c356b397ccb0e1e) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix possible leak when commiting. [Commit.](http://commits.kde.org/kate/83ec5582204d547bde2fb84526270ff9b1aa4db9) 
+ Fix branch change not reflected if projectbase != dotGit. [Commit.](http://commits.kde.org/kate/3a7d17f556c65d561d19b4354b5133c5d8c0c14d) 
+ Revert "Fix branch change not reflected if projectbase != dotGit". [Commit.](http://commits.kde.org/kate/736d437953f459df521fc225893e74d038dcf9af) 
+ Revert "[addons/project/git/gitutils] Add missing include optional to make bsd happy". [Commit.](http://commits.kde.org/kate/f216e2664bd64f75874354fdf5091822a0d4e776) 
+ [addons/project/git/gitutils] Add missing include optional to make bsd happy. [Commit.](http://commits.kde.org/kate/319f39ef41fe41defc47a5f9cc548d9e872ac61e) 
+ Fix branch change not reflected if projectbase != dotGit. [Commit.](http://commits.kde.org/kate/2dfc012862ac2b9fa7e2b88a6efb266961b58271) 
+ Git: Don't report error if git not found. [Commit.](http://commits.kde.org/kate/2f3da2ca0e3f829163b17b6d7f5f5d423a1b610f) 
+ Fix changing branch doesn't update the branch button. [Commit.](http://commits.kde.org/kate/144dab9446e0c8adabcdc859a219c9add62f7543) 
+ Try to fix of a rare crash with Ctrl + Click in LSP. [Commit.](http://commits.kde.org/kate/e9ff4c923c1a57765b2e16043f06cc9205e42bd9) 
+ Show new mascot in about dialog. [Commit.](http://commits.kde.org/kate/e4a8ec025580b0f1f0ec97192538a0584c3b8118) 
+ Html escape lsptooltip text. [Commit.](http://commits.kde.org/kate/5823df3ec2394c1431f179ddbd730cdf3d986aa2) 
+ Downgrade .git not found warning to info message. [Commit.](http://commits.kde.org/kate/ae0bd0fd387145a4a44d2521e0b456f38b8d4ebe) Fixes bug [#435945](https://bugs.kde.org/435945)
{{< /details >}}
{{< details id="kblocks" title="kblocks" link="https://commits.kde.org/kblocks" >}}
+ Packaging in app repos has been rejected. [Commit.](http://commits.kde.org/kblocks/01fee40dfe11a69b0cc435378115fd94fec5eea6) 
{{< /details >}}
{{< details id="kcalc" title="kcalc" link="https://commits.kde.org/kcalc" >}}
+ Update snap. [Commit.](http://commits.kde.org/kcalc/f2592c922d3258e3cf25d90c71c439dac3259367) 
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Invalidate preview render on subtitle actions. [Commit.](http://commits.kde.org/kdenlive/3ed0dbf11a251e19a74d717ae1161594e395d61d) Fixes bug [#435692](https://bugs.kde.org/435692)
+ Fix timecode validation on settings dialog. [Commit.](http://commits.kde.org/kdenlive/5511f780a3cc525a811ca52b07464a1dfa85a422) 
+ Fix proxied clip cannot be dragged from monitor to timeline. [Commit.](http://commits.kde.org/kdenlive/75c3f0044b01e41d3adadbc75686cf57eba73d89) 
+ Fix incorrect speed cycling with j/l keys. [Commit.](http://commits.kde.org/kdenlive/efab200479a9126efa2f0291f07e58a59b959fdb) 
+ Ensure render widget is displayed again after being minimized. [Commit.](http://commits.kde.org/kdenlive/27ef1e4d973ee1ca5c2948742a34f8e641c2cffe) 
+ Fix playback speed not reset on pause. [Commit.](http://commits.kde.org/kdenlive/1e2494bb7b0160acc89fb31c74e6b48099b0d8f6) 
+ Update effect zones on effect deletion. [Commit.](http://commits.kde.org/kdenlive/43e78433d23a61f3d38f93d53052e894d91bf111) 
+ Render presets: load default values properly to ui. [Commit.](http://commits.kde.org/kdenlive/4c668d8ccc0f407485deff813b63caefb68f33ff) See bug [#421174](https://bugs.kde.org/421174)
+ Fix spacer tool not workin on single clips (without groups). [Commit.](http://commits.kde.org/kdenlive/f559aa5d3351e59ddbd1ddf7eb8a32929d0179ff) 
+ Improve naming of newely created profile. [Commit.](http://commits.kde.org/kdenlive/232c1753d570f1f3f4b6f082e0297bd33c13031b) Fixes bug [#385981](https://bugs.kde.org/385981)
+ Archiver: Fix more bugs and crashes. [Commit.](http://commits.kde.org/kdenlive/05f3314d59f9f94e947a9c0778da55c518a00fad) See bug [#432206](https://bugs.kde.org/432206)
+ Archiver: Block UI while job is running. [Commit.](http://commits.kde.org/kdenlive/62808fae1fe9ab294930909eead704a905e31e53) 
+ Archiver: Don't miss lumas,... on "timline only" mode, prettify code. [Commit.](http://commits.kde.org/kdenlive/3addc5fbf4f5f79bb17d6a8852c084efc96c30a8) 
+ Fix several archiving issues with mlt files. [Commit.](http://commits.kde.org/kdenlive/80dc8d4e7afeaa24fac4cfc45cc219919ecd3eca) Fixes bug [#435882](https://bugs.kde.org/435882)
+ Archive LUT files too. [Commit.](http://commits.kde.org/kdenlive/03c0112172cbfa7fa43345e912de833f565decdd) 
+ Appimage: use mlt v6 branch. [Commit.](http://commits.kde.org/kdenlive/e485c3ce74af3f14aba2d0368038324fd8335c59) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix country field in address-based OSM map links. [Commit.](http://commits.kde.org/kdepim-addons/2300b8fd9d9111fea248e7a1e1d2edf015a0002b) 
+ Add missing closing bold tag for canceled hotel reservations. [Commit.](http://commits.kde.org/kdepim-addons/8ca8e8a2ff13f4db1c56de6002cd914da4c9a368) 
+ Remove dead url. [Commit.](http://commits.kde.org/kdepim-addons/d2e97e9a0ae07c6ec297aca3cfff7ddb4e14e924) 
{{< /details >}}
{{< details id="kig" title="kig" link="https://commits.kde.org/kig" >}}
+ Fix build in C++17 mode. [Commit.](http://commits.kde.org/kig/17c398d9eebfe81e601d37f29eee284e060f060f) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Support building with OpenEXR 3. [Commit.](http://commits.kde.org/kio-extras/80b3335302a790803999a501b0c5cce8ba0d6177) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Detect booking.com cancelations. [Commit.](http://commits.kde.org/kitinerary/0a61bfb8610e7ece23d9e4a7659db74bd4cb9075) 
+ Handle more variants of NH hotel booking confirmation emails. [Commit.](http://commits.kde.org/kitinerary/ece5df2b133fab8fc7593ee5c27fb1d5ead6c3c4) 
{{< /details >}}
{{< details id="kldap" title="kldap" link="https://commits.kde.org/kldap" >}}
+ Hide kdoctools_install() behind KF5DocTools_FOUND conditional. [Commit.](http://commits.kde.org/kldap/d4b3b521cc398d85340075d99a2bf6964f13b544) 
{{< /details >}}
{{< details id="klickety" title="klickety" link="https://commits.kde.org/klickety" >}}
+ Fix "New Game" action always generating a game for value 0. [Commit.](http://commits.kde.org/klickety/41a4acd66aa486f5a32f278f84688e90f91b05be) 
{{< /details >}}
{{< details id="kmahjongg" title="kmahjongg" link="https://commits.kde.org/kmahjongg" >}}
+ Fix "New Game" action always generating a game for value 0. [Commit.](http://commits.kde.org/kmahjongg/2a34cab6a57cc2d4f4417df714e1ee176f38b09f) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Add mailing_list in messagelist view menu too. [Commit.](http://commits.kde.org/kmail/fd28ed2c84d58b95a40377401cff1a82c81bf24b) 
+ Fix Bug 436375 - "Editing as new" selects the wrong transport. [Commit.](http://commits.kde.org/kmail/99c484d2992760405110c6fc014231b1da29212f) Fixes bug [#436375](https://bugs.kde.org/436375)
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Specify desktop-id in appdata file. [Commit.](http://commits.kde.org/konqueror/b8a83df8c68d75a338417509ad44b1da5de20218) 
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix Screen::_cuY position inside screen. [Commit.](http://commits.kde.org/konsole/ba9a21090c6985bfc652afb2e79c19b4ed77d6fb) 
+ Fix crash showing Settings Dialog. [Commit.](http://commits.kde.org/konsole/7b769ff141e9d0f081e81fdf31f2bf44a5849af4) Fixes bug [#436366](https://bugs.kde.org/436366)
+ Add missing include to fix build on gcc-11. [Commit.](http://commits.kde.org/konsole/d7f3fec79d08a74ff66fff0da0c5f92e727cdcb4) 
+ Make CompactHistoryBlock size variable (v21.04.0). [Commit.](http://commits.kde.org/konsole/ed2a9a9e3c3e69f4611c61716f51bb7dc3f5e11f) Fixes bug [#436031](https://bugs.kde.org/436031)
+ Use inverted colours when calculated fancy BG has too low contrast. [Commit.](http://commits.kde.org/konsole/2b441030a745a17f76d8f010a299a74532e3f211) Fixes bug [#435309](https://bugs.kde.org/435309)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Clazy warning:  container inside loop. [Commit.](http://commits.kde.org/korganizer/3c21cacce232f9f07b74470fbb6eb221aac82788) 
+ Replace some SIGNAL and SLOT macros. [Commit.](http://commits.kde.org/korganizer/3a388a6e01dccf069416440448d7a8b349f25de2) 
+ Replace some SLOT macros. [Commit.](http://commits.kde.org/korganizer/83f86296e0eb9d29675bbc31956107dd00b268e8) 
+ Replace some SIGNAL macros. [Commit.](http://commits.kde.org/korganizer/e78a4ea649e8adbe2187d9248794c26855580c92) 
+ Replace some SIGNAL macros. [Commit.](http://commits.kde.org/korganizer/b9c0ac2d500d23f48f5c1e7e030503f8fe3a02d8) 
+ Replace some SIGNAL macros. [Commit.](http://commits.kde.org/korganizer/5e85652cbf08e63064c6b143f7166efa5e515f7a) 
+ Replace some SIGNAL macros. [Commit.](http://commits.kde.org/korganizer/66a2fe53903eb6c69ccb2361415470f02f302d18) 
+ Replace some SIGNAL macros. [Commit.](http://commits.kde.org/korganizer/3bd22dd4c80f9fea7d5594b9e94287aca3d3b457) 
+ Replace some SIGNAL macros. [Commit.](http://commits.kde.org/korganizer/0d67cdcc465366a43938a60c246c353c3878ba99) 
+ Replace some SIGNAL macros. [Commit.](http://commits.kde.org/korganizer/c1814d2fc7c4001b45dd4b9df28a5c3a3fd1644b) 
+ Replace some SIGNAL macros. [Commit.](http://commits.kde.org/korganizer/530ac630343b5f32b8d87b29cd9e7abcf154f2ba) 
+ Replace some SIGNAL and SLOT macros. [Commit.](http://commits.kde.org/korganizer/bd675eaa538b442252e1046719537677861f3d16) 
+ Replace some SIGNAL and SLOT macros. [Commit.](http://commits.kde.org/korganizer/a67b02d36f01369fd264df400c27192e47bd4d75) 
+ Replace some SIGNAL and SLOT macros. [Commit.](http://commits.kde.org/korganizer/05f2446f79b6e5cb0cb5df415be3497e0a8005b8) 
+ Initialize the timeline view's preferences. [Commit.](http://commits.kde.org/korganizer/56502d733630e04bd8e2a0fbf512f31082fbdb96) 
+ Fix some compiler warnings. [Commit.](http://commits.kde.org/korganizer/f35980ab9b5e25f3ab1d322349cbb338a41cf5a9) 
{{< /details >}}
{{< details id="kpat" title="kpat" link="https://commits.kde.org/kpat" >}}
+ Fix ace auto move crash. [Commit.](http://commits.kde.org/kpat/638d8c2c6b47054bcf3deece3b065ac5c2fb01d8) Fixes bug [#436181](https://bugs.kde.org/436181)
+ Fix unitialized value sometimes causing solver to exit and report game as unsolvable. [Commit.](http://commits.kde.org/kpat/428375cd2780eb00883b268d470a6e366040f70f) 
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Fix cmake policy CMP0115 warning. [Commit.](http://commits.kde.org/kpublictransport/c852745020325131860b0f2a7dffcad5e30ae5cd) 
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Fix Bug 436687 - Folder Properties Dialog is broken. [Commit.](http://commits.kde.org/mailcommon/a91ee8b0a7338e25e87c93940997aa1861537a0a) Fixes bug [#436687](https://bugs.kde.org/436687)
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Fix CVE-2021-31855. [Commit.](http://commits.kde.org/messagelib/3b5b171e91ce78b966c98b1292a1bcbc8d984799) 
+ Minor: Fix compiler warning. [Commit.](http://commits.kde.org/messagelib/99b50c22b0e6ab37c100ba1e2e18218c11fa7e3c) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Try to stabilize test. [Commit.](http://commits.kde.org/okular/be1f0de1960bc2e6c3c5f698562c49e01701a003) 
+ Fix saving signed revisions of a signature. [Commit.](http://commits.kde.org/okular/72658e0ec389338baccd78af97183427e247636a) 
+ Fix bug: select browse tool after trim select. [Commit.](http://commits.kde.org/okular/f6b58ed804b70a953888f3c009c9fd351fb4c913) 
+ Fix: Viewport coords normalized w.r.t. full page. [Commit.](http://commits.kde.org/okular/61551d2356ea5f93ba3e921a1c4ab9cca88fdbda) See bug [#198427](https://bugs.kde.org/198427)
{{< /details >}}
{{< details id="palapeli" title="palapeli" link="https://commits.kde.org/palapeli" >}}
+ Drop bogus Qt5::Concurrent from target_link_libraries. [Commit.](http://commits.kde.org/palapeli/30ff07fd12c0767ed033e71d2226053115963401) Fixes bug [#436132](https://bugs.kde.org/436132)
{{< /details >}}
{{< details id="partitionmanager" title="partitionmanager" link="https://commits.kde.org/partitionmanager" >}}
+ Fix appstream link. [Commit.](http://commits.kde.org/partitionmanager/881d86b3c09ba7e3392a40156872abac601581ef) 
{{< /details >}}
{{< details id="pimcommon" title="pimcommon" link="https://commits.kde.org/pimcommon" >}}
+ Use correct config name. [Commit.](http://commits.kde.org/pimcommon/a0d5b0e1536cf9a887e8b321a64de225588e33c6) 
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Fix crash reported in https://bugs.kde.org/show_bug.cgi?id=338649#c20. [Commit.](http://commits.kde.org/umbrello/7d3eb053739413a0eef8c111e0c9a41843227cf2) See bug [#338649](https://bugs.kde.org/338649)
+ Lib/cppparser/lexer.{h,cpp} cosmetics and error detection improvement:. [Commit.](http://commits.kde.org/umbrello/4cea2f59e6b62bd0fd5686b4747fef5acbb3fbf0) 
+ Lib/cppparser/driver.cpp cosmetics in class Driver::ParseHelper :. [Commit.](http://commits.kde.org/umbrello/af296cce784d6e169bce3022b6ed135b5db1353b) 
+ Support C++11 "enum class" in C++ Import:. [Commit.](http://commits.kde.org/umbrello/464c7132349e4cf10a84fc9e6f3792087a934158) See bug [#338649](https://bugs.kde.org/338649)
+ Compile on Qt4: QStringLiteral -> QLatin1String. [Commit.](http://commits.kde.org/umbrello/dd912a2df31b1179b3eb4d6b706a1bf6d9aa5d26) 
+ Lib/cppparser/README. [Commit.](http://commits.kde.org/umbrello/24f19c94137ceb6e6a04540dcdd5d1dfaa66d59e) 
+ Test/import/cxx/namespace-enum-literal.h : Document semicolon after end of namespace. [Commit.](http://commits.kde.org/umbrello/5892e7e14438d400f4df52e8b6c20f1633bee34e) 
+ Test/import/cxx/comments-class-enums.h: Fix typo in comment, add newline at EOF. [Commit.](http://commits.kde.org/umbrello/845a56144180546e9e5d04ab0b2459c3974f7083) 
{{< /details >}}
