2008-02-02 13:27 +0000 [r769952]  woebbe

	* branches/KDE/4.0/kdemultimedia/juk/CMakeLists.txt: enable Qt's
	  STL support to make TStringToQString work

2008-02-03 00:50 +0000 [r770207]  mpyne

	* branches/KDE/4.0/kdemultimedia/juk/CMakeLists.txt: Some are still
	  having build failures, maybe simply removing QT_NO_STL isn't
	  enough to get juk to build?

