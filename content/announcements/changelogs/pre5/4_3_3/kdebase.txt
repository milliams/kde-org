------------------------------------------------------------------------
r1030455 | segato | 2009-10-02 10:31:54 +0000 (Fri, 02 Oct 2009) | 4 lines

backport r1030454
don't crash if no theme is selected
CCBUG:208649

------------------------------------------------------------------------
r1030474 | lmurray | 2009-10-02 10:52:01 +0000 (Fri, 02 Oct 2009) | 5 lines

Backport r1030473:
Prevent KWin from sending out multiple sync requests before the client
has time to reply.
CCBUG: 183263

------------------------------------------------------------------------
r1030495 | mueller | 2009-10-02 11:46:41 +0000 (Fri, 02 Oct 2009) | 2 lines

bump version

------------------------------------------------------------------------
r1030579 | dfaure | 2009-10-02 15:53:30 +0000 (Fri, 02 Oct 2009) | 2 lines

backport input validation fix

------------------------------------------------------------------------
r1030705 | scripty | 2009-10-03 03:14:25 +0000 (Sat, 03 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1030720 | lmurray | 2009-10-03 07:09:35 +0000 (Sat, 03 Oct 2009) | 4 lines

Backport r1030718:
Prevent jerkiness of the first two frames of the logout blur effect by
starting the animation after the render target has been created.

------------------------------------------------------------------------
r1030835 | lmurray | 2009-10-03 09:14:20 +0000 (Sat, 03 Oct 2009) | 5 lines

Backport r1030828:
Fix double->int conversions to prevent incorrect rounding of window
opacity at KWin start and window focus change.
CCBUG: 209274

------------------------------------------------------------------------
r1030846 | lmurray | 2009-10-03 10:02:13 +0000 (Sat, 03 Oct 2009) | 5 lines

Backport r1030845:
Use correct fullscreen geometry when moving fullscreen windows across
Xinerama screens.
CCBUG: 188827

------------------------------------------------------------------------
r1030882 | aacid | 2009-10-03 12:25:03 +0000 (Sat, 03 Oct 2009) | 5 lines

Backport r1030881 | aacid | 2009-10-03 14:23:09 +0200 (Sat, 03 Oct 2009) | 3 lines

[Try to] fix the description of the georgian locale
BUGS: 190431

------------------------------------------------------------------------
r1030908 | ilic | 2009-10-03 13:21:33 +0000 (Sat, 03 Oct 2009) | 1 line

Updated Russian locale according to Andrey Cherepanov. (bport: 1030907)
------------------------------------------------------------------------
r1030922 | lmurray | 2009-10-03 14:09:24 +0000 (Sat, 03 Oct 2009) | 5 lines

Backport r1030921:
Don't send sync requests when using the rubber band for window resizing.
CCBUG: 181800
CCBUG: 183263

------------------------------------------------------------------------
r1030933 | lmurray | 2009-10-03 14:39:26 +0000 (Sat, 03 Oct 2009) | 5 lines

Backport r1030932:
Detect when _MOTIF_WM_HINTS gains or loses the no border hint.
Patch by Daniel Erat.
CCBUG: 201523

------------------------------------------------------------------------
r1030941 | lmurray | 2009-10-03 15:02:26 +0000 (Sat, 03 Oct 2009) | 5 lines

Backport r1030940:
Activate the desktop under the mouse cursor instead of the highlighted
one in the Desktop Grid effect.
CCBUG: 207271

------------------------------------------------------------------------
r1030962 | fredrik | 2009-10-03 15:57:12 +0000 (Sat, 03 Oct 2009) | 1 line

Backport r1030959.
------------------------------------------------------------------------
r1031131 | scripty | 2009-10-04 03:00:41 +0000 (Sun, 04 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1031355 | mzanetti | 2009-10-04 19:26:03 +0000 (Sun, 04 Oct 2009) | 2 lines

backport of bugfix commit 1031353

------------------------------------------------------------------------
r1031482 | trueg | 2009-10-05 09:28:01 +0000 (Mon, 05 Oct 2009) | 1 line

Backport: fixed context markers in labels
------------------------------------------------------------------------
r1031783 | scripty | 2009-10-06 02:59:25 +0000 (Tue, 06 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1031926 | trueg | 2009-10-06 13:10:26 +0000 (Tue, 06 Oct 2009) | 1 line

Backport: properly fail to initialize if the soprano model could not be created
------------------------------------------------------------------------
r1032180 | freininghaus | 2009-10-07 07:52:18 +0000 (Wed, 07 Oct 2009) | 7 lines

Revert the changes to TerminalPanel from r969265 in the 4.3
branch. This makes problems with "automatic cd-ing" in the Terminal
Panel reappear, but fixes a regression which caused a lot of trouble.

CCBUG: 167810
CCBUG: 202176

------------------------------------------------------------------------
r1032328 | dfaure | 2009-10-07 14:51:20 +0000 (Wed, 07 Oct 2009) | 2 lines

backport from trunk: make dnd work

------------------------------------------------------------------------
r1032897 | lunakl | 2009-10-08 20:28:40 +0000 (Thu, 08 Oct 2009) | 5 lines

backport r1032893
kdeglobals no longer stores Desktop and Document paths,
they are accessed using QDesktopServices/xdg-user-dirs


------------------------------------------------------------------------
r1032941 | bcooksley | 2009-10-08 22:09:12 +0000 (Thu, 08 Oct 2009) | 2 lines

Reduce minimum window size as already done in Trunk.
BUG: 209867
------------------------------------------------------------------------
r1032978 | scripty | 2009-10-09 03:12:18 +0000 (Fri, 09 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1033384 | kossebau | 2009-10-10 00:40:25 +0000 (Sat, 10 Oct 2009) | 2 lines

backport of 1033383: fixed: splitting the servicetype suffix was off by one

------------------------------------------------------------------------
r1033416 | djarvie | 2009-10-10 09:29:14 +0000 (Sat, 10 Oct 2009) | 2 lines

Fix punctuation in i18n strings

------------------------------------------------------------------------
r1033688 | darioandres | 2009-10-10 19:29:06 +0000 (Sat, 10 Oct 2009) | 4 lines

Backport to 4.3:
- Fix a possible crash when deleting items + Group all the deletions in one job


------------------------------------------------------------------------
r1033757 | hindenburg | 2009-10-10 23:16:47 +0000 (Sat, 10 Oct 2009) | 7 lines

Fix issues with reading /proc/pid/status and use thread safe getpwuid_r.

Backport - already in trunk

BUG: 209544


------------------------------------------------------------------------
r1033830 | ossi | 2009-10-11 09:44:29 +0000 (Sun, 11 Oct 2009) | 4 lines

download wallpapers to system location, otherwise kdm won't find them.

BUG: 146154

------------------------------------------------------------------------
r1033882 | ossi | 2009-10-11 13:28:38 +0000 (Sun, 11 Oct 2009) | 5 lines

fix style, color and font settings being ignored

BUG: 182870


------------------------------------------------------------------------
r1033991 | hindenburg | 2009-10-11 17:55:49 +0000 (Sun, 11 Oct 2009) | 1 line

update version
------------------------------------------------------------------------
r1034013 | freininghaus | 2009-10-11 19:03:33 +0000 (Sun, 11 Oct 2009) | 10 lines

Make sure that the faked mouse event in
DolphinViewAutoScroller::scrollViewport() gets the right keyboard
modifiers. This fixes the problem that the previous selection is lost
when several Ctrl-rubber band selections are made and the view scrolls
or the mouse pointer gets close to the edge of the viewport.

CCBUG: 190703

Fix will be in KDE 4.3.3.

------------------------------------------------------------------------
r1034476 | mart | 2009-10-12 20:10:36 +0000 (Mon, 12 Oct 2009) | 2 lines

backport fix to 202799

------------------------------------------------------------------------
r1034944 | cfeck | 2009-10-14 00:18:17 +0000 (Wed, 14 Oct 2009) | 4 lines

Fix possible crash (backport r1034943)

CCBUG: 210497

------------------------------------------------------------------------
r1035377 | aseigo | 2009-10-14 21:11:15 +0000 (Wed, 14 Oct 2009) | 3 lines

backport r1029419 since no issues have been reported with it; this should also resolve remaining issues associated with bug 199325. testing by people using this branch would be most appreciated.
BUG:199325

------------------------------------------------------------------------
r1035637 | gkiagia | 2009-10-15 15:02:42 +0000 (Thu, 15 Oct 2009) | 3 lines

Backport r1035627.
Fix the backtrace parser to work with backtraces in freebsd.

------------------------------------------------------------------------
r1035642 | lunakl | 2009-10-15 15:11:47 +0000 (Thu, 15 Oct 2009) | 3 lines

backport r1035639, xdg-user-dirs paths must be in ""


------------------------------------------------------------------------
r1035860 | scripty | 2009-10-16 03:16:18 +0000 (Fri, 16 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1036044 | dfaure | 2009-10-16 11:50:29 +0000 (Fri, 16 Oct 2009) | 3 lines

Fix "Adding tab (Ctrl+T) in detached (tab) window crashes Konqueror".
Excellent investigation, unit-test, and suggested fixes by Frank Reininghaus. BUG 203069.

------------------------------------------------------------------------
r1036097 | adawit | 2009-10-16 15:24:30 +0000 (Fri, 16 Oct 2009) | 1 line

Make it compile. webview.h is no longer installed. Use QWebView instead.
------------------------------------------------------------------------
r1036400 | bcooksley | 2009-10-17 09:31:37 +0000 (Sat, 17 Oct 2009) | 2 lines

Backport fix.
CCBUG: 194917
------------------------------------------------------------------------
r1036734 | trueg | 2009-10-17 17:26:06 +0000 (Sat, 17 Oct 2009) | 1 line

backport: xesam is deprecated
------------------------------------------------------------------------
r1037397 | hindenburg | 2009-10-18 22:18:18 +0000 (Sun, 18 Oct 2009) | 4 lines

Fix issue where Shift+Tab would focus tab bar.

CCBUG: 157039

------------------------------------------------------------------------
r1037426 | scripty | 2009-10-19 03:22:16 +0000 (Mon, 19 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1038228 | fredrik | 2009-10-20 21:10:19 +0000 (Tue, 20 Oct 2009) | 1 line

Backport r1038223.
------------------------------------------------------------------------
r1038316 | scripty | 2009-10-21 03:07:36 +0000 (Wed, 21 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1038603 | trueg | 2009-10-21 13:42:59 +0000 (Wed, 21 Oct 2009) | 1 line

Backport: do not toggle between suspended and non-suspended when getting the resume signal multiple times in a row
------------------------------------------------------------------------
r1038723 | darioandres | 2009-10-21 18:08:21 +0000 (Wed, 21 Oct 2009) | 5 lines

Backport to 4.3 of svn rev.1038720:
- Fix the KIcon name of the restart action

BUG: 211361

------------------------------------------------------------------------
r1039058 | markuss | 2009-10-22 16:07:23 +0000 (Thu, 22 Oct 2009) | 1 line

Fix missing icons
------------------------------------------------------------------------
r1040024 | freininghaus | 2009-10-25 10:48:59 +0000 (Sun, 25 Oct 2009) | 9 lines

Do not zoom the icons if the user presses Control and the left mouse
button while using the mouse wheel. The user is probably trying to
scroll the view during a rubberband selection in that case.

This will be in KDE 4.3.3.

CCBUG: 190703


------------------------------------------------------------------------
r1040313 | scripty | 2009-10-26 04:16:38 +0000 (Mon, 26 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1041452 | cfeck | 2009-10-28 02:31:25 +0000 (Wed, 28 Oct 2009) | 6 lines

Support non-ASCII password and fullname (backport r1041451)

Fix will be in KDE 4.3.3

CCBUG: 212004

------------------------------------------------------------------------
r1041530 | scripty | 2009-10-28 04:28:17 +0000 (Wed, 28 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1042071 | scripty | 2009-10-29 04:20:00 +0000 (Thu, 29 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1042475 | mueller | 2009-10-29 21:39:20 +0000 (Thu, 29 Oct 2009) | 2 lines

bump version

------------------------------------------------------------------------
r1042535 | lmurray | 2009-10-30 01:39:30 +0000 (Fri, 30 Oct 2009) | 4 lines

Backport r1042534:
Correctly handle pre-multiplied textures in the invert effect.
CCBUG: 212339

------------------------------------------------------------------------
r1042551 | scripty | 2009-10-30 04:03:13 +0000 (Fri, 30 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
