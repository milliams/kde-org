---
aliases:
- ../changelog3_1_5to3_2
hidden: true
title: KDE 3.1.5 to KDE 3.2.0 Changelog
---

<p>
This page tries to present as much as possible the additions
and corrections that occured in KDE between the 3.1.5 and 3.2.0 releases.
</p>

<h2>Libraries</h2>
	<ul>
		<li>Hidden methods in DCOP interfaces - DCOPIDLng <em>Alexander Kellett &lt;&#00108;yp&#0097;no&#118;&#64;&#107;de&#46;o&#0114;g&gt;</em></li>
		<li>Documentation parsing (kdoc based) and inclusion in generated .dcopidl files (note: kdebindings dcopidlng only) <em>Alexander Kellett &lt;&#108;yp&#97;&#110;o&#118;&#064;k&#x64;&#101;&#x2e;org&gt;</em></li>
		<li>Kalyptus based rewrite of DCOPIDL - DCOPIDLng <em>Alexander Kellett &lt;&#x6c;&#121;p&#x61;&#x6e;&#x6f;&#x76;&#x40;k&#100;e&#0046;o&#x72;g&gt;</em></li>
		<li>Thumbnail generator for DVI files. <em>Stefan Kebekus &lt;k&#0101;&#0098;&#x65;ku&#x73;&#x40;k&#x64;&#x65;&#x2e;&#0111;rg&gt;</em></li>
		<li>Thumbnail generator for Xcursor files. <em>Fredrik Hoeglund</em></li>
		<li>KMainWindow can now maintain the statusbar and corresponding action (duplicate (99% of the time broken) code removed from countless applications in KDE). <em>Benjamin Meyer</em></li>
		<li>KMainWindow, KToolbar, and several other core classes now will only stores settings if those settings are not the default values (helping to reduce the huge size of config files). <em>Benjamin Meyer</em></li>
		<li>Add support for International Domain Names (IDN). <em>Thiago Macieira, Waldo Bastian &lt;&#98;&#x61;st&#00105;a&#x6e;&#x40;k&#100;e.o&#00114;g&gt;</em></li>
		<li>KDE Password Registry and Wallet. <em>George Staikos &lt;s&#x74;&#97;ik&#111;s&#64;kd&#00101;.&#111;r&#0103;&gt;</em></li>
		<li>SSL session ID reuse. <em>George Staikos &lt;s&#116;a&#x69;&#0107;&#111;s&#x40;kde.&#111;&#x72;&#x67;&gt;</em></li>
		<li>KCookiejar: Support HttpOnly cookies. <em>Waldo Bastian &lt;&#0098;a&#115;&#116;i&#97;n&#0064;kd&#x65;.&#x6f;&#x72;&#x67;&gt;</em></li>
		<li>KLocale: support for different calendar systems <em>Hans Petter Bieker &lt;&#x62;ieke&#x72;&#x40;&#x6b;de.or&#0103;&gt;</em></li>
		<li>KCalendarSystem: support for Hebrew calendar system <em>Hans Petter Bieker &lt;&#00098;&#x69;e&#00107;&#101;&#114;&#64;kde.&#111;&#114;g&gt;</em></li>
		<li>Support data urls.
        See <a href="http://www.ietf.org/rfc/rfc2397.txt" title="The &quot;data&quot; URL scheme">rfc 2397</a>.
       <em>Leo Savernik &lt;l.savernik at aon.at&gt;</em></li>
		<li>New tab widget classes KTabBar and KTabWidget in kdeui <em>Stephan Binner &lt;&#98;i&#110;&#110;&#101;&#x72;&#64;&#107;&#x64;&#00101;&#46;org&gt;</em></li>
		<li>New KFile permissions panel <em>Tim Jansen &lt;tim@tjansen.de&gt;</em></li>
		<li>Support for mimetype inheritance and aliases <em>David Faure &lt;&#00102;a&#x75;r&#101;&#64;kd&#101;.&#111;r&#103;&gt;</em></li>
		<li>KDialogBase: Replace setButtonOkText() &amp; co. with
        setButtonOk() that takes a KGuiItem instead (deprecating the
        old methods) <em>Martijn Klingens &lt;&#0107;li&#0110;&#x67;&#x65;&#x6e;&#115;&#00064;kd&#x65;.org&gt;</em></li>
		<li>Move KPrefs from libkdepim to kdelibs. <em>Cornelius Schumacher &lt;s&#x63;h&#0117;&#109;ac&#104;&#x65;r&#64;k&#x64;e.&#0111;r&#103;&gt;, Waldo Bastian &lt;bastian@kde.org&gt;</em></li>
		<li>Support for &quot;extra fields&quot; in KIO slaves and in Konqueror / KFileDialog <em>David Faure &lt;fau&#00114;&#101;&#x40;&#107;d&#101;.&#0111;&#x72;g&gt;</em></li>
		<li>Add KSplashScreen, a splash screen class based on QSplashScreen (with support for KDE's Xinerama options) <em>Chris Howells &lt;&#104;&#00111;we&#108;&#108;&#0115;&#64;kd&#101;.&#x6f;r&#x67;&gt;</em></li>
		<li>KPasteTextAction: enhanced action for pasting text by offering a list with the clipboard history. <em>Andras Mantia &lt;ama&#x6e;&#116;ia&#x40;k&#100;&#101;.o&#114;&#00103;&gt;</em></li>
	<li><h2>KHTML</h2>
	<ul>
		<li>Non-modal find dialog <em>David Faure &lt;faure&#x40;&#00107;&#100;e.o&#00114;&#x67;&gt;</em></li>
	</ul>

    </li>
    <li><h2>KDEfx</h2>
    <ul>
    	<li>Added KCPUInfo class which makes it possible for applications to do runtime checks for architecture specific CPU features, such MMX, 3DNow! and AltiVec. <em>Fredrik Hoeglund</em></li>
    	<li>Added MMX and SSE2 optimizations to the blending functions in KImageEffect, that among other things are used to blend the translucent menus when the menu effect is set to software blend, and to tint icons when they're selected in icon views. <em>Fredrik Hoeglund</em></li>
    </ul>

    </li>
    <li><h2>Kate Part</h2>
    <ul>
    	<li>File type specific settings <em>Christoph Cullmann &lt;cu&#x6c;&#x6c;m&#00097;&#x6e;&#110;&#00064;&#107;d&#x65;.&#111;r&#x67;&gt;</em></li>
    	<li>vi style cmd line <em>Christoph Cullmann &lt;cul&#108;&#x6d;ann&#0064;k&#00100;e.&#111;r&#103;&gt;</em></li>
    	<li>Support variable lines in files <em>Anders Lund &lt;anders.lund@lund.tdcadsl.dk&gt;</em></li>
    	<li>Color Schemes <em>Christoph Cullmann &lt;&#x63;&#x75;&#108;&#00108;m&#x61;nn&#64;k&#x64;&#101;&#x2e;or&#x67;&gt;</em></li>
    	<li>Indentation based code folding <em>Christoph Cullmann &lt;cul&#x6c;&#x6d;a&#110;n&#64;k&#x64;e.&#00111;rg&gt;</em></li>
    	<li>Implement more DCOP interfaces and clean up the
          ktexteditor dcop interfaces <em>Christoph Cullmann &lt;cu&#108;lm&#0097;n&#x6e;&#0064;k&#x64;e&#x2e;&#x6f;&#x72;&#0103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KImgIO</h2>
    <ul>
    	<li>Add PCX image plugin, supports reading and writing 1, 4,
        8 and 24bpp images <em>Nadeem Hasan &lt;nhas&#97;n&#x40;k&#x64;e&#x2e;o&#x72;&#00103;&gt;</em></li>
    	<li>Add TGA image plugin, supports reading and writing
        true color TGA images (compressed and uncompressed) <em>Dominik Seichter &lt;domseichter@web.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>Interfaces</h2>
    <ul>
    	<li>Interfaces for KBytesEdit widget part <em>Friedrich W. H. Kossebau &lt;Friedrich.W.H@Kossebau.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>KSpell</h2>
    <ul>
    	<li>Adding ability to spell check HTML, TeX and nroff documents. <em>Zack Rusin &lt;za&#00099;k&#00064;k&#x64;e.&#111;rg&gt;</em></li>
    	<li>Completely reworking the spell checking dialog. Adding language
          selection combo. <em>Zack Rusin &lt;zac&#x6b;&#x40;kde&#046;o&#x72;&#x67;&gt;</em></li>
    </ul>

    </li>
    </ul>

<h2>Base</h2>
	<ul>
	<li><h2>Kate</h2>
	<ul>
		<li>Moving KATE view management over to KMDI <em>Joseph Wenninger &lt;&#106;&#111;&#x77;&#x65;n&#x6e;&#x40;kde&#46;or&#103;&gt;</em></li>
		<li>Basic project managment <em>Christoph Cullmann &lt;cu&#108;&#x6c;m&#x61;&#x6e;n&#064;k&#x64;&#00101;&#46;&#x6f;rg&gt;</em></li>
	</ul>

    </li>
    <li><h2>KControl</h2>
    <ul>
    	<li>Xinerama KControl module, enhanced KDE Xinerama and dual-head support <em>George Staikos &lt;st&#97;ik&#111;&#x73;&#x40;k&#100;&#101;&#46;&#00111;r&#103;&gt;</em></li>
    	<li>add/update control module for configuring per-domain settings.
        See <a href="http://bugs.kde.org/show_bug.cgi?id=49145" title="RFE: Access Control over JavaScript Properties Globally and Per-Domain">Bug 49145</a>.
         <em>Leo Savernik &lt;l.savernik at aon.at&gt;</em></li>
    	<li>Support for per-style configuration settings in the style control center module, a module for Keramik <em>Maksim Orlovich &lt;orlovich at cs.rochester.edu&gt;</em></li>
    	<li>Add support for installing/removing- and switching Xcursor themes to the mouse KControl module <em>Fredrik Hoeglund</em></li>
    	<li>Font installer - create fonts:/ ioslave, and modify KControl module to use this.
        See http://members.lycos.co.uk/cpdrummond for (old) screenshots/download <em>Craig Drummond &lt;&#x63;&#x72;&#97;i&#0103;&#64;k&#x64;&#x65;&#x2e;org&gt;</em></li>
    </ul>

    </li>
    <li><h2>KHotKeys</h2>
    <ul>
    	<li>Update to version 2.0, which includes more features like mouse gestures. <em>Lubos Lunak &lt;&#108;.&#x6c;una&#x6b;&#64;k&#0100;e&#0046;org&gt;</em></li>
    </ul>

    </li>
    <li><h2>KWin</h2>
    <ul>
    	<li>Full support for latest NETWM spec version in KWin, and seamless usage of other compliant window managers with KDE. <em>Lubos Lunak &lt;l&#046;lunak&#x40;k&#0100;&#x65;&#0046;o&#114;&#103;&gt;</em></li>
    	<li>New API for window decoration plugins. <em>Lubos Lunak &lt;l.&#x6c;unak&#00064;k&#100;&#101;&#x2e;&#111;&#114;g&gt;</em></li>
    	<li>Focus stealing prevention. <em>Lubos Lunak &lt;&#x6c;&#46;&#x6c;&#0117;nak&#0064;k&#00100;e.&#0111;r&#103;&gt;</em></li>
    	<li>Add an option to allow wide borders to some of the window decoration styles <em>Gunnar Schmi DT &lt;gunnar@schmi-dt.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>Kicker</h2>
    <ul>
    	<li>Make addition of non-KDE applications to Kicker easier <em>Alexander Kellett &lt;l&#x79;p&#x61;&#x6e;&#111;&#x76;&#64;kd&#x65;&#46;or&#103;&gt;</em></li>
    	<li>Enable backgrounds for child panels and all applets, allow for colorization of background pixmaps <em>Aaron J. Seigo &lt;&#97;&#x73;&#x65;i&#103;&#111;&#64;kde&#46;&#x6f;&#00114;&#x67;&gt;</em></li>
    	<li>Allow applets to export a menu to the handle <em>Aaron J. Seigo &lt;&#x61;&#x73;&#00101;&#105;g&#00111;&#64;&#0107;d&#101;&#x2e;o&#114;&#103;&gt;</em></li>
    	<li>User defined plain colors for tiles <em>Aaron J. Seigo &lt;a&#00115;&#00101;i&#00103;o&#064;kde&#046;o&#114;&#x67;&gt;</em></li>
    	<li>Make the panel transparent. <em>Zack Rusin &lt;z&#x61;&#00099;&#107;&#x40;&#x6b;&#100;e&#046;or&#x67;&gt;</em></li>
    	<li>Add an option to the RMB-menu and to the &quot;Advanced Options&quot; in kcontrol to
        enable/disable the panel handle when &quot;Custom&quot; size is selected. <em>&lt;gjwucherpfennig at gmx.net&gt;</em></li>
    </ul>

    </li>
    <li><h2>KDM</h2>
    <ul>
    	<li>More sophisticated session definitions, common with GDM <em>Oswald Buddenhagen &lt;o&#x73;&#x73;&#105;&#064;k&#100;&#00101;.&#111;&#114;&#0103;&gt;</em></li>
    	<li>Pluggable greeter input methods; better architectural compliance with PAM's input model. <em>Oswald Buddenhagen &lt;o&#x73;&#115;&#105;&#64;k&#100;&#x65;&#x2e;or&#103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>Various (kwin, kdesktop, kcontrol, ksmserver)</h2>
    <ul>
    	<li>Optimally support the X Resize and Rotate extension. <em>Hamish Rodda &lt;r&#x6f;&#x64;da&#00064;&#107;&#x64;&#x65;.&#0111;&#114;g&gt;</em></li>
    </ul>

    </li>
    <li><h2>All KDE applications (kdebase/kdenetwork/kdepim/kdegraphics,...)</h2>
    <ul>
    	<li>No absolute paths to user home directories in KDE config files. This allows to support roaming users. <em>Helge Deller &lt;&#100;&#101;&#00108;l&#0101;&#x72;&#64;kde&#x2e;o&#x72;&#0103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>Konsole</h2>
    <ul>
    	<li>Add a better interface to konsolepart for other apps. <em>Dominique Devriese &lt;d&#00101;vr&#x69;&#x65;&#x73;e&#0064;kd&#101;&#046;o&#114;&#103;&gt;</em></li>
    	<li>Added &quot;Set Selection End&quot; to right mouse button menu. <em></em></li>
    	<li>Column text selecting when Ctrl and Alt are pressed <em></em></li>
    	<li>Uses now KTabWidget, allows tab context menus and more <em>Stephan Binner &lt;&#0098;i&#x6e;&#0110;er&#00064;&#x6b;d&#00101;.&#111;r&#x67;&gt;</em></li>
    	<li>Utilize KNotify (e.g. passive popup) for &quot;Activity&quot;, &quot;Bell&quot;, &quot;Shell Exit&quot; and &quot;Silence&quot; events <em></em></li>
    	<li>ZModem up- and download (requires rzsz) <em>Waldo Bastian &lt;&#x62;a&#115;&#116;i&#0097;n&#0064;kde&#x2e;o&#114;&#x67;&gt;</em></li>
    	<li>Make bidi rendering (for RTL languages) configurable. <em>Meni Livne &lt;&#108;&#x69;v&#x6e;&#x65;&#064;&#x6b;&#00100;e.o&#x72;g&gt;</em></li>
    </ul>

    </li>
    <li><h2>KEditBookmarks</h2>
    <ul>
    	<li>Export to HTML and printing of bookmark collection. <em>Alexander Kellett &lt;ly&#112;a&#0110;ov&#0064;kde&#x2e;or&#103;&gt;</em></li>
    	<li>Recursive favicon updates in bookmark editor. <em>Alexander Kellett &lt;&#00108;&#121;&#x70;&#97;n&#x6f;&#118;&#00064;&#00107;&#100;e&#046;o&#114;g&gt;</em></li>
    	<li>Field for incremental searching on titles. <em>Alexander Kellett &lt;l&#121;&#x70;&#97;nov&#x40;&#0107;de.&#111;rg&gt;</em></li>
    	<li>Opera and Internet Explorer bookmark collection exporting. <em>Alexander Kellett &lt;ly&#x70;ano&#118;&#0064;&#107;&#x64;e&#00046;org&gt;</em></li>
    	<li>Bookmark editor now includes an extra pane to allow title/url editing without use of context menu / keyboard shortcuts. <em>Alexander Kellett &lt;l&#121;p&#x61;&#110;ov&#64;k&#x64;&#x65;.&#x6f;&#114;g&gt;</em></li>
    	<li>More detailed bookmark access information (last viewed, first seen, view counts). <em>Alexander Kellett &lt;&#108;y&#x70;&#x61;&#110;&#111;v&#x40;k&#100;e.&#111;&#x72;g&gt;</em></li>
    	<li>Recursive - entire collection, per folder - alphabetical sorting <em>Alexander Kellett &lt;l&#121;&#x70;a&#00110;&#111;&#118;&#x40;&#107;&#100;&#00101;&#x2e;&#111;&#114;g&gt;</em></li>
    </ul>

    </li>
    <li><h2>Konqueror</h2>
    <ul>
    	<li>Enhanced Konqueror statusbar and kpart statusbar extension. Statusbar notifications for secure connections, wallet, js errors. <em>David Faure &lt;&#102;&#x61;u&#x72;e&#x40;&#x6b;de.org&gt;, George Staikos &lt;staikos@kde.org&gt;</em></li>
    	<li>Bookmarklets support in the form of a &quot;Mini-tools&quot; konq-plugin <em>Alexander Kellett &lt;l&#x79;p&#0097;&#x6e;&#x6f;&#x76;&#64;&#107;d&#x65;&#046;&#x6f;r&#x67;&gt;</em></li>
    	<li>Addition of an advanced Add Bookmark dialog. <em>Alexander Kellett &lt;l&#121;p&#97;n&#111;v&#x40;&#x6b;&#x64;e.&#x6f;&#0114;g&gt;</em></li>
    	<li>Read-only bookmark menu includes. <em>Alexander Kellett &lt;&#x6c;&#0121;pa&#x6e;&#0111;&#118;&#x40;kd&#x65;&#x2e;or&#103;&gt;</em></li>
    	<li>Ability to select which items should be shown in the bookmark bar. <em>Alexander Kellett &lt;ly&#112;a&#110;ov&#x40;k&#100;e.&#x6f;&#00114;g&gt;</em></li>
    	<li>Servicemenu improvements: placed in a common Actions submenu, allow grouping into submenus, allow for separators, allow simple mimetype globbing <em>Aaron J. Seigo &lt;a&#0115;e&#105;go&#x40;kde&#46;&#x6f;&#0114;&#x67;&gt;</em></li>
    	<li>Bookmarking of all loaded tabs as a folder. <em>Alexander Kellett &lt;&#0108;&#x79;&#112;an&#x6f;&#x76;&#0064;&#107;&#x64;e.o&#00114;g&gt;</em></li>
    	<li>Lightweight bookmark editing from within bookmarks menu via context menu. <em>Alexander Kellett &lt;&#0108;&#121;p&#097;&#x6e;&#00111;v&#064;&#x6b;&#x64;&#101;.org&gt;</em></li>
    	<li>New sidebar module: Web.  Implements sidebar modules similar to those introduced in Netscape 6/Mozilla. <em>George Staikos &lt;sta&#105;&#107;os&#00064;&#0107;&#100;e.o&#x72;g&gt;</em></li>
    	<li>Konqueror &quot;preloading&quot;, often reducing Konqueror startup time. <em>Lubos Lunak &lt;l.&#00108;una&#x6b;&#x40;&#x6b;&#100;e&#046;o&#114;g&gt;</em></li>
    	<li>Spellchecking in text forms and spelling highlighting <em>Scott Wheeler &lt;&#00119;&#00104;e&#101;&#108;&#101;r&#00064;k&#x64;&#0101;&#x2e;&#111;rg&gt;, Don Sanders &lt;sanders@kde.org&gt;</em></li>
    <li><h2>Tabbed Browsing</h2>
    <ul>
    	<li>&quot;New Tab&quot; and &quot;Close Tab&quot; buttons besides the tab bar <em>Stephan Binner &lt;&#98;in&#x6e;&#x65;&#x72;&#64;&#107;de&#x2e;&#x6f;&#x72;&#0103;&gt;</em></li>
    	<li>Loading/read state indication by tab color <em>Stephan Binner &lt;bin&#x6e;&#101;r&#64;k&#100;&#0101;&#46;&#x6f;&#x72;&#103;&gt;</em></li>
    	<li>Drag and drop support from/to tabs and to empty tab bar space <em>Stephan Binner &lt;bi&#x6e;&#110;e&#114;&#0064;&#107;&#x64;&#101;&#x2e;org&gt;</em></li>
    	<li>Allow to reorder tabs with pressed middle mouse button <em>Stephan Binner &lt;b&#x69;&#110;n&#0101;r&#64;kd&#x65;&#x2e;&#00111;r&#103;&gt;</em></li>
    	<li>Middle mouse button on tabs or empty tab bar space opens clipboard content <em>Stephan Binner &lt;binn&#00101;r&#x40;&#107;&#x64;&#101;&#46;&#x6f;rg&gt;</em></li>
    	<li>Support for opening external URLs and popups in new tabs <em>Stephan Binner &lt;b&#0105;n&#00110;&#101;&#x72;&#64;kd&#00101;.&#x6f;r&#00103;&gt;</em></li>
    	<li>Optional permanent close buttons within tabs and permanent shown tab bar <em>Stephan Binner &lt;&#x62;i&#x6e;&#110;&#101;&#00114;&#064;kde.&#111;rg&gt;</em></li>
    </ul>

    </li>
    <li><h2>KHTML part</h2>
    <ul>
    	<li>KHTML: Implement caret mode. This is a prerequisite for designmode.
          See <a href="http://bugs.kde.org/show_bug.cgi?id=48302" title="khtml support for designMode and contenteditable=true">Bug 48302</a>.
           <em>Leo Savernik &lt;l.savernik at aon dot at&gt;</em></li>
    	<li>KHTML: add infrastructure for per-domain settings. See
          <a href="http://bugs.kde.org/show_bug.cgi?id=49145" title="RFE: Access Control over JavaScript Properties Globally and Per-Domain">Bug 49145</a>.
           <em>Leo Savernik &lt;l.savernik at aon.at&gt;</em></li>
    	<li>better support for samba shares through libsmbclient of samba 3.0 <em>Stephan Kulow &lt;co&#x6f;lo&#0064;&#00107;&#100;e&#46;org&gt;</em></li>
    </ul>

    </li>
    </ul>

    </li>
    <li><h2>KIO Slaves</h2>
    <ul>
    	<li>kio_smtp: SMTP pipelining support for faster submission, esp. over high-latency links. <em>Marc Mutz &lt;&#00109;ut&#x7a;&#x40;kd&#0101;&#x2e;or&#x67;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KDesktop</h2>
    <ul>
    	<li>Added GUI for &quot;Mouse wheel over desktop switches desktop&quot; option. </li>
    	<li>Make screen savers capable of being DPMS aware, so they can be disabled when watching a movie or TV if the app sets up X DPMS appropriately. <em>Gregor Jasny &lt;Gregor.Jasny@epost.de&gt;</em></li>
    	<li>Screen locker: pluggable greeter input methods; better architectural compliance with PAM's input model. <em>Oswald Buddenhagen &lt;&#111;s&#115;&#105;&#64;&#0107;&#100;&#x65;&#46;&#x6f;&#114;g&gt;</em></li>
    	<li>Shadow behind the text of the desktop icons, for more readability <em>Laur Ivan</em></li>
    	<li>E-mail addresses entered in &quot;Run Command...&quot; (Alt-F2) will start mailer. <em>Waldo Bastian &lt;b&#0097;st&#x69;an&#x40;k&#100;&#x65;&#x2e;&#111;rg&gt;</em></li>
    	<li>Numeric expression in &quot;Run Command...&quot; (Alt-F2) is evaluated. <em>Waldo Bastian &lt;b&#x61;stian&#64;&#107;d&#x65;&#46;&#111;&#x72;&#103;&gt;</em></li>
    </ul>

    </li>
    </ul>

<h2>Addons</h2>
	<ul>
	<li><h2>Vim KPart</h2>
	<ul>
		<li><strong>NEW IN KDE:</strong> A Vim KPart for embedding in KMail, Develop and other programs <em>Mickael Marchand &lt;&#x6d;&#x61;&#x72;c&#104;and&#64;kd&#00101;.o&#0114;g&gt;</em></li>
	</ul>

    </li>
    <li><h2>Konqueror KPart FSView</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> View mode for
          directories, showing files/directories as rectangles with
          area proportional to sizes. <em>Josef Weidendorfer &lt;Josef.Weidendorfer@gmx.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>KBinaryClock</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> KBinaryClock is a panel applet for KDE that displays the current time in binary. <em>Benjamin Meyer</em></li>
    </ul>

    </li>
    </ul>

<h2>Artwork</h2>
	<ul>
	<li><h2>Screen Savers</h2>
	<ul>
		<li>GUI for making the screensaver start/stay not started when the mouse pointer is in a certain corner of the screen. <em>Chris Howells &lt;h&#x6f;&#x77;&#0101;lls&#x40;&#0107;&#0100;&#0101;.or&#0103;&gt;</em></li>
		<li>Use the kiosk framework to allow sysadmins to prevent certain kinds of screensavers (e.g. those that manipulate the screen) from being used. <em>Chris Howells &lt;ho&#119;&#x65;ll&#x73;&#064;k&#x64;e&#0046;org&gt;</em></li>
		<li>Make it possible to disable certain kinds of screensaver in the random screenaver (e.g. OpenGL) <em>Chris Howells &lt;howel&#x6c;&#x73;&#00064;kde.&#x6f;r&#x67;&gt;</em></li>
		<li>Remove kdeartwork/kscreensaver/xsavers -- they are forks of xscreensaver screensavers and xscreensaver screen savers are already well supported in KDE <em>Chris Howells &lt;h&#x6f;&#00119;ell&#115;&#0064;k&#100;&#0101;&#46;o&#x72;&#x67;&gt;</em></li>
		<li><strong>NEW IN KDE:</strong> kclock.kss -- a native KDE screen saver that shows a moving or centered analog clock. <em>Melchior Franz &lt;m&#x66;&#x72;&#x61;&#x6e;&#x7a;&#64;kde&#46;&#x6f;&#x72;g&gt;</em></li>
	</ul>

    </li>
    <li><h2>Styles</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> Plastik widget style <em>Sandro Giessl &lt;ceebx@users.sourceforge.net&gt;</em></li>
    </ul>

    </li>
    </ul>

<h2>Education</h2>
	<ul>
	<li><h2>KBruch</h2>
	<ul>
		<li><strong>NEW IN KDE:</strong> A small program to generate tasks with fractions (<a href="http://edu.kde.org/kbruch/">homepage</a>) <em>Sebastian Stein &lt;kbruch@hpfsc.de&gt;</em></li>
	</ul>

    </li>
    <li><h2>Kig</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> An interactive geometry program (<a href="http://edu.kde.org/kig">homepage</a>) <em>Dominique Devriese &lt;d&#x65;&#118;ri&#101;s&#101;&#64;&#107;&#100;e.&#0111;&#x72;g&gt;</em></li>
    	<li>Scripting Support <em>Dominique Devriese &lt;d&#x65;v&#x72;i&#x65;&#115;&#x65;&#x40;&#x6b;de.&#0111;&#114;&#x67;&gt;</em></li>
    	<li>Horizontal Scrolling Support <em>Dominique Devriese &lt;&#x64;&#101;&#x76;ri&#00101;s&#101;&#0064;kd&#101;.&#x6f;r&#103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KHangMan</h2>
    <ul>
    	<li>Add a toolbar with special characters per language <em>Anne-Marie Mahfouf &lt;a&#110;&#110;&#x6d;&#0097;&#64;kd&#101;&#046;&#x6f;r&#x67;&gt;</em></li>
    	<li>Implement a tip to help guess the word on right-click in French <em>Anne-Marie Mahfouf &lt;&#00097;nnm&#x61;&#x40;k&#x64;e.&#111;r&#103;&gt;</em></li>
    	<li>Add the German language data <em>Anne-Marie Mahfouf &lt;ann&#00109;&#097;&#64;&#x6b;&#x64;e&#x2e;&#00111;r&#0103;&gt;</em></li>
    	<li>Modified the GUI so it resizes much better, fixed bugs, cleaned code, transparent pictures.  <em>Anne-Marie Mahfouf, Stefan Asserh&auml;ll and Robert Gogolok</em></li>
    	<li>Added several language data and a script for adding a new language easily.  <em>Anne-Marie Mahfouf and Stefan Asserh&auml;ll</em></li>
    </ul>

    </li>
    <li><h2>KLettres</h2>
    <ul>
    	<li>Add a toolbar with special characters per language <em>Anne-Marie Mahfouf &lt;&#x61;&#x6e;nm&#x61;&#x40;kd&#101;.&#0111;&#x72;g&gt;</em></li>
    	<li>Cleaned code, added classes, added Czech as new language.  <em>Anne-Marie Mahfouf</em></li>
    </ul>

    </li>
    <li><h2>Kalzium</h2>
    <ul>
    	<li>New informationdialog with pictures of the elements and a
          lot informations. The old dialog has now less information but all the
          really important. <em>Carsten Niehaus &lt;cnieh&#x61;u&#x73;&#64;&#107;&#x64;&#x65;&#x2e;&#x6f;&#00114;g&gt;</em></li>
    	<li>
          Show the atomic orbits (Bohr). Each hull is represented by one orbit
          (a circle) and every electron is represented by a smaller circle
          on the corresponding orbit.
         <em>Carsten Niehaus &lt;&#099;nieha&#117;&#115;&#64;kd&#x65;.&#0111;r&#103;&gt;</em></li>
    	<li>The data can now be searched and views in a table. Furthermore you can now export the
          data you selected into a csv-file. <em>Carsten Niehaus &lt;&#x63;n&#00105;e&#104;&#x61;&#117;&#x73;&#x40;k&#0100;e.o&#x72;g&gt;</em></li>
    	<li>Adding several data-types (eg information about the orbits) <em>Carsten Niehaus &lt;&#099;&#x6e;&#105;eh&#x61;&#117;&#x73;&#64;&#107;d&#x65;&#x2e;o&#00114;g&gt;</em></li>
    </ul>

    </li>
    <li><h2>KStars</h2>
    <ul>
    	<li>Telescope Control using INDI for hardware interface <em>Jasem Mutlaq</em></li>
    	<li>Command-line image generation mode (does not launch GUI) <em>Jason Harris</em></li>
    	<li>Comets and Asteroids <em>Jason Harris</em></li>
    	<li>Jupiter's moons and a tool to plot their positions vs. time. <em>Jason Harris</em></li>
    	<li>Tracks showing paths of solar system bodies <em>Jason Harris</em></li>
    	<li>AAVSO light-curve plotter for variable stars <em>Jasem Mutlaq</em></li>
    	<li>Tool to plot object altitude vs. time <em>Pablo de Vicente</em></li>
    	<li>Batch mode in some astronomial calculator modules <em>Pablo de Vicente</em></li>
    	<li>&quot;What's Up Tonight?&quot; tool <em>Thomas Kabelmann</em></li>
    	<li>ScriptBuilder for point-and-click creation of
                 kstars DCOP scripts <em>Jason Harris</em></li>
    	<li>Improved startup and rendering time <em>KStars team</em></li>
    	<li>Replace SAO star catalog with Hipparcos/Tycho catalog <em>Jason Harris</em></li>
    	<li>&quot;Export Image&quot; menu action <em>Jason Harris</em></li>
    	<li>&quot;Execute scipt&quot; menu action <em>Jason Harris</em></li>
    	<li>Run script from within the ScriptBuilder tool <em>Jason Harris</em></li>
    	<li>click-and-drag zoom box <em>Jason Harris</em></li>
    	<li>customizable eyepiece field-of view indicator <em>Jason Harris</em></li>
    	<li>Zoom-dependent faint limit should affect non-stellar objects <em>Jason Harris</em></li>
    	<li>Evening only/Morning only option in WUT tool <em>Jason Harris</em></li>
    	<li>Add Epoch box to manual focus tool <em>Pablo de Vicente</em></li>
    	<li>Expand list of named stars. <em>Jason Harris</em></li>
    	<li>Upgrade telescope control to INDI 1.11 <em>Jasem Mutlaq</em></li>
    	<li>Handbook chapter on telescope control <em>Jasem Mutlaq</em></li>
    	<li>Non-sidereal telescope tracking <em>Jasem Mutlaq</em></li>
    	<li>Telescope Setup wizard <em>Jasem Mutlaq</em></li>
    	<li>Display Greek letter designation of stars <em>Jason Harris</em></li>
    </ul>

    </li>
    </ul>

<h2>Games</h2>
	<ul>
	<li><h2>Atlantik</h2>
	<ul>
		<li>Support custom tokens <em>Rob Kaper &lt;&#x6b;a&#00112;&#0101;&#x72;&#00064;&#00107;&#x64;&#x65;.&#x6f;&#x72;&#00103;&gt;</em></li>
		<li>Token/avatar images</li>
		<li>Privacy: only connect to meta server on Internet upon specific user request</li>
		<li>KNotify event support</li>
	</ul></li>

    <li><h2>KGoldrunner</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> Write and test game engine, game editor, game levels and KDE application interface for new game KGoldrunner <em>Ian Wadham &lt;ianw@netspace.net.au&gt;</em></li>
    	<li>Write HTML documentation (KGoldrunner Handbook) <em>Ian Wadham &lt;ianw@netspace.net.au&gt;</em></li>
    	<li>Move source code and games data from kdenonbeta to kdegames CVS <em>Ian Wadham &lt;ianw@netspace.net.au&gt;</em></li>
    	<li>Improve the graphics of the game and update the screenshots in the documentation <em>Ian Wadham &lt;ianw@netspace.net.au&gt;</em></li>
    	<li>Convert the documentation to docbook form and commit to kdegames <em>Ian Wadham &lt;ianw@netspace.net.au&gt;</em></li>
    </ul>

    </li>
    <li><h2>KReversi</h2>
    <ul>
    	<li>Modified the AI so it scales much better from easy to hard and can make mistakes on easier levels (much more fun now to play). <em>Benjamin Meyer</em></li>
    	<li>Converted to use the standard KDE games highscore widget.  High schores now corilates to a much better ranking system based upon the AI level and then the total number of pieces at the end of the game. <em>Benjamin Meyer</em></li>
    	<li>Fixed segfaults and converted the application to use xmlui (standard KDE key accels, help menu etc). <em>Benjamin Meyer</em></li>
    </ul>

    </li>
    <li><h2>KMahjongg</h2>
    <ul>
    	<li>Massive code cleanup and bug fixes. <em>Benjamin Meyer</em></li>
    </ul>

    </li>
    <li><h2>KPatience</h2>
    <ul>
    	<li>Implemented the popular spider game. <em>Josh Metzler</em></li>
    	<li>Freecell drops cards more intellegent. <em>Josh Metzler</em></li>
    </ul>

    </li>
    </ul>

<h2>Graphics</h2>
	<ul>
	<li><h2>KPDF</h2>
	<ul>
		<li><strong>NEW IN KDE:</strong> PDF viewer based on XPDF <em>Christophe Devriese &lt;oelewapperke@ulyssis.org&gt;</em></li>
	</ul>

    </li>
    <li><h2>KSVG</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> Scalable Vector Graphics plugins for Konqueror <em>Nikolas Zimmermann &lt;&#119;il&#100;fox&#64;kde&#46;&#x6f;r&#x67;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KFilePlugins</h2>
    <ul>
    	<li>pcx: Displays width, height, bpp, dpi for PCX images <em>Nadeem Hasan &lt;&#110;ha&#115;&#x61;n&#x40;&#x6b;d&#x65;&#46;o&#114;&#103;&gt;</em></li>
    	<li>dvi: show basic information <em>Stefan Kebekus &lt;&#x6b;&#x65;&#x62;&#101;&#107;&#x75;&#115;&#x40;kde&#00046;&#x6f;&#114;&#103;&gt;</em></li>
    	<li>pnm: Display format, dimension, bpp and comments of PBM, PGM and PPM images. <em>Volker Krause &lt;volker.krause@rwth-aachen.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>KDVI</h2>
    <ul>
    	<li>Function to embed external PostScript files into a DVI file <em>Stefan Kebekus &lt;&#x6b;&#101;&#098;&#101;kus&#x40;k&#x64;&#101;&#46;&#x6f;&#0114;&#103;&gt;</em></li>
    	<li>support for colored fonts <em>Stefan Kebekus &lt;&#x6b;&#101;be&#107;u&#0115;&#x40;kde&#00046;org&gt;</em></li>
    	<li>KDVI can now use Type1 and TrueType fonts in addition to PK fonts. This
        reduces waiting times for font generation to a minimum, and give easier
        access to fonts used in Asian languages <em>Stefan Kebekus &lt;kebekus&#x40;k&#x64;&#0101;.or&#x67;&gt;</em></li>
    	<li>massive performance improvement <em>Stefan Kebekus &lt;keb&#x65;kus&#x40;&#x6b;d&#101;&#46;&#x6f;&#x72;&#x67;&gt;</em></li>
    	<li>support for papersize specials <em>Stefan Kebekus &lt;&#107;&#x65;be&#x6b;&#117;s&#00064;&#x6b;d&#x65;&#x2e;&#x6f;&#x72;g&gt;</em></li>
    	<li>usability enhancements (better statusbar display, reload button, 'read-up'
        feature, etc.) <em>Stefan Kebekus &lt;&#x6b;&#101;&#x62;e&#107;&#x75;&#x73;&#00064;&#x6b;&#x64;&#x65;.o&#x72;&#x67;&gt;</em></li>
    </ul>

    </li>
    <li><h2>kviewshell</h2>
    <ul>
    	<li>FullScreen mode <em>Stefan Kebekus &lt;ke&#98;ek&#117;&#x73;&#0064;kd&#0101;&#0046;o&#x72;g&gt;</em></li>
    	<li>more intelligent handling of paper sizes/orientations <em>Stefan Kebekus &lt;&#x6b;e&#x62;eku&#115;&#064;kd&#0101;&#46;&#00111;&#x72;g&gt;</em></li>
    	<li>usability enhancements <em>Stefan Kebekus &lt;k&#x65;bek&#117;s&#x40;kde.&#111;r&#103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KView</h2>
    <ul>
    	<li>autoscroll the image when creating a selection <em>Matthias Kretz &lt;k&#114;etz&#0064;k&#x64;&#00101;&#46;org&gt;</em></li>
    	<li>drag and drop support <em>Matthias Kretz &lt;k&#x72;&#x65;&#116;&#x7a;&#x40;&#107;&#x64;e.&#x6f;r&#103;&gt;</em></li>
    	<li>some basic image effects as a new plugin <em>Matthias Kretz &lt;kre&#116;z&#0064;&#107;&#x64;&#101;&#046;&#111;&#x72;&#103;&gt;</em></li>
    	<li>open images from stdin <em>Matthias Kretz &lt;k&#00114;&#101;&#x74;&#00122;&#00064;kd&#x65;.o&#00114;&#00103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KSnapshot</h2>
    <ul>
    	<li>Window snapshots optionally don't include window decorations <em>Lubos Lunak &lt;l.lu&#00110;a&#107;&#64;kd&#101;.&#111;r&#103;&gt;</em></li>
    	<li>Region snapshots. User interface makeover. <em>Nadeem Hasan &lt;&#110;ha&#115;a&#x6e;&#64;k&#100;&#x65;.or&#103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KGamma</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> A KControl module for monitor gamma correction. <em>Michael v.Ostheim &lt;MvOstheim@web.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>KPovModeler</h2>
    <ul>
    	<li>Basic plugin framework <em>Andreas Zehender &lt;ze&#x68;end&#101;&#114;&#x40;&#x6b;&#100;e&#46;&#00111;r&#0103;&gt;</em></li>
    	<li>The user is asked if pending changes should be saved before rendering <em>Andreas Zehender &lt;&#x7a;e&#x68;&#101;nd&#101;r&#0064;kd&#101;.or&#103;&gt;</em></li>
    	<li>Control point selection in the properties view for bicubic patch, sor, lathe and prism <em>Andreas Zehender &lt;z&#x65;&#x68;&#x65;n&#100;&#101;&#0114;&#x40;kde.&#111;r&#0103;&gt;</em></li>
    	<li>Export flag for graphical objects and the camera <em>Andreas Zehender &lt;ze&#104;&#x65;nder&#64;&#00107;d&#101;.or&#0103;&gt;</em></li>
    	<li>Height field view structure <em>Leon Pennington &lt;leon@leonscape.co.uk&gt;</em></li>
    	<li>Light object gained, parallel, circular and orient options <em>Leon Pennington &lt;leon@leonscape.co.uk&gt;</em></li>
    	<li>Dispersion options are supported for interior <em>Leon Pennington &lt;leon@leonscape.co.uk&gt;</em></li>
    	<li>Support for POV-Ray 3.5 noise generators <em>Leon Pennington &lt;leon@leonscape.co.uk&gt;</em></li>
    	<li>New POV-Ray 3.5 warp types <em>Leon Pennington &lt;leon@leonscape.co.uk&gt;</em></li>
    	<li>New POV-Ray 3.5 objects: isosurface, projected through, radiosity, global photons, photons, light groups, interior texture, mesh <em>Leon Pennington &lt;leon@leonscape.co.uk&gt;, Andreas Zehender &lt;zehender@kde.org&gt;</em></li>
    </ul>

    </li>
    <li><h2>KGhostView</h2>
    <ul>
    	<li>Thumbnail generation for all pages <em>Albert Astals Cid &lt;tsdgeos@terra.es&gt;, Luis Pedro Coelho &lt;luis@luispedro.org&gt;</em></li>
    </ul>
    </li>

    </ul>

<h2>Multimedia</h2>
	<ul>
	<li><h2>aRts</h2>
	<ul>
		<li>New mixer-channel: LittleStereoMixer <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>
          Create KAudioPlayStream and put it into kdelibs/arts/kde. Needed at
          least for KRec and its the logical counterpart to KAudoRecordStream...
          <br></br>It is basicly a total rewrite of the (not compiling) version
          Matthias Kretz put into kdenonbeta/arts/kde so I could grab and finish
          it.
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>
          New Widget: LayoutBox being able to change direction dynamically. As
          replacement for [HV]Box.
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>
          New Widget: PopupBox with the ability to hide its contents and showing
          them inside the widget or a new top-level widget (like a tooltip but
          permanent).
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>New Widget: Label that can rotate its text. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>New Widget: Tickmarks specially for deciBel. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>New Widget: VolumeFader specially for Volumes supporting deciBel. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>
          New Widget: VU-Meter specially for deciBel.<br></br>Supports various
          styles: NormalBars (just a configurable number of bars), FireBars (a
          colored bar moving), LineBar (a bar moving, color depends on the
          actual volume), Small (whole widget painted in a color depending on
          the volume)<br></br>
          Configurable things: Direction, Peak, Peakfalloff, min. dB, style.
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>A Gui for the StereoVolumeControl. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>
          First part of the rewriting/reordering of artscontrol. Making the features available via a lib.
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>
          A little kicker-applet named artscontrol ;-) Perhaps with the ability
          to include not only the master-fader but an environment-mixer with a
          small gui. Of course it includes all the normal artscontrol features.
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
		<li>AudioIOEsd: output to an esd <em>Ian Chiew &lt;ian@snork.net&gt;</em></li>
		<li>autosuspend in full duplex <em>Stefan Westerfeld &lt;st&#119;&#0064;k&#100;e.o&#114;g&gt;</em></li>
	</ul>

    </li>
    <li><h2>Noatun</h2>
    <ul>
    	<li>Equalizer in KJofol Skin Loader. <em>Stefan Gehn &lt;metz AT gehn.net&gt;</em></li>
    	<li>Support for loading of EXTM3U Format (saved by Winamp since 2.x). <em>Stefan Gehn &lt;metz AT gehn.net&gt;</em></li>
    	<li>Support for loading of Windows Media Player Playlists. <em>Stefan Gehn &lt;metz AT gehn.net&gt;</em></li>
    	<li>Variable Band Equalizer. <em>Charles Samuels &lt;&#0099;&#x68;&#x61;r&#00108;e&#115;&#x40;k&#100;&#x65;.&#00111;rg&gt;</em></li>
    	<li>New playlist, Oblique. <em>Charles Samuels &lt;ch&#0097;r&#x6c;&#101;&#x73;&#x40;k&#100;e.org&gt;</em></li>
    </ul>

    </li>
    <li><h2>KRec</h2>
    <ul>
    	<li>
          Big rewrite to create a new simplier Gui and make it easier to use.
          Its a big bunch of work and depends on a lot of small things.
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
    	<li>
          Make exporting to various soundformats work. OGG and MP3 need some
          additional work for 3.3 but are working for now.
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
    	<li>Saving and loading of KRec's own fileformat. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
    	<li>Create a view-mode for the soundfiles. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
    	<li>Create an informative time/size/etc display. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
    	<li>
          Add a menu to open the Audiomanager (and other tools) since there is
          an artscontrol library.
         <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
    	<li>Make it use the Arts::LevelMeter. <em>Arnold Krille &lt;kde@arnoldarts.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>aKtion</h2>
    <ul>
    	<li>Drop aKtion. <em>Chris Howells &lt;&#0104;o&#0119;&#101;&#00108;ls&#00064;kde.&#111;r&#103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KsCD</h2>
    <ul>
    	<li>LIRC support <em>Aaron J. Seigo &lt;&#097;&#x73;&#101;&#105;go&#0064;k&#x64;&#0101;&#46;o&#00114;g&gt;</em></li>
    	<li>Using the new KDE cddb library. <em>Richard Laerkaeng &lt;richard@goteborg.utfors.se&gt;, Aaron J. Seigo &lt;aseigo@olympusproject.org&gt;</em></li>
    </ul>

    </li>
    <li><h2>KAudioCreator</h2>
    <ul>
    	<li>Using the new KDE cddb library. <em>Benjamin Meyer</em></li>
    	<li>Automaticly detects CD's. <em>Benjamin Meyer</em></li>
    	<li>Many new small features including: File text regular expression replacment. <em>Benjamin Meyer</em></li>
    	<li>Disk drive now user selectable. <em>Benjamin Meyer</em></li>
    </ul>

    </li>
    <li><h2>libkcddb</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> Common library for accessing cddb information <em>Richard Laerkaeng &lt;richard@goteborg.utfors.se&gt;</em></li>
    	<li>Add support for sending cddb information <em>Richard Laerkaeng &lt;richard@goteborg.utfors.se&gt;</em></li>
    </ul>

    </li>
    <li><h2>KFileMetaInfo</h2>
    <ul>
    	<li>ID3v2 support (without id3lib) <em>Scott Wheeler &lt;w&#00104;eel&#101;&#114;&#x40;kde.&#111;r&#x67;&gt;</em></li>
    </ul>

    </li>
    <li><h2>JuK</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> A jukebox and music manager <em>Scott Wheeler &lt;&#00119;h&#x65;ele&#x72;&#064;kd&#0101;&#x2e;&#x6f;r&#103;&gt;</em></li>
    	<li>Advanced search mode <em>Scott Wheeler &lt;&#x77;&#104;&#101;el&#x65;r&#x40;k&#x64;&#101;.o&#114;&#103;&gt;</em></li>
    	<li>Tree view mode <em>Scott Wheeler &lt;wh&#00101;el&#101;r&#64;&#107;d&#101;&#x2e;o&#x72;&#103;&gt;</em></li>
    	<li>Storable searches <em>Scott Wheeler &lt;w&#104;ee&#x6c;&#x65;r&#x40;&#00107;d&#101;&#46;o&#114;&#x67;&gt;</em></li>
    	<li>A &quot;weighted width&quot; mode for the playlists -- possibly to be merged into KListView <em>Scott Wheeler &lt;w&#x68;&#x65;&#101;&#00108;&#x65;r&#00064;kde&#46;or&#x67;&gt;</em></li>
    </ul>

    </li>
    </ul>

<h2>Network</h2>
	<ul>
	<li><h2>Kopete</h2>
	<ul>
		<li><strong>NEW IN KDE:</strong> A multi-protocol instant messaging tool (<a href="http://kopete.kde.org">homepage</a>) <em></em></li>
		<li>Remove the config page for the yahoo module. All configuration will be
        moved to the Yahoo account config. <em>Matt Rogers &lt;matt@matt.rogers.name&gt;</em></li>
		<li>Port the plugin loading GUI over to kdelibs::KPluginSelector
        and get rid of the custom PluginLoader class <em>Martijn Klingens &lt;kl&#105;n&#x67;e&#00110;s&#0064;&#x6b;&#100;e.&#x6f;rg&gt;</em></li>
		<li>Support KWallet for password storage <em>Martijn Klingens &lt;k&#108;ingen&#0115;&#00064;&#107;d&#00101;&#00046;org&gt;</em></li>
		<li>Port the configuration to KSettings::Dialog.  <em>Matt Rogers &lt;matt@matt.rogers.name&gt;</em></li>
		<li>Integrate Kopete's contact list into KAddressBook <em>The Kopete team &lt;&#00107;&#111;&#00112;&#x65;t&#x65;-&#100;ev&#x65;&#108;&#064;&#00107;d&#0101;&#046;o&#x72;g&gt;</em></li>
		<li>Cleanup the API to be more robust. Notably, unload plugins asynchronously, get
        rid of ConfigModule and use KCMs instead and try to cut down on the amount of nested
        event loops. <em>Martijn Klingens &lt;kl&#105;&#00110;&#x67;&#0101;n&#115;&#x40;k&#x64;e.or&#103;&gt;</em></li>
		<li>Switch the Oscar Plugin to KExtendedSocket <em>Matt Rogers &lt;matt@matt.rogers.name&gt;</em></li>
		<li>Port the Jabber plugin to use libxmpp <em>Till Gerken</em></li>
		<li>Port MSN to the new protocol (MSNP9) <em>Olivier Goffart &lt;ogoffart(@)tiscalinet.be&gt;</em></li>
		<li>MSN display picture and incomming MSN custom emoticons support <em>Olivier Goffart &lt;ogoffart(@)tiscalinet.be&gt;</em></li>
	</ul>

    </li>
    <li><h2>Desktop Sharing (krfb)</h2>
    <ul>
    	<li>HTTP Server for Java applet <em></em></li>
    </ul>

    </li>
    <li><h2>ksim</h2>
    <ul>
    	<li>Snmp plugin <em>Simon Hausmann &lt;&#00104;&#097;&#x75;sm&#97;nn&#x40;&#107;&#0100;e.o&#114;g&gt;</em></li>
    </ul>

    </li>
    <li><h2>Remote Desktop Connection (krdc)</h2>
    <ul>
    	<li>Additional protocol: RDP <em></em></li>
    	<li>Preferences system <em></em></li>
    	<li>Special keys dialog for CTRL-ALT-DEL and similar combinations <em></em></li>
    	<li>View Only option <em></em></li>
    	<li>Allow scaling in fullscreen mode <em></em></li>
    	<li>Support for CursorPos extension, better 'dot-cursor' handling <em></em></li>
    	<li>Toolbar redesign <em></em></li>
    </ul>

    </li>
    <li><h2>KWiFiManager</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> application for monitoring and configuring wireless LAN connections <em>Stefan Winter &lt;kde@stefan-winter.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>KPPP</h2>
    <ul>
    	<li><strong>Log viewer:</strong>Show summary for selections and show a monthly estimates in the statistics area. <em>Andras Mantia, Mikolaj Machowski</em></li>
                <li>Fixed a bug in modem command writing</li>
    </ul>

    </li>
    </ul>

<h2><a name="pim">PIM</a></h2>
	<ul>
	<li><h2>Kontact</h2>
	<ul>
		<li><strong>NEW IN KDE:</strong> Introduction of Kontact, KDE's PIM and groupware suite. <em>Daniel Molkentin &lt;&#x6d;ol&#x6b;e&#x6e;t&#x69;&#x6e;&#00064;&#x6b;de&#046;&#x6f;r&#103;&gt;</em></li>
		<li>Drag and Drop of mails on calendar and todo list. <em>Cornelius Schumacher &lt;sc&#x68;&#00117;mac&#x68;e&#x72;&#x40;&#x6b;d&#x65;&#x2e;o&#x72;g&gt;, Don Sanders &lt;sanders@kde.org&gt;</em></li>
		<li>Adding KNode part. <em>Zack Rusin &lt;&#x7a;a&#0099;&#x6b;&#64;&#x6b;d&#101;&#00046;&#x6f;&#0114;&#x67;&gt;</em></li>
	</ul>

    </li>
    <li><h2>KMail</h2>
    <ul>
    	<li>Load attachments on demand (imap). <em>Carsten Burghardt &lt;b&#x75;rgh&#x61;rd&#x74;&#064;kde.&#x6f;&#x72;g&gt;</em></li>
    	<li>Add &quot;Play Sound&quot; option as filter action / when receiving new mail. <em></em></li>
    	<li>Add identity-dependant spell-checking dictionary. <em>Ingo Kloecker &lt;&#x6b;l&#0111;&#101;cker&#x40;k&#00100;&#00101;.&#x6f;rg&gt;</em></li>
    	<li>Add Imap-subscription. <em>Carsten Burghardt &lt;&#98;u&#x72;g&#x68;ard&#116;&#x40;&#00107;de&#0046;&#111;r&#x67;&gt;</em></li>
    	<li>Select startup-folder. <em>Carsten Burghardt &lt;&#x62;&#0117;rg&#x68;ar&#100;t&#x40;&#107;d&#00101;.org&gt;</em></li>
    	<li>Easy language selection for as-you-type spell checking. <em>Ingo Kloecker &lt;k&#x6c;&#111;&#00101;c&#00107;&#x65;r&#x40;kde.org&gt;</em></li>
    	<li>Allow message preview window to be hidden or displayed
            to the right of the messages list. <em>Till Adam &lt;till@adam-lilienthal.de&gt;</em></li>
    	<li>Improved Threading, particularly subject threading. <em>Till Adam &lt;till@adam-lilienthal.de&gt;</em></li>
    	<li>Sort and thread stable adding and removing of messages in the current folder. <em>Till Adam &lt;till@adam-lilienthal.de&gt;, Don Sanders &lt;sanders@kde.org&gt;</em></li>
    	<li>Allow mails to have multiple statuses. <em>Till Adam &lt;till@adam-lilienthal.de&gt;</em></li>
    	<li>Watch thread and ignore thread. <em>Till Adam &lt;till@adam-lilienthal.de&gt;</em></li>
    	<li>Searching and filtering mail by status. <em>Till Adam &lt;till@adam-lilienthal.de&gt;</em></li>
    	<li>Mailcheck in individual imap folders. <em>Till Adam &lt;till@adam-lilienthal.de&gt;</em></li>
    	<li>As-you-type spell checking. <em>Don Sanders &lt;&#x73;&#00097;&#00110;&#100;&#101;rs&#0064;k&#100;e.&#x6f;&#00114;g&gt;</em></li>
    	<li>Search folders (a.k.a virtual folders). <em>Don Sanders &lt;s&#097;&#x6e;&#100;er&#00115;&#x40;&#x6b;&#x64;e&#46;&#00111;r&#x67;&gt;</em></li>
    	<li>Kontact integration. <em>Don Sanders &lt;s&#x61;nd&#x65;&#0114;s&#x40;k&#100;&#0101;.&#00111;r&#00103;&gt;</em></li>
    	<li>Per folder duplicate message removal. <em>Don Sanders &lt;&#x73;&#97;n&#0100;&#101;rs&#00064;kde&#46;&#x6f;rg&gt;</em></li>
    	<li>Search dialog usability improvements including
        context menu. <em>Don Sanders &lt;&#115;&#097;n&#0100;er&#x73;&#064;&#x6b;de.o&#x72;&#103;&gt;</em></li>
    	<li>Separate reader window usability improvements including
        context menu. <em>Don Sanders &lt;&#x73;a&#110;&#x64;e&#x72;&#x73;&#x40;k&#x64;&#101;.&#x6f;&#114;&#x67;&gt;</em></li>
    	<li>Auto saving of mails being composed, prevents mail
        loss in the result of a hard crash. <em>Don Sanders &lt;&#x73;&#97;n&#100;&#00101;r&#115;&#x40;&#00107;de&#x2e;&#x6f;rg&gt;</em></li>
    	<li>Ad hoc filters. <em>Don Sanders &lt;&#x73;a&#x6e;de&#x72;&#00115;&#064;&#x6b;de&#x2e;o&#114;&#103;&gt;</em></li>
    	<li>Drag and Drop of mails on KOrganizer to turn mails into events or
        todos. This requires a message drag object and a DCOP call to select a
        mail in the reader window by an identifier. <em>Cornelius Schumacher &lt;schu&#00109;a&#x63;&#104;er&#x40;&#x6b;d&#101;.&#x6f;rg&gt;</em></li>
    	<li>Disconnected IMAP. <em>Bo Thorsen &lt;bo@sonofthor.dk&gt;</em></li>
    <li><h2>Merging kroupware_branch</h2>
    <ul>
    	<li>Disconnected IMAP <em>Bo Thorsen &lt;bo@sonofthor.dk&gt;</em></li>
    	<li>compliant MDN support <em>Marc Mutz &lt;&#00109;&#117;t&#0122;&#x40;&#0107;de.o&#114;g&gt;</em></li>
    </ul>

    </li>
    </ul>

    </li>
    <li><h2>KNode</h2>
    <ul>
    	<li>Add DCOP interface <em>Zack Rusin &lt;&#122;&#00097;&#099;k&#x40;k&#x64;e.org&gt;</em></li>
    	<li>Make KNode a fully functional KPart <em>Zack Rusin &lt;z&#97;c&#107;&#64;kde&#46;&#x6f;rg&gt;</em></li>
    	<li>Integration with KWallet <em>Zack Rusin &lt;&#122;a&#0099;k&#x40;k&#100;&#101;&#46;&#111;rg&gt;</em></li>
    </ul>

    </li>
    <li><h2>KMailCVT</h2>
    <ul>
    	<li>New GUI based on KWizard <em>Laurence Anderson &lt;l.d.anderson@warwick.ac.uk&gt;</em></li>
    	<li>Code cleanup <em></em></li>
    </ul>

    </li>
    <li><h2>libkabc</h2>
    <ul>
    	<li>Port libkabc to the new kresource model <em></em></li>
    	<li>Add asynchronous interface <em>Tobias Koenig &lt;to&#107;&#00111;&#x65;&#x40;kde&#x2e;&#x6f;&#x72;g&gt;</em></li>
    </ul>

    </li>
    <li><h2>KAddressbook</h2>
    <ul>
    	<li>Input fields for GEO, CLASS, PHOTO, LOGO and SOUND <em>Tobias Koenig &lt;t&#111;&#x6b;oe&#x40;kd&#x65;.org&gt;</em></li>
    	<li>Make the views plugins <em></em></li>
    	<li>Kontact integration. <em>Cornelius Schumacher &lt;schu&#x6d;a&#x63;&#0104;e&#x72;&#64;kde.&#x6f;&#x72;g&gt;, Don Sanders &lt;sanders@kde.org&gt;, Tobias Koenig &lt;tokoe@kde.org&gt;</em></li>
    </ul>

    </li>
    <li><h2>libkcal</h2>
    <ul>
    	<li>Unit tests. <em>Cornelius Schumacher &lt;s&#099;&#x68;umach&#101;r&#00064;&#x6b;&#00100;&#x65;.o&#114;g&gt;</em></li>
    </ul>

    </li>
    <li><h2>KOrganizer</h2>
    <ul>
    	<li>File backend which automatically stores all changes and supports external
        changes of the data <em>Cornelius Schumacher</em></li>
    	<li>Allow moving of events that span several days <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Full support for arbitrary week start days (as configured in the control center) <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Configurable printing <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Dialog to import calendars as resources, e.g. for subscribing
        to webcal URLs. <em>Cornelius Schumacher &lt;s&#099;&#0104;uma&#99;h&#x65;r&#64;&#x6b;de&#46;or&#00103;&gt;</em></li>
    	<li>Quick event generation in agenda view (type-ahead). <em>Cornelius Schumacher &lt;sc&#0104;&#x75;ma&#99;&#104;&#x65;r&#64;&#x6b;&#0100;&#x65;&#x2e;&#x6f;&#0114;g&gt;</em></li>
    	<li>Remote calendars (caching)
         <em>Cornelius Schumacher &lt;&#x73;&#x63;&#104;&#117;&#109;ac&#104;&#00101;&#114;&#64;&#x6b;&#x64;e.or&#103;&gt;</em></li>
    	<li>Support drag and drop of todos to event views. <em>Cornelius Schumacher &lt;s&#x63;humach&#0101;&#x72;&#x40;k&#100;&#101;.o&#114;&#x67;&gt;, Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Locking of calendar files. <em>Cornelius Schumacher &lt;&#115;&#00099;&#104;um&#x61;&#x63;h&#x65;&#x72;&#x40;&#x6b;d&#x65;.&#0111;&#114;g&gt;</em></li>
    	<li>Incidence attachements. <em>Cornelius Schumacher &lt;s&#x63;&#104;&#x75;ma&#99;&#0104;er&#00064;&#107;de.&#x6f;&#114;g&gt;</em></li>
    	<li>Kontact integration. <em>Cornelius Schumacher &lt;sch&#x75;&#109;&#097;&#x63;her&#64;&#107;&#x64;e.org&gt;, Don Sanders &lt;sanders@kde.org&gt;</em></li>
    </ul>

    </li>
    <li><h2>KAlarm</h2>
    <ul>
    	<li>Add option for alarm to send an email. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add options for non-modal alarm message window. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add options to store and view expired alarms. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add copy, view and undelete actions for the selected alarm. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Restore alarm messages which were displayed before KAlarm was killed or crashed. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Implement shift key accelerator for spinbox buttons. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Store alarm data in calendar file in a more standard way. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add KAddressBook birthday import facility. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add option to set an advance warning for alarms. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add option to set font and foreground color for individual alarm messages. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add option to list next 24 hours' alarms in system tray tooltip. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    	<li>Add time-to-alarm display option to main alarm list. <em>David Jarvie &lt;software@astrojar.org.uk&gt;</em></li>
    </ul>

    </li>
    <li><h2>KPilot</h2>
    <ul>
    	<li>Generalized .pdb viewer component <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>DOC conduit to convert text files to PalmDOC (Aportis DOC, TealReader, Weasel reader etc.) texts on the handheld. <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>New application &quot;kpalmdoc&quot; that converts texts (.txt) to .pdb files and the other way round. Also works for whole directories, bookmarks are supported. (This app is similar to MakeDocJ/MakeDocW) <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Allow addressbook sync with an arbitrary vcard file (not only StdAddressbook) <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Allow synchronization of the handheld's custom fields with addressbook fields like URL, birthdate, ICQ, etc. <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Fixed the backup when doing a hot sync. New fast backup mode. <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>All sync settings are now global settings, but can be overriden in the conduit configuration. <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>Redesigned conduit selection and configuration dialog. <em>Adriaan de Groot</em></li>
    	<li>New conduit: The systeminfo conduit exports information about the handheld to a text or html file. <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>New Viewer/Editor: It is now possible to edit todos directly in KPilot. The changes are synced to the handheld. <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    	<li>New Viewer/Editor: You can edit the raw data of any database from the handheld (using a Hex Editor). The changes are synced to the handheld <em>Reinhold Kainhofer &lt;reinhold@kainhofer.com&gt;</em></li>
    </ul>

    </li>
    <li><h2>KArm</h2>
    <ul>
    	<li>
          Use libkcal to store data.  This way KArm and KOrganizer can
          work off the same TODO list.  Also, KArm now stores the history of
          hours logged in the same file as the tasks.  VTODO == task, and
          VEVENT == a start/stop timer event.
         <em>Mark Bucciarelli &lt;mark@hubcapconsulting.com&gt;</em></li>
    	<li>
          Prompt user to pick file name when importing legacy storage.
          Previously, storage format was determined with magic based on
          strings in the file name.
         <em>Mark Bucciarelli &lt;mark@hubcapconsulting.com&gt;</em></li>
    	<li>
          Create new timecard report.  Ask user for a date range and generate
          a list of weekly tables that show hours spent by task and day.
          Paste this text report into the clipboard.
         <em>Mark Bucciarelli &lt;mark@hubcapconsulting.com&gt;</em></li>
    	<li>Use IconList dialog for preferences screen. <em>Mark Bucciarelli &lt;mark@hubcapconsulting.com&gt;</em></li>
    </ul>

    </li>
    </ul>

<h2>SDK</h2>
	<ul>
	<li><h2>KUIViewer</h2>
	<ul>
		<li><strong>NEW IN KDE:</strong> KUIViewer part that allows the preview of UI files. <em>Ian Reinhart Geiser &lt;ge&#105;&#115;e&#x72;&#105;&#x40;&#x6b;&#100;&#0101;.&#x6f;rg&gt;</em></li>
	</ul>

    </li>
    <li><h2>UI Thumbnail</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> Thumbnail preview for UI files. <em>Ian Reinhart Geiser &lt;g&#x65;&#x69;s&#101;&#114;&#0105;&#64;&#x6b;de&#x2e;&#111;r&#103;&gt;</em></li>
    </ul>

    </li>
    <li><h2>KCachegrind</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> <a href="http://kcachegrind.sf.net">KCachegrind</a>,
          a profile data visualisation. The data is generated
          with the Valgrind derived tool Calltree (see homepage) <em>Josef Weidendorfer &lt;Josef.Weidendorfer@gmx.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>Cervisia</h2>
    <ul>
    	<li>Login support for pserver cvs servers <em>Christian Loose &lt;christian.loose@hamburg.de&gt;</em></li>
    	<li>DCOP service that provides easy access to CVS functions (cvsservice) <em>Christian Loose &lt;christian.loose@hamburg.de&gt;</em></li>
    	<li>Optimize and improve Cervisia's file and directory view (UpdateView) <em>Andre Woebbeking &lt;woebbeking@onlinehome.de&gt;</em></li>
    	<li>Better SSH support. Add ability to start and use ssh-agent. (BR #39155) <em>Christian Loose &lt;christian.loose@hamburg.de&gt;</em></li>
    	<li>More KDE standard compliant dialogs (QDialog -&gt; KDialogBase) <em>Andre Woebbeking &lt;woebbeking@onlinehome.de&gt;</em></li>
    	<li>Remember last opened directory in &quot;Open Sandbox...&quot; dialog <em>Christian Loose &lt;christian.loose@hamburg.de&gt;</em></li>
    	<li>Add new command line option (-log) to display a log dialog for a single file without starting Cervisia <em>Christian Loose &lt;christian.loose@hamburg.de&gt;</em></li>
    	<li>Add &quot;diff to HEAD&quot; functionality to display changes made by other developers before updating the local working copy <em>Andre Woebbeking &lt;woebbeking@onlinehome.de&gt;</em></li>
    	<li>Make list views configurable (column order/widths, sorting) <em>Andre Woebbeking &lt;woebbeking@onlinehome.de&gt;</em></li>
    	<li>Add &quot;Hide Empty Directories&quot; functionality to display only directories which contain any <i>&quot;changes&quot;</i> <em>Andre Woebbeking &lt;woebbeking@onlinehome.de&gt;</em></li>
    	<li>Use user's settings (locale and timezone) to display dates and times <em>Andre Woebbeking &lt;woebbeking@onlinehome.de&gt;</em></li>
    	<li>Add splitters to resolve dialog (BR #41263) <em>Christian Loose &lt;christian.loose@hamburg.de&gt;</em></li>
    	<li>Add new view to log dialog that shows the cvs output in a similar form
        as the command-line client. <em>Christian Loose &lt;christian.loose@hamburg.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>KFilePlugins</h2>
    <ul>
    	<li><b>ts</b>: Displays the number of translated, untranslated and obsolete messages
        of Qt-Linguist files
        <a href="http://www.cip.biologie.Uni-Osnabrueck.DE/niehaus/linux/kfile_ts.tar.bz2">kfile_ts.tar.bz2</a>
         <em>Carsten Niehaus &lt;cni&#x65;ha&#x75;s&#0064;&#x6b;&#100;e&#46;o&#0114;g&gt;</em></li>
    	<li><b>diff</b>Added support for recognizing Subversion diff output. <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>
    </ul>

    </li>
    <li><h2>KBabel</h2>
    <ul>
    	<li>Catalog Manager caching <em>Stefan Asserhall</em></li>
    	<li>Bookmarks <em>Marco Wegner</em></li>
    	<li>TMX dictionary plugin <em>Stanislav Visnovsky &lt;vi&#x73;novs&#107;&#121;&#64;k&#0100;e.o&#x72;g&gt;</em></li>
    	<li>Highlighting code rewrite <em>Marco Wegner</em></li>
    	<li>CVS integration in Catalog Manager <em>Marco Wegner</em></li>
    	<li>GNU gettext plural forms <em>Stanislav Visnovsky &lt;&#x76;&#0105;&#x73;nov&#115;ky&#64;k&#100;&#x65;&#x2e;&#00111;&#114;&#0103;&gt;</em></li>
    	<li>Validation plugin-based tools <em>Stanislav Visnovsky &lt;&#x76;&#x69;&#00115;n&#111;v&#115;&#00107;&#121;&#x40;k&#x64;&#101;&#x2e;&#x6f;rg&gt;</em></li>
    	<li>Create plural form translation from single text <em>Stanislav Visnovsky &lt;&#118;&#00105;&#0115;nov&#x73;&#107;y&#0064;&#x6b;&#100;e.&#x6f;&#0114;g&gt;</em></li>
    	<li>Marking using mouse in Catalog Manager <em>Stanislav Visnovsky &lt;vis&#110;ovsky&#64;k&#x64;&#0101;&#x2e;&#111;&#x72;g&gt;</em></li>
    	<li>RegExp marking in Catalog Manager <em>Marco Wegner &lt;mail@marcowegner.de&gt;</em></li>
    	<li>Plugin-based file import/export <em>Stanislav Visnovsky &lt;&#118;i&#115;novs&#0107;y&#64;&#x6b;&#x64;&#x65;.&#111;&#114;&#103;&gt;</em></li>
    	<li>Autosave feature <em>Marco Wegner &lt;mail@marcowegner.de&gt;</em></li>
    	<li>Whitespace translations validation plugin <em>Dwayne Bailey &lt;dwayne@translate.org.za&gt;</em></li>
    	<li>Translation length validation plugin <em>Dwayne Bailey &lt;dwayne@translate.org.za&gt;</em></li>
    	<li>Translations still in English validation plugin <em>Dwayne Bailey &lt;dwayne@translate.org.za&gt;</em></li>
    	<li>Qt Linguist import/export plugin <em>Marco Wegner &lt;dubbleu@web.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>Kompare</h2>
    <ul>
    	<li>Fixed some problems with the interaction between the navigation part and the komparepart, mostly preventing double emitted signals so it got a bit faster. <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>
    	<li>Added the ability to load a file and diff output and to patch the file. <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>
    	<li>A lot of internal cleanups so maintenance will be easier. <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>
    	<li>Added an interface class/library you can link your application against to expose the extra methods the komparepart implements over a standard KPart. This is not guaranteed to be binary compatible yet. I might need to add support for the encoding in the interface.  <em>Otto Bruggeman &lt;bruggie@home.nl&gt;</em></li>
    </ul>

    </li>
    <li><h2>Umbrello</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> <a href="http://uml.sourceforge.net/">Umbrello UML Modeller</a> <em>Umbrello Developers &lt;uml-devel@lists.sourceforge.net&gt;</em></li>
    	<li>refactoring agent <em>Umbrello Developers &lt;uml-devel@lists.sourceforge.net&gt;</em></li>
    	<li>improved code export <em>Umbrello Developers &lt;uml-devel@lists.sourceforge.net&gt;</em></li>
    	<li>changing properties of multiple items at one time <em>Sebastian Stein &lt;seb_stein@gmx.de&gt;</em></li>
    	<li>refactor internal code (Widget and WidgetData classes) <em>Oliver Kellogg &lt;Oliver.Kellogg@t-online.de&gt;</em></li>
    	<li>Add Datatype widget and internal representation <em>Umbrello Developers &lt;uml-devel@lists.sourceforge.net&gt;</em></li>
    </ul>

    </li>
    </ul>

<h2>Bindings</h2>
	<ul>
	<li><h2>KJSEmbed</h2>
	<ul>
		<li>Make more core facilities available to scripts <em>Richard Moore &lt;r&#x69;c&#x68;&#64;&#107;de&#00046;&#111;rg&gt;</em></li>
		<li>Add a factory for creating non-widget QObjects <em>Richard Moore &lt;&#114;&#00105;ch&#x40;&#107;&#x64;e.&#111;&#114;&#x67;&gt;</em></li>
		<li>Add support for slots that return a value <em>Richard Moore &lt;&#114;ich&#0064;&#107;&#100;&#x65;&#x2e;&#x6f;&#114;g&gt;</em></li>
		<li>Add bindings to allow loading of XML and DOM access <em>Richard Moore &lt;r&#x69;&#00099;h&#64;&#00107;d&#x65;.&#x6f;r&#x67;&gt;</em></li>
		<li>Add DCOP access to create and use dcop interfaces from KJS <em>Ian Reinhart Geiser &lt;geise&#114;&#x69;&#x40;kd&#x65;.o&#114;g&gt;</em></li>
		<li>Add access to KConfig for KJSEmbed <em>Ian Reinhart Geiser &lt;&#0103;e&#0105;s&#x65;&#x72;&#00105;&#x40;kd&#00101;.o&#00114;g&gt;</em></li>
		<li>Add NetAccess bindings <em>Zack Rusin &lt;&#x7a;a&#99;k&#x40;&#107;de&#x2e;&#111;&#114;g&gt;</em></li>
	</ul>

    </li>
    <li><h2>Smoke language bindings backend</h2>
    <ul>
    	<li>Added operator method parsing to kalyptus and smoke <em>Alexander Kellett &lt;l&#121;pan&#00111;&#0118;&#00064;kde&#46;&#111;rg&gt;</em></li>
    </ul>

    </li>
    <li><h2>Korundum / QtRuby</h2>
    <ul>
    	<li>Fairly complete Qt and KDE bindings for Ruby <em>Richard Dale &lt;Richard_Dale@tipitina.demon.co.uk&gt;, Alexander Kellett &lt;lypanov@kde.org&gt;</em></li>
    </ul>

    </li>
    <li><h2>Koala Java</h2>
    <ul>
    	<li>Remove the need for manual edits or patches in the generated bindings code <em>Richard Dale &lt;Richard_Dale@tipitina.demon.co.uk&gt;</em></li>
    	<li>Convert operator methods, with names such as op_plus() for '+' or op_write() for '&lt;&lt;' <em>Richard Dale &lt;Richard_Dale@tipitina.demon.co.uk&gt;</em></li>
    	<li>Add any friend functions, as static methods to the Qt or KDE classes. For instance, BarIconSet() as a static method
    KDE.BarIconSet(), and bitBlt() is Qt.bitBlt() <em>Richard Dale &lt;Richard_Dale@tipitina.demon.co.uk&gt;</em></li>
    	<li>For C++ methods with default args, generate all the possible java methods,
    not just the variants with the least, and the most args <em>Richard Dale &lt;Richard_Dale@tipitina.demon.co.uk&gt;</em></li>
    	<li>Add new classes such as KMdi*. Remove obsolete classes <em>Richard Dale &lt;Richard_Dale@tipitina.demon.co.uk&gt;</em></li>
    	<li>Convert non const string arguments to StringBuffers, and leave ordinary immutable args as Strings <em>Richard Dale &lt;Richard_Dale@tipitina.demon.co.uk&gt;</em></li>
    	<li>Generate the correct code for boolean[] and int[] arg types <em>Richard Dale &lt;Richard_Dale@tipitina.demon.co.uk&gt;</em></li>
    </ul>

    </li>
    </ul>

<h2>Utilities</h2>
	<ul>
	<li><h2>KRegExpEditor</h2>
	<ul>
		<li>Create an application on its own from KRegExpEditor (several users
          have requested this) <em>Jesper K. Pedersen &lt;blac&#x6b;&#x69;e&#x40;&#107;&#100;&#101;&#x2e;o&#00114;&#00103;&gt;</em></li>
		<li> Add a verifier window where user can try his regular expression <em>Jesper K. Pedersen &lt;bla&#x63;ki&#x65;&#x40;&#107;de.org&gt;</em></li>
		<li> Add some warnings on invalid regular expressions (like $^) <em>Jesper K. Pedersen &lt;b&#108;a&#x63;k&#0105;&#101;&#x40;&#107;de.&#111;&#x72;&#x67;&gt;</em></li>
		<li> Ensure predefined regular expressions are correct i18n'ed <em>Jesper K. Pedersen &lt;bl&#97;ck&#x69;&#101;&#0064;&#00107;&#100;&#x65;.&#x6f;&#x72;&#x67;&gt;</em></li>
		<li>Add support for Emacs style Regular Expression
        syntaxes <em>Jesper K. Pedersen &lt;&#98;l&#x61;&#x63;ki&#00101;&#x40;k&#x64;e&#046;&#00111;r&#00103;&gt;</em></li>
	</ul>

    </li>
    <li><h2>KJots</h2>
    <ul>
    	<li>XMLUI-ify and improve the interface. Using a treeview for books and bookmarks for quick nav. <em>Aaron J. Seigo &lt;&#097;&#00115;&#101;&#105;go&#x40;&#x6b;d&#101;.&#x6f;&#x72;&#x67;&gt;</em></li>
    	<li>Printing and viewing of entire books. <em>Aaron J. Seigo &lt;ase&#x69;go&#00064;&#x6b;&#100;e.o&#114;&#0103;&gt;</em></li>
    	<li>Autosave. <em>Aaron J. Seigo &lt;a&#x73;&#101;&#0105;go&#x40;&#107;&#x64;e&#46;org&gt;</em></li>
    </ul>

    </li>
    <li><h2>KMilo</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> A generic framework to support special keyboards
        and hardware features, such as those found on laptops. <em>George Staikos &lt;&#x73;tai&#x6b;o&#115;&#064;&#107;&#x64;&#0101;&#46;o&#114;&#x67;&gt;</em></li>
    	<li>KMilo support for PowerBooks via pbbuttonsd. <em>George Staikos &lt;st&#x61;&#x69;kos&#00064;&#0107;&#00100;&#x65;&#46;o&#114;g&gt;</em></li>
    </ul>

    </li>
    <li><h2>KLaptop</h2>
    <ul>
    	<li>Full Linux ACPI support (suspend etc, throttling, performance profiles) <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Back panel brightness support for some laptops (Sony, Toshiba) <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Recode to run as a KDED daemon <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Better Thinkpad support <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Triggers from lid closure (suspend, powerdown etc) - on laptops with ACPI support <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Sony laptop extensions support (/dev/sonypi - scroll wheel, panel brightness) <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Added the ability to auto-disable suspend etc when the load average is high <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Display the state of individual batteries in ACPI systems <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Support for Linux kernel software suspend <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Added battery charged notification dialog <em>Paul Campbell &lt;paul@taniwha.com&gt;</em></li>
    	<li>Option to use blank screensaver when in battery mode <em>George Staikos &lt;s&#116;ai&#x6b;o&#x73;&#64;kde.org&gt;</em></li>
    	<li>Support for the Linux CPUFreq interface. <em>Volker Krause &lt;volker.krause@rwth-aachen.de&gt;</em></li>
    </ul>

    </li>
    <li><h2>Kgpg</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> frontend for gpg. <em>Jean-Baptiste Mardelle &lt;bj@altern.org&gt;</em></li>
    	<li>Key editing in GUI (changing passphrase, trust, expiration) <em>&lt;bj@altern.org&gt;</em></li>
    	<li>Support for key groups <em>&lt;bj@altern.org&gt;</em></li>
    	<li>Speed up key manager <em>&lt;bj@altern.org&gt;</em></li>
    	<li>Gpg-agent support <em>&lt;bj@altern.org&gt;</em></li>
    	<li>Auto import missing signatures <em>&lt;bj@altern.org&gt;</em></li>
    	<li>improved editor (print, spell check, ...) <em>&lt;bj@altern.org&gt;</em></li>
    	<li>Manage photo id's <em>&lt;bj@altern.org&gt;</em></li>
    	<li>Revocation certificates (print, save) <em>&lt;bj@altern.org&gt;</em></li>
    	<li>Change dialogs to KBaseDialog for ui consistency <em>&lt;bj@altern.org&gt;</em></li>
    	<li>Implement the missing key editing commands in the GUI (adduid, deluid,..) <em>&lt;bj@altern.org&gt;</em></li>
    	<li>When encrypting a folder, give a choice of compression scheme (zip, tar,...) <em>&lt;bj@altern.org&gt;</em></li>
    </ul>

    </li>
    <li><h2>KHexEdit2</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> A hex edit widget library <em>Friedrich W. H. Kossebau &lt;Friedrich.W.H@Kossebau.de&gt;</em></li>
    	<li>move the lib of the new KHexEdit2 widget from kdenonbeta over to kdeutils/khexedit/lib <em>Friedrich W. H. Kossebau &lt;Friedrich.W.H@Kossebau.de&gt;</em></li>
    	<li>implement interfaces for the KBytesEdit widget part <em>Friedrich W. H. Kossebau &lt;Friedrich.W.H@Kossebau.de&gt;</em></li>
    </ul>

    </li>
    </ul>

<h2>Quanta</h2>
	<ul>
	<li><h2>Quanta Plus</h2>
	<ul>
		<li>Basic VPL (WYSIWYG) mode <em>Nicolas Deschildre &lt;nicolasdchd@ifrance.com&gt;</em></li>
		<li>Project documentation handling <em>Andras Mantia &lt;am&#97;&#110;tia&#x40;kde&#46;or&#x67;&gt;</em></li>
		<li>Enhanced editing and autocompletion (user tags, user PHP functions, CSS selector classes, autofilling of mandatory child tags, etc.) <em>Andras Mantia &lt;&#x61;&#109;an&#116;ia&#00064;k&#100;&#0101;&#0046;o&#x72;g&gt;</em></li>
		<li>Frame wizard <em>Luciano Gulmini &lt;e.gulmini@tiscali.it&gt;</em></li>
		<li>New CSS wizard <em>Luciano Gulmini &lt;e.gulmini@tiscali.it&gt;</em></li>
		<li>Abbreviation support <em>Andras Mantia &lt;&#x61;&#x6d;&#00097;&#x6e;t&#105;&#x61;&#00064;kde&#0046;&#111;&#0114;&#103;&gt;</em></li>
		<li>Create DTEPs from real DTD files <em>Andras Mantia &lt;am&#x61;nti&#x61;&#0064;kde&#x2e;&#x6f;rg&gt;</em></li>
		<li>Filter the templates through an action <em>Andras Mantia &lt;a&#00109;&#x61;&#00110;&#x74;i&#x61;&#64;&#107;de.o&#x72;&#103;&gt;</em></li>
		<li>Offline page parsing <em>Andras Mantia &lt;&#097;&#x6d;ant&#105;&#x61;&#64;kde.o&#00114;&#103;&gt;</em></li>
		<li>Attribute tree view (another, easy way to edit tag attributes) <em>Andras Mantia &lt;&#x61;&#x6d;&#0097;&#0110;&#x74;ia&#x40;&#107;&#x64;&#x65;.&#0111;&#114;g&gt;</em></li>
		<li>Modify the user toolbars and actions from a context menu <em>Andras Mantia &lt;a&#x6d;&#x61;ntia&#x40;&#107;&#x64;e.or&#00103;&gt;</em></li>
		<li>Visual problem reporter <em>Andras Mantia &lt;&#x61;&#x6d;&#00097;nti&#x61;&#00064;kde&#x2e;o&#114;g&gt;</em></li>
		<li>Tip dialog <em>Eric Laffoon &lt;&#115;equit&#x75;r&#x40;&#107;d&#101;&#46;o&#x72;g&gt;</em></li>
		<li>Kommander based Quick Start dialog </li>
		<li>Crash recovery <em>Emiliano Gulmini &lt;emi_barbarossa@yahoo.it&gt;</em></li>
		<li>Script management facility <em>Eric Laffoon &lt;s&#101;&#113;ui&#x74;u&#114;&#64;&#00107;&#100;e.&#00111;&#114;&#103;&gt;</em></li>
		<li>XML Tools <em>Chris Hornbaker &lt;chrishornbaker@earthlink.net&gt;</em></li>
		<li>integrate Keith Isdale's kxsldbg  <em>Chris Hornbaker &lt;chrishornbaker@earthlink.net&gt;</em></li>
		<li>HTML table editor <em>Andras Mantia &lt;a&#x6d;an&#x74;i&#97;&#064;kd&#x65;&#0046;org&gt;</em></li>
		<li>KFileReplace integration <em>Andras Mantia &lt;am&#x61;&#110;&#116;i&#097;&#x40;&#107;d&#x65;&#46;org&gt;</em></li>
		<li>tag file edit mode <em>Eric Laffoon &lt;&#x73;eq&#00117;&#105;&#x74;&#117;&#114;&#x40;&#107;d&#x65;&#x2e;&#0111;&#114;g&gt;</em></li>
		<li>Loading of new DTEP packages without restarting and sending of DTEP packages via e-mail. <em>Andras Mantia &lt;&#00097;m&#x61;n&#x74;i&#x61;&#64;&#x6b;&#100;e&#46;o&#x72;&#x67;&gt;</em></li>
		<li>Enhanced DCOP, script action and plugin support. <em>Andras Mantia &lt;am&#97;&#x6e;t&#00105;&#097;&#64;kde&#00046;o&#x72;&#x67;&gt;</em></li>
		<li>Function to convert the tag and attribute cases in a document. <em>Andras Mantia &lt;a&#x6d;a&#110;&#x74;ia&#64;k&#x64;e.or&#103;&gt;</em></li>
		<li>User extensible preview in external browser. By default Konqueror, Mozilla, Netscape, Opera and Lynx is supported. <em>Andras Mantia &lt;&#97;m&#x61;n&#x74;i&#x61;&#x40;k&#0100;e.&#111;rg&gt;</em></li>
		<li>Enhanced document management: close buttons on tabs, close other tabs, reload functionality. <em>Andras Mantia &lt;am&#0097;&#110;t&#105;a&#x40;k&#100;&#101;&#46;&#0111;rg&gt;</em></li>
		<li>New Kommander based Quick Start dialog for (X)HTML. <em>Andras Mantia &lt;am&#x61;nti&#x61;&#64;k&#00100;&#101;&#046;o&#x72;g&gt;</em></li>
		<li>Support for previewing the &quot;noframes&quot; section of a document. <em>Andras Mantia &lt;a&#00109;&#097;nt&#105;&#x61;&#64;k&#0100;e.&#x6f;r&#x67;&gt;</em></li>
	</ul>

    </li>
    </ul>

<h2>KDE Accessibility</h2>
	<ul>
	<li><h2>KMagnifier</h2>
	<ul>
		<li><strong>NEW IN KDE:</strong> A screen magnifier <em></em></li>
		<li>GUI improvements: Optional menu and toolbars, popup menu <em>Olaf Jan Schmidt &lt;&#111;&#0106;&#115;ch&#109;i&#100;&#00116;&#00064;&#x6b;d&#00101;.&#x6f;&#114;&#103;&gt;, Gunnar Schmi Dt &lt;gunnar@schmi-dt.de&gt;</em></li>
		<li>Speed improvements <em>Olaf Jan Schmidt &lt;oj&#115;&#x63;&#00104;&#0109;i&#100;&#116;&#64;k&#100;e&#046;&#111;r&#x67;&gt;</em></li>
	</ul>

    </li>
    <li><h2>KMouseTool</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> Mouse click tool for people with carpal tunnel syndrome <em></em></li>
    	<li>Make KMouseTool dock into the system tray <em>Gunnar Schmi Dt &lt;gunnar@schmi-dt.de&gt;</em></li>
    	<li>GUI improvements <em>Olaf Jan Schmidt &lt;&#111;&#106;&#x73;&#x63;h&#109;id&#x74;&#0064;&#107;de&#x2e;&#x6f;rg&gt;</em></li>
    </ul>

    </li>
    <li><h2>KMouth</h2>
    <ul>
    	<li><strong>NEW IN KDE:</strong> Tool to create sentences for speech synthesizer <em></em></li>
    	<li>Word completion <em>Gunnar Schmi Dt &lt;gunnar@schmi-dt.de&gt;</em></li>
    	<li>Support for multiple languages <em>Gunnar Schmi Dt &lt;gunnar@schmi-dt.de&gt;</em></li>
    </ul>

    </li>
    </ul>