------------------------------------------------------------------------
r1168848 | gladhorn | 2010-08-28 03:26:41 +1200 (Sat, 28 Aug 2010) | 5 lines

Plain Qt applications do not have KGlobal available unless we create a KComponentData.
This fixes the crashes that non-kde applications were experiencing when loading the kde plugin for libattica.

backport r1168847 

------------------------------------------------------------------------
r1168900 | cfeck | 2010-08-28 07:44:20 +1200 (Sat, 28 Aug 2010) | 2 lines

Reset filter model after populating it (backport r1168897)

------------------------------------------------------------------------
r1168925 | mlaurent | 2010-08-28 10:06:30 +1200 (Sat, 28 Aug 2010) | 2 lines

backport: fix compile with qt4.7

------------------------------------------------------------------------
r1168977 | scripty | 2010-08-28 14:25:51 +1200 (Sat, 28 Aug 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1169040 | graesslin | 2010-08-28 20:06:34 +1200 (Sat, 28 Aug 2010) | 7 lines

Add sanity check to DesktopGrid in case of pager layout and only one desktop.

Pager reported two rows, which does not make sense and caused the grid to be in
an inconsistent state.
CCBUG: 248817


------------------------------------------------------------------------
r1169042 | graesslin | 2010-08-28 20:15:27 +1200 (Sat, 28 Aug 2010) | 5 lines

Do not drop desktop on non-existing desktops in the desktop grid.
BUG: 248817
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1169047 | graesslin | 2010-08-28 20:41:31 +1200 (Sat, 28 Aug 2010) | 11 lines

Block keyboard input during animations in desktop grid.

This works around crashers if a key is pressed directly after activating the effect.
Working around is not the perfect solution, but we already block mouse events, so it
is consistent. And unfortunately the effect has become difficult to maintain and I
fear it's like chasing one crash after the other if we allow user interaction during
the animations.
BUG: 244813
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1169059 | woebbe | 2010-08-28 21:53:51 +1200 (Sat, 28 Aug 2010) | 4 lines

don't crash in save() after deleting domain policies

deletePressed(): keep the map in sync with the tree view

------------------------------------------------------------------------
r1169086 | graesslin | 2010-08-28 23:58:18 +1200 (Sat, 28 Aug 2010) | 11 lines

Update text shadow information whenever Tabs are added/removed in Aurorae.

Fixes incorrect initial text shadow position in Aurorae. Unfortunately
QGraphicsDropShadowEffect seems to be not side effect free. Updates to the scene
are not propagated leaving the tabs in a wrong visual state. Therefore text shadow
is disabled for window tabbing. I will investigate further to see if it gets fixed
in Qt 4.7.
BUG: 248754
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1169145 | graesslin | 2010-08-29 02:12:24 +1200 (Sun, 29 Aug 2010) | 5 lines

Drop keyboard events while moving windows in desktop grid.
BUG: 249325
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1169158 | graesslin | 2010-08-29 02:38:02 +1200 (Sun, 29 Aug 2010) | 9 lines

Exclude all transformed windows from blur effect.

Currently only scaled or translated windows were excluded,
but it is possible to also have windows with just changed
window quads (e.g. magic lamp).
BUG: 243693
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1169323 | graesslin | 2010-08-29 17:44:54 +1200 (Sun, 29 Aug 2010) | 9 lines

Allow moving of "unmovable" windows if present windows mode is used in desktop grid.

Allows to move a window with a fixed position to another desktop. This is only 
available in present windows mode as in normal mode each mouse movement results 
in a window movement and therefore not possible.
BUG: 249337
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1169432 | graesslin | 2010-08-29 20:42:39 +1200 (Sun, 29 Aug 2010) | 12 lines

Fix window movment regression (e.g. pack) in 4.5.

The regression was caused in tiling which set the geometry to a different one again.
Nikhil please verify that tiling is still working correctly. Looking at the code this
commit should be side-effect free. When coming from Client::move() moveResizeGeom and
initialMoveResizeGeom may not be set. So I think the code in notifyTilingWindowMove()
cannot work at all ;-)
CCMAIL: nsm.nikhil@gmail.com
BUG: 241049
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1169752 | scripty | 2010-08-30 14:23:12 +1200 (Mon, 30 Aug 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1170005 | asouza | 2010-08-31 02:29:26 +1200 (Tue, 31 Aug 2010) | 7 lines

backporting 1155751

actually push updates when the network updates; tested envcan thoroughly. the rest 
follow the same pattern, but need further testing
CCBUG:245639


------------------------------------------------------------------------
r1170006 | asouza | 2010-08-31 02:31:04 +1200 (Tue, 31 Aug 2010) | 9 lines

backporting 1169254

First step to fix the network bug

We should connect to the ions anyway, even if network is down.
This way, when we reset the ions later they will have a way to
notify the objects connected to them that they are in the game
again.

------------------------------------------------------------------------
r1170007 | asouza | 2010-08-31 02:33:10 +1200 (Tue, 31 Aug 2010) | 4 lines

backporting 1169265

SVN_SILENT Remove unused variables

------------------------------------------------------------------------
r1170021 | asouza | 2010-08-31 03:06:22 +1200 (Tue, 31 Aug 2010) | 7 lines

backporting 1155751

actually push updates when the network updates; tested envcan thoroughly. the rest 
follow the same pattern, but need further testing
CCBUG:245639


------------------------------------------------------------------------
r1170042 | gkiagia | 2010-08-31 04:04:33 +1200 (Tue, 31 Aug 2010) | 2 lines

Backport r1170040: Use sane permissions for kcmclockrc (644 instead of 004).

------------------------------------------------------------------------
r1170055 | asouza | 2010-08-31 04:33:02 +1200 (Tue, 31 Aug 2010) | 19 lines

backporting r1169287

Make weather dataengine work if started without network

Always call setData() using the original source so it's initialized
and when we reset the ions, they have sources connected to them and are
able to communicate back.

Also, fix the ions to update their data into the weather engine and later
emit the signal that will make the engine force the clients (plasmoids) to
update using the new data.

This mostly fix the problem of turning the computer on without network,
getting network later and the weather plasmoids not updating.

CCBUG: 226022
CCBUG: 245639


------------------------------------------------------------------------
r1170094 | aseigo | 2010-08-31 06:44:29 +1200 (Tue, 31 Aug 2010) | 3 lines

need to update all processes first
BUG:249206

------------------------------------------------------------------------
r1170162 | cfeck | 2010-08-31 11:48:58 +1200 (Tue, 31 Aug 2010) | 5 lines

Limit number of profiles that are visible (backport r1162174 and r1170157)

FIXED-IN: 4.5.2
BUG: 219873

------------------------------------------------------------------------
r1170172 | rysin | 2010-08-31 12:26:42 +1200 (Tue, 31 Aug 2010) | 1 line

improve keyboard hardware settings code (try at BUG 248250)
------------------------------------------------------------------------
r1170189 | rysin | 2010-08-31 14:15:39 +1200 (Tue, 31 Aug 2010) | 4 lines

Allow kcm_keyboard to start even if rule files not found
BUG: 248096
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1170192 | rysin | 2010-08-31 14:30:16 +1200 (Tue, 31 Aug 2010) | 4 lines

Don't crash if we can't find the xkb option info
BUG: 244913
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1170365 | dfaure | 2010-09-01 05:46:22 +1200 (Wed, 01 Sep 2010) | 2 lines

Fix "Malformed URL" error when reloading the initial about:konqueror page.

------------------------------------------------------------------------
r1170446 | aseigo | 2010-09-01 10:59:11 +1200 (Wed, 01 Sep 2010) | 3 lines

backport fixes for:
CCBUG:241332

------------------------------------------------------------------------
r1170605 | trueg | 2010-09-02 03:54:41 +1200 (Thu, 02 Sep 2010) | 1 line

Properly delete open connections on shutdown
------------------------------------------------------------------------
r1170665 | lueck | 2010-09-02 07:29:12 +1200 (Thu, 02 Sep 2010) | 1 line

doc backport for 4.5.2
------------------------------------------------------------------------
r1170765 | hpereiradacosta | 2010-09-02 13:04:02 +1200 (Thu, 02 Sep 2010) | 4 lines

backport r1170764
Try reduce the number of paintEvents sent due to state change.


------------------------------------------------------------------------
r1170767 | hpereiradacosta | 2010-09-02 13:29:20 +1200 (Thu, 02 Sep 2010) | 5 lines

backport r1170765
Reduced size of the frame shadow widgets, notably to avoid overlaps with other widget (e.g. scrollbars), and thus
reduce the number of repaints.
CCBUG: 245740

------------------------------------------------------------------------
r1170772 | hpereiradacosta | 2010-09-02 13:44:18 +1200 (Thu, 02 Sep 2010) | 3 lines

Backport r1170771
Fixed 'changed' flag in updateState. (previous was missing some updates)

------------------------------------------------------------------------
r1170776 | hpereiradacosta | 2010-09-02 14:01:01 +1200 (Thu, 02 Sep 2010) | 4 lines

backport r1170775
More reduction of frame shadow widget's size.
CCBUG: 245740

------------------------------------------------------------------------
r1170779 | scripty | 2010-09-02 15:17:28 +1200 (Thu, 02 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1171005 | lueck | 2010-09-03 01:28:43 +1200 (Fri, 03 Sep 2010) | 1 line

backport from trunk: screenshot update
------------------------------------------------------------------------
r1171046 | hpereiradacosta | 2010-09-03 02:26:59 +1200 (Fri, 03 Sep 2010) | 5 lines

backport r1171043
Explicitely 'show' shadow after reparented.
This notably fixes the fact that shadows would not be visible when re-created after 
're-polishing' the style.

------------------------------------------------------------------------
r1171079 | hpereiradacosta | 2010-09-03 04:35:43 +1200 (Fri, 03 Sep 2010) | 4 lines

backport r1171078
Really disable caching when caching is disabled (hehe)


------------------------------------------------------------------------
r1171123 | whiting | 2010-09-03 07:35:52 +1200 (Fri, 03 Sep 2010) | 4 lines

Backport bugfix to 4.5 branch.
BUG: 153056
BUG: 233436

------------------------------------------------------------------------
r1171145 | hpereiradacosta | 2010-09-03 09:36:04 +1200 (Fri, 03 Sep 2010) | 3 lines

Always draw focus indicator in AbstractItemViews, when relevant option is ON.
BUG: 242158

------------------------------------------------------------------------
r1171362 | hpereiradacosta | 2010-09-04 02:32:39 +1200 (Sat, 04 Sep 2010) | 7 lines

backport r1171358
Fixed arithmetic exception when calculating pixmaps size if horizontal (or 
vertical) borders are zero. (this is the case notably for the 'selection' 
tileset

CCMAIL: Woebbeking@kde.org

------------------------------------------------------------------------
r1171484 | hpereiradacosta | 2010-09-04 11:49:00 +1200 (Sat, 04 Sep 2010) | 2 lines

replaced "delete animationData" by "animationData->deleteLater()" when a target is unregistered.

------------------------------------------------------------------------
r1171568 | ppenz | 2010-09-05 01:20:23 +1200 (Sun, 05 Sep 2010) | 6 lines

Fix issue that an old preview might be shown in a tooltip. Cool bug-id btw ;-)

BUG: 250000
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1171651 | aseigo | 2010-09-05 07:08:05 +1200 (Sun, 05 Sep 2010) | 3 lines

jobs need to be explicitly destroyed; not overly fond of that design decision, but this works and doesn't need to be generically usable
BUG:250034

------------------------------------------------------------------------
r1171791 | chani | 2010-09-05 18:52:12 +1200 (Sun, 05 Sep 2010) | 6 lines

backport r1171790:
ensure views are properly set active/inactive when they're created

now the "setup mode" button in the screensaver KCM will
work again. :)

------------------------------------------------------------------------
r1171793 | aseigo | 2010-09-05 19:17:53 +1200 (Sun, 05 Sep 2010) | 2 lines

allow iconviews to be configured as to wether folder drill-down should be triggered by click or hover; this is use to allow the drill down to be started with a mouse click and then further drill down to be on hover only. in 4.6 we'll include a config option to make this happen on the top level as well.

------------------------------------------------------------------------
r1171800 | chani | 2010-09-05 20:05:18 +1200 (Sun, 05 Sep 2010) | 3 lines

backport r1171798:
setHasConfigurationInterface(true);

------------------------------------------------------------------------
r1172095 | mlaurent | 2010-09-07 00:46:53 +1200 (Tue, 07 Sep 2010) | 2 lines

Fix mem leak

------------------------------------------------------------------------
r1172103 | mlaurent | 2010-09-07 01:07:46 +1200 (Tue, 07 Sep 2010) | 2 lines

Backport: use const+ref

------------------------------------------------------------------------
r1172105 | mlaurent | 2010-09-07 01:13:09 +1200 (Tue, 07 Sep 2010) | 2 lines

backport Use const+ref

------------------------------------------------------------------------
r1172167 | mlaurent | 2010-09-07 04:09:20 +1200 (Tue, 07 Sep 2010) | 2 lines

Backport: fix mem leal

------------------------------------------------------------------------
r1172224 | aseigo | 2010-09-07 06:58:20 +1200 (Tue, 07 Sep 2010) | 3 lines

fix resize for floating mode
BUG:242516

------------------------------------------------------------------------
r1172366 | aseigo | 2010-09-07 17:29:35 +1200 (Tue, 07 Sep 2010) | 3 lines

check ptr value
BUG:245539

------------------------------------------------------------------------
r1172466 | trueg | 2010-09-07 22:19:49 +1200 (Tue, 07 Sep 2010) | 1 line

Backport: Fixed DBus cleanup: Do only watch the service in question instead of all services.
------------------------------------------------------------------------
r1172469 | trueg | 2010-09-07 22:21:31 +1200 (Tue, 07 Sep 2010) | 1 line

Backport: Fixed DBus cleanup: Do only watch the service in question instead of all services.
------------------------------------------------------------------------
r1172866 | scripty | 2010-09-08 15:19:36 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1172946 | mlaurent | 2010-09-08 21:16:07 +1200 (Wed, 08 Sep 2010) | 2 lines

Backport: not necessary

------------------------------------------------------------------------
r1172962 | mlaurent | 2010-09-08 21:43:33 +1200 (Wed, 08 Sep 2010) | 3 lines

Backport: Change boolean before to return otherwise it will never change.


------------------------------------------------------------------------
r1173079 | trueg | 2010-09-09 03:30:14 +1200 (Thu, 09 Sep 2010) | 1 line

Backport: Set a connection since the watcher does not use the session bus by default
------------------------------------------------------------------------
r1173245 | scripty | 2010-09-09 14:45:54 +1200 (Thu, 09 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173431 | trueg | 2010-09-09 22:34:49 +1200 (Thu, 09 Sep 2010) | 1 line

Backport: Force the number of server threads to 100 instead of relying on the Soprano default which is 10 (although the docu states it should be 100)
------------------------------------------------------------------------
r1173483 | trueg | 2010-09-10 02:13:43 +1200 (Fri, 10 Sep 2010) | 1 line

Backport: build without Nepomuk
------------------------------------------------------------------------
r1173503 | annma | 2010-09-10 03:15:22 +1200 (Fri, 10 Sep 2010) | 3 lines

fix Recently Used applications kept between sessions
BUG=237033

------------------------------------------------------------------------
r1173626 | aseigo | 2010-09-10 09:24:31 +1200 (Fri, 10 Sep 2010) | 5 lines

* reinstate the hide-the-controller-when-its-our-own-graphicswidget-and-we-lose-appropriate-focus feature
* delete our own graphicswidgets if they are replaced with others
* remove an unused variable
BUG:250395

------------------------------------------------------------------------
r1173668 | hindenburg | 2010-09-10 14:31:33 +1200 (Fri, 10 Sep 2010) | 6 lines

Change the way tab width is determined by using min/max stylesheet.

BUG: 166573
BUG: 189847
BUG: 157201

------------------------------------------------------------------------
r1173669 | hindenburg | 2010-09-10 14:34:03 +1200 (Fri, 10 Sep 2010) | 1 line

update version
------------------------------------------------------------------------
r1173673 | scripty | 2010-09-10 14:44:17 +1200 (Fri, 10 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173678 | hindenburg | 2010-09-10 15:32:22 +1200 (Fri, 10 Sep 2010) | 4 lines

Dragonfly patch to build - patch by Alex Hornung

CCBUG: 247626

------------------------------------------------------------------------
r1173946 | ginkel | 2010-09-11 10:43:04 +1200 (Sat, 11 Sep 2010) | 5 lines

wetter.com Ion: fix forecast retrieval for some geographic regions

BUG: 236309
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1173952 | sgielen | 2010-09-11 11:19:25 +1200 (Sat, 11 Sep 2010) | 4 lines

Backport of r1173950:
Reviewboard (svn) entry 5229
This patch adds a HtmlEntityResolver class and makes the XML stripper use it to resolve additional entities. The &nbsp; which would have caused an XML error, is now turned into a regular space, fixing the problem. (Any HTML entities unknown to KCharset cause a warning and are replaced with an empty character.)

------------------------------------------------------------------------
r1173955 | hpereiradacosta | 2010-09-11 11:27:00 +1200 (Sat, 11 Sep 2010) | 1 line

removed unused 'aiv' variable
------------------------------------------------------------------------
r1173981 | aseigo | 2010-09-11 13:14:17 +1200 (Sat, 11 Sep 2010) | 2 lines

don't crash when removing activities: ActivityIcon was calling destroy on the Activity object in its mouse event, which can then lead to a quick (and crash inducing) deletion of the item while it is still in its mouse event. so we take two precautions: call destroy using a single shot timer, and clear the Activity* in ActivityIcon when it is removed

------------------------------------------------------------------------
r1173984 | aseigo | 2010-09-11 13:26:44 +1200 (Sat, 11 Sep 2010) | 2 lines

remove the stored file on activity removal

------------------------------------------------------------------------
r1174003 | hein | 2010-09-11 18:09:03 +1200 (Sat, 11 Sep 2010) | 2 lines

Backport 1168744, signed off by Ben.

------------------------------------------------------------------------
r1174202 | mlaurent | 2010-09-12 01:18:55 +1200 (Sun, 12 Sep 2010) | 3 lines

Backport: Fix #250769
BUG: 250769

------------------------------------------------------------------------
r1174209 | rysin | 2010-09-12 01:35:44 +1200 (Sun, 12 Sep 2010) | 1 line

don't set the keyboard model if it already has right value
------------------------------------------------------------------------
r1174243 | rysin | 2010-09-12 04:33:32 +1200 (Sun, 12 Sep 2010) | 1 line

use numlockx code to reliably set numlock
------------------------------------------------------------------------
r1174286 | ossi | 2010-09-12 08:02:54 +1200 (Sun, 12 Sep 2010) | 8 lines

fix timed login

the async-ification broke it. need to enable the socket listener after
sending the auto-login command.

BUG: 229475


------------------------------------------------------------------------
r1174332 | hpereiradacosta | 2010-09-12 12:36:53 +1200 (Sun, 12 Sep 2010) | 14 lines

Backport r1173689

Added a "steps" parameter to the animations, that limits the maximum number of different 
values a given animation can take. This should effectively reduce the size of the caches by 
a factor 10 to 20, improve performances, without any noticeable differences to the eye, and 
possibly fix some of the performance issues experienced by some users with NVidia graphic 
cards. 

The number of steps is configurable (as an hidden option) and is still to be optimized. 

Current value is set to 10. (was 256 in the past)

CCBUG: 242653

------------------------------------------------------------------------
r1174342 | abryant | 2010-09-12 14:08:56 +1200 (Sun, 12 Sep 2010) | 2 lines

Make read/writeGlobalConfig() actually use the global config.

------------------------------------------------------------------------
r1174413 | gkiagia | 2010-09-12 22:28:29 +1200 (Sun, 12 Sep 2010) | 6 lines

Remove the kmail2 mapping from the 4.5 branch.
This is because kmail 1.x is still the recommended version for 4.5.
Of course, if someone uses kmail2 with kde 4.5, this will report
crashes to the wrong product, but we can't do much for it...
CCBUG: 250553

------------------------------------------------------------------------
r1174604 | aseigo | 2010-09-13 10:22:43 +1200 (Mon, 13 Sep 2010) | 3 lines

if we delay the query, but then we aren't actually running the query, and we have a delayed run in the pipe, try it again
BUG:250582

------------------------------------------------------------------------
r1174636 | scripty | 2010-09-13 14:36:26 +1200 (Mon, 13 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1174717 | trueg | 2010-09-13 19:54:20 +1200 (Mon, 13 Sep 2010) | 1 line

Backport: make all Nepomuk services run with low CPU and IO scheduling priority.
------------------------------------------------------------------------
r1175024 | fredrik | 2010-09-14 10:12:04 +1200 (Tue, 14 Sep 2010) | 16 lines

Backport r1175021:

Make the lanczos shader use a fixed number of iterations in the loop.

This makes it possible for the GLSL compiler to unroll it, which also
avoids the need to use relative addressing. With this change the shader
should hopefully work with the R300G driver.

The unused kernel weights are set to zero so they don't contribute
to the end result.

Thanks to Tom Stellard and Marek Olšák for their suggestions on how
to solve this problem.

CCBUG: 243191

------------------------------------------------------------------------
r1175029 | aseigo | 2010-09-14 11:09:56 +1200 (Tue, 14 Sep 2010) | 3 lines

remove an unused bit of bookkeeping; seems to cause a crash on some systems due to an XIO error (though i'd expect NETWinInfo to protect against such things? hm..)
BUG:251131

------------------------------------------------------------------------
r1175074 | scripty | 2010-09-14 15:02:16 +1200 (Tue, 14 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1175404 | scripty | 2010-09-15 14:40:01 +1200 (Wed, 15 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1175733 | aseigo | 2010-09-16 06:11:07 +1200 (Thu, 16 Sep 2010) | 3 lines

remove the access to kwininfo, that's the important bit, actually
CCBUG:251131

------------------------------------------------------------------------
r1175767 | ruberg | 2010-09-16 09:01:01 +1200 (Thu, 16 Sep 2010) | 3 lines

BUG #251138: Fixed by making the edit dialog in solid-actions-kcm non-modal


------------------------------------------------------------------------
r1175897 | scripty | 2010-09-16 15:07:57 +1200 (Thu, 16 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1176070 | hpereiradacosta | 2010-09-17 02:50:43 +1200 (Fri, 17 Sep 2010) | 4 lines

Backport r1176068
Delete transition widget in TranstionData destructor.
This fixes potential leak when changing widget style.

------------------------------------------------------------------------
r1176111 | graesslin | 2010-09-17 06:07:46 +1200 (Fri, 17 Sep 2010) | 5 lines

Backport rev 1176110: Disable Lanczos filter while moving windows in present windows.

Thanks to the movement the change of quality is hardly visible.


------------------------------------------------------------------------
r1176152 | aseigo | 2010-09-17 08:19:21 +1200 (Fri, 17 Sep 2010) | 3 lines

keep above when unhidden, keepbelow when hidden
CCBUG:251443

------------------------------------------------------------------------
r1176226 | rysin | 2010-09-17 15:58:20 +1200 (Fri, 17 Sep 2010) | 1 line

code cleanup
------------------------------------------------------------------------
r1176403 | rysin | 2010-09-18 03:21:26 +1200 (Sat, 18 Sep 2010) | 4 lines

Fix kded module name for service manager to update status properly
BUG: 251132
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1176438 | aseigo | 2010-09-18 06:11:52 +1200 (Sat, 18 Sep 2010) | 2 lines

make maps-n-hashes in maps-n-hashes work

------------------------------------------------------------------------
r1176859 | ruberg | 2010-09-19 10:29:11 +1200 (Sun, 19 Sep 2010) | 4 lines

BUG: 251153
Fixed crash. Applet is now rejecting a configuration without any visibile buttons.


------------------------------------------------------------------------
r1177290 | alexmerry | 2010-09-20 12:29:44 +1200 (Mon, 20 Sep 2010) | 5 lines

Backport r1177289: Fix updating of the source when it was connected to before being populated.

CCBUG: 251459


------------------------------------------------------------------------
r1177301 | scripty | 2010-09-20 14:47:04 +1200 (Mon, 20 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1177603 | aseigo | 2010-09-21 06:44:11 +1200 (Tue, 21 Sep 2010) | 3 lines

ensure the file items returned are valid
CCBUG:251842

------------------------------------------------------------------------
r1177626 | aseigo | 2010-09-21 06:58:47 +1200 (Tue, 21 Sep 2010) | 2 lines

ensure the item is valid before adding it

------------------------------------------------------------------------
r1177652 | lueck | 2010-09-21 07:45:46 +1200 (Tue, 21 Sep 2010) | 1 line

backport: change gui strings from ktts to Jovie
------------------------------------------------------------------------
r1177739 | scripty | 2010-09-21 15:38:16 +1200 (Tue, 21 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1177871 | rysin | 2010-09-22 02:42:43 +1200 (Wed, 22 Sep 2010) | 1 line

increase layout label shadow radius to 4
------------------------------------------------------------------------
r1177954 | ppenz | 2010-09-22 07:34:07 +1200 (Wed, 22 Sep 2010) | 5 lines

Backport of SVN commit 1177952: Update the title of the tab when closing the second view in the split-mode. Thanks to  
Enrique Alonso for analysing the root-cause! 

CCMAIL: enrique_alonso@igluk.com

------------------------------------------------------------------------
r1177977 | habacker | 2010-09-22 09:20:09 +1200 (Wed, 22 Sep 2010) | 1 line

backported 1177975 and 1177976
------------------------------------------------------------------------
r1177981 | habacker | 2010-09-22 09:27:28 +1200 (Wed, 22 Sep 2010) | 1 line

backported 1177979 and 1177980
------------------------------------------------------------------------
r1178358 | fredrik | 2010-09-23 08:14:39 +1200 (Thu, 23 Sep 2010) | 4 lines

Backport r1178357:
Don't set gl_TextCoord[0] in the vertex shader when the fragment
shader doesn't use it.

------------------------------------------------------------------------
r1178409 | abryant | 2010-09-23 12:08:01 +1200 (Thu, 23 Sep 2010) | 3 lines

Hide panel applet handles when their associated applet is destroyed.
Also, always check before using the QWeakPointer's value.

------------------------------------------------------------------------
r1178841 | scripty | 2010-09-24 14:43:09 +1200 (Fri, 24 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179205 | lueck | 2010-09-25 08:40:07 +1200 (Sat, 25 Sep 2010) | 5 lines

new documentation backported from trunk
this is the 'landing page' in case a doc was not found for whatever reason (no doc, doc not installed, bug in the help system etc) 
it explains several ways how to solve this issue or to get the requested help online
BUG:240679
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r1179269 | fredrik | 2010-09-25 12:12:39 +1200 (Sat, 25 Sep 2010) | 6 lines

Backport r1179268:
- Check if the FBO size will exceed GL_MAX_TEXTURE_SIZE in
  BlurEffect::supported().
- Don't announce support to clients if the shader failed to compile,
  or the FBO is invalid.

------------------------------------------------------------------------
r1179294 | scripty | 2010-09-25 14:47:14 +1200 (Sat, 25 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179522 | graesslin | 2010-09-26 07:49:46 +1300 (Sun, 26 Sep 2010) | 5 lines

Ensure electric borders are above effect input windows after blocking stacking order updates.
BUG: 200235
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1179569 | freininghaus | 2010-09-26 11:03:20 +1300 (Sun, 26 Sep 2010) | 10 lines

Use KStringHandler::preProcessWrap() when calculating the width of a
file name in DolphinFileItemDelegate. This makes sure that zero width
spaces (which prevent kerning) are added at the same places where
KFileItemDelegate adds them when actually rendering the name. Fixes
the problem that file names are elided and partly replaced by "..."
even though there is enough space for the full name.

BUG: 251121
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1179694 | scripty | 2010-09-26 16:12:11 +1300 (Sun, 26 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179865 | ossi | 2010-09-26 23:20:11 +1300 (Sun, 26 Sep 2010) | 8 lines

double default ServerTimeout

in times of virtualization and parallelized boots, 15 secs are not
enough anymore (again ...).
BUG: 252055
FIXED-IN: 4.5.2


------------------------------------------------------------------------
r1180116 | abryant | 2010-09-27 15:37:22 +1300 (Mon, 27 Sep 2010) | 4 lines

Backport the bugfix from r1180090 to the 4.5 branch:
Split grid resizing logic into its own method, and ensure that the applet is always resized when the grid is. 
CCBUG:250756

------------------------------------------------------------------------
r1180119 | scripty | 2010-09-27 15:53:46 +1300 (Mon, 27 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180449 | scripty | 2010-09-28 16:23:19 +1300 (Tue, 28 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180692 | ppenz | 2010-09-29 04:57:48 +1300 (Wed, 29 Sep 2010) | 5 lines

Fix issue that tooltip-labels might get clipped (this regression has been introduced by revision 1158057)

BUG: 241608
FIXED-IN: 4.5.2

------------------------------------------------------------------------
r1180764 | lueck | 2010-09-29 09:29:30 +1300 (Wed, 29 Sep 2010) | 1 line

backport from trunk r1180759 : fix broken anchor help invocation
------------------------------------------------------------------------
r1181259 | mart | 2010-10-01 03:28:54 +1300 (Fri, 01 Oct 2010) | 2 lines

backport fix to 252774

------------------------------------------------------------------------
r1181386 | mueller | 2010-10-01 10:12:57 +1300 (Fri, 01 Oct 2010) | 2 lines

bump version

------------------------------------------------------------------------
