------------------------------------------------------------------------
r1085152 | mlaurent | 2010-02-04 13:09:39 +0000 (Thu, 04 Feb 2010) | 3 lines

Backport:
use double click to select type of transport

------------------------------------------------------------------------
r1085284 | djarvie | 2010-02-04 19:55:26 +0000 (Thu, 04 Feb 2010) | 1 line

apidox
------------------------------------------------------------------------
r1085498 | tnyblom | 2010-02-05 11:21:14 +0000 (Fri, 05 Feb 2010) | 4 lines

Don't double escape "&".

BUG: 211128

------------------------------------------------------------------------
r1087104 | vkrause | 2010-02-08 14:47:58 +0000 (Mon, 08 Feb 2010) | 4 lines

Backport SVN commit 1087068 by vkrause from trunk:

Actually disable session management in all cases.

------------------------------------------------------------------------
r1087671 | mueller | 2010-02-09 12:25:53 +0000 (Tue, 09 Feb 2010) | 2 lines

require a somewhat recent akonadi version

------------------------------------------------------------------------
r1088400 | weilbach | 2010-02-10 18:32:53 +0000 (Wed, 10 Feb 2010) | 2 lines

Backport fix for 194357.

------------------------------------------------------------------------
r1089226 | skelly | 2010-02-12 18:14:53 +0000 (Fri, 12 Feb 2010) | 5 lines

Backport bugfix from r1089222

CCBUG: 226559


------------------------------------------------------------------------
r1092486 | smartins | 2010-02-19 02:20:12 +0000 (Fri, 19 Feb 2010) | 10 lines

Forward port r1092482 from e35 to branch 4.4:

Fixed recursOn( date ) to not always return true when date == dateStart().

If the incidence recurs at mondays and dtStart() is sunday then recursOn (some-sunday-date) should return false.

Now recursOn() doesn't contradict timesInInterval() which already didn't return dtStart as a recurring date.

kolab/issue4145 

------------------------------------------------------------------------
r1092906 | tmcguire | 2010-02-19 17:51:24 +0000 (Fri, 19 Feb 2010) | 5 lines

Backport r1073082 by tmcguire from trunk to the 4.4 branch:

Support nicknames as criterion


------------------------------------------------------------------------
r1092907 | tmcguire | 2010-02-19 17:54:25 +0000 (Fri, 19 Feb 2010) | 4 lines

Backport r1073113 by tmcguire from trunk to the 4.4 branch:

Make the nickname query case-insensitive

------------------------------------------------------------------------
r1092908 | tmcguire | 2010-02-19 17:57:04 +0000 (Fri, 19 Feb 2010) | 2 lines

bump version

------------------------------------------------------------------------
r1092920 | tmcguire | 2010-02-19 18:29:06 +0000 (Fri, 19 Feb 2010) | 3 lines

Lower kdelibs requirement
It builds fine with kdelibs 4.3.90, which I have installed.

------------------------------------------------------------------------
r1092921 | tmcguire | 2010-02-19 18:29:44 +0000 (Fri, 19 Feb 2010) | 9 lines

Backport r1092916 by tmcguire from trunk to the 4.4 branch:

Don't send STARTTLS when it is not advertised, even if we requested it (but not if
m_allowUnencrypted is set to false).

Thanks to Don Martio <donmartio at larkos dot de> for providing the solution.
CCBUG: 212951


------------------------------------------------------------------------
r1093420 | smartins | 2010-02-20 20:54:35 +0000 (Sat, 20 Feb 2010) | 4 lines

Revert r1092486.

I'll fix timesInInterval() instead.

------------------------------------------------------------------------
r1094467 | tmcguire | 2010-02-22 20:21:47 +0000 (Mon, 22 Feb 2010) | 16 lines

Backport r1094466 by tmcguire from trunk to the 4.4 branch:

Improve search performance for searches that don't have a LIMIT set.
- Only search in PersonContacts. The strigi file indexer creates ordinary Contact object, we
  don't want to search them.
- use DISTINCT
- use "graph ?g"

Thanks to Yury G. Kudryashov for his help on IRC.

The search speed was improved from 220 seconds to 20 milliseconds (!) here, when searching for
my own mail address.

CCBUG: 219687


------------------------------------------------------------------------
r1095361 | scripty | 2010-02-24 04:27:08 +0000 (Wed, 24 Feb 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1096085 | weilbach | 2010-02-25 19:04:34 +0000 (Thu, 25 Feb 2010) | 2 lines

Backport of small cleanups due to Mehrdads patch.

------------------------------------------------------------------------
r1096479 | mueller | 2010-02-26 18:25:09 +0000 (Fri, 26 Feb 2010) | 2 lines

KDE 4.4.1 versioning

------------------------------------------------------------------------
