2006-10-09 08:56 +0000 [r593832]  mueller

	* branches/KDE/3.5/kdeartwork/kwin-styles/smooth-blend/client/smoothblend.cc:
	  fix another &&-confusion bug

2006-10-09 14:18 +0000 [r593912]  mueller

	* branches/KDE/3.5/kdeartwork/styles/phase/phasestyle.cpp: no
	  global static objects libs.. fixes crashes

2006-10-20 16:09 +0000 [r597513]  mueller

	* branches/KDE/3.5/kdeartwork/styles/phase/phasestyle.h,
	  branches/KDE/3.5/kdeartwork/styles/phase/phasestyle.cpp: fix
	  crash in ksmserver due to my previous commit

2006-10-23 09:21 +0000 [r598339]  mueller

	* branches/KDE/3.5/kdeartwork/styles/phase/phasestyle.cpp: forgot
	  to remove debug output, thanks Maksim

2007-01-15 09:15 +0000 [r623692]  binner

	* branches/KDE/3.5/kdesdk/kdesdk.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/libkopete/kopeteversion.h,
	  branches/KDE/3.5/kdepim/kdepim.lsm,
	  branches/KDE/3.5/kdewebdev/quanta/quanta.lsm,
	  branches/KDE/3.5/kdeaccessibility/kdeaccessibility.lsm,
	  branches/arts/1.5/arts/arts.lsm,
	  branches/KDE/3.5/kdeadmin/kdeadmin.lsm,
	  branches/KDE/3.5/kdeaddons/kdeaddons.lsm,
	  branches/KDE/3.5/kdenetwork/kdenetwork.lsm,
	  branches/KDE/3.5/kdelibs/kdelibs.lsm,
	  branches/KDE/3.5/kdeartwork/kdeartwork.lsm,
	  branches/KDE/3.5/kdewebdev/VERSION,
	  branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm,
	  branches/KDE/3.5/kdebase/kdebase.lsm,
	  branches/KDE/3.5/kdenetwork/kopete/VERSION,
	  branches/KDE/3.5/kdewebdev/kdewebdev.lsm,
	  branches/KDE/3.5/kdegames/kdegames.lsm,
	  branches/KDE/3.5/kdeedu/kdeedu.lsm,
	  branches/KDE/3.5/kdebindings/kdebindings.lsm,
	  branches/KDE/3.5/kdetoys/kdetoys.lsm,
	  branches/KDE/3.5/kdegraphics/kdegraphics.lsm,
	  branches/KDE/3.5/kdeutils/kdeutils.lsm: increase version

