------------------------------------------------------------------------
r838187 | scripty | 2008-07-27 06:42:00 +0200 (Sun, 27 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r838528 | scripty | 2008-07-28 06:45:33 +0200 (Mon, 28 Jul 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r840455 | mueller | 2008-08-01 09:58:31 +0200 (Fri, 01 Aug 2008) | 1 line

fix linking
------------------------------------------------------------------------
r840666 | bettio | 2008-08-01 15:03:05 +0200 (Fri, 01 Aug 2008) | 2 lines

backported aboutToShow fix: see r840656.

------------------------------------------------------------------------
r842771 | aacid | 2008-08-05 23:34:42 +0200 (Tue, 05 Aug 2008) | 6 lines

Backport r842761 kioslave/trunk/KDE/kdemultimedia/kioslave/audiocd/audiocd.cpp:

Make it work(that is show tracks), if you change the device everytime, kcompactdisclib creates a new private CD each time breaking how wmanlib works

Attention: Someone with FreeBSD should test the #else part of #if defined(HAVE_CDDA_IOCTL_DEVICE) because probably something similar is needed

------------------------------------------------------------------------
r843708 | sitter | 2008-08-07 18:54:41 +0200 (Thu, 07 Aug 2008) | 3 lines

backport r843707
  add video/ogg as described in http://wiki.xiph.org/index.php/MIME_Types_and_File_Extensions

------------------------------------------------------------------------
r843840 | scripty | 2008-08-08 06:53:35 +0200 (Fri, 08 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r846740 | mpyne | 2008-08-14 02:56:39 +0200 (Thu, 14 Aug 2008) | 2 lines

Backport fix for late access to JuK global static object to KDE 4.1.

------------------------------------------------------------------------
r846742 | mpyne | 2008-08-14 02:58:22 +0200 (Thu, 14 Aug 2008) | 1 line

Bump version of KDE 4.1.1 version of JuK for eventual release
------------------------------------------------------------------------
r847714 | scripty | 2008-08-16 08:06:57 +0200 (Sat, 16 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r848555 | scripty | 2008-08-18 07:08:29 +0200 (Mon, 18 Aug 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r851611 | mpyne | 2008-08-24 07:56:21 +0200 (Sun, 24 Aug 2008) | 5 lines

Backport the simple (and yet effective) JuK startup speedups to KDE 4.1.  Avoiding searching an
MPEG file for an attached cover unnecessarily can be a big win, especially since
Q3ListView::pixmap() and ::compare() (called on startup for each PlaylistItem) would indirectly
lead to a cover search happening.

------------------------------------------------------------------------
r852960 | aacid | 2008-08-27 00:21:43 +0200 (Wed, 27 Aug 2008) | 4 lines

Backport r852955, fromRawData is static so it basically was not modifying the array at all.

Fixes assert on riping from cd

------------------------------------------------------------------------
