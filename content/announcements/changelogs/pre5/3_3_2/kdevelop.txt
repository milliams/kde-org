Dir: kdevelop
----------------------------
new version numbers
----------------------------
new version number
----------------------------
changelog

Alex



Dir: kdevelop/buildtools/custommakefiles
----------------------------
-add a new tag <filelistdir>, which specifies in which directory the
foo.kdevelop.filelist is located, currently this is only used by the cmake
kdevelop3 project generator
-it doesn't add translatable strings
-it can't introduce new bugs
-it fixes the bug "the kdevelop3 custom makefile based project files
 do not work with cmake out-of-source builds"

Alex



Dir: kdevelop/buildtools/generic/buildsystem/shellscript/Attic
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp
----------------------------
backport fix for amd64 crash
----------------------------
Make it compile again.



Dir: kdevelop/languages/cpp/app_templates/chello
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/chello_gba
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/clanlib
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/cppcurseshello
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/cpphello
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/cppsdlhello
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/dcopservice
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/generichello
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/gnome2mmapp
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/gnomeapp
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/gtk2mmapp
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kapp
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kateplugin
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kbearimportfilter
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kbearplugin
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kcmodule
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kdedcop
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kdevlang
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kdevpart
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kdevpart2
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kfileplugin
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/khello2
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kicker
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kioslave
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kmod
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kofficepart
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard
----------------------------
BACKPORT Fix comment #3 of CCBUG: 83334



Dir: kdevelop/languages/cpp/app_templates/konqnavpanel
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kpartapp
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kpartplugin
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kscreensaver
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/kxt
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/noatunui
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/noatunvisual
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/opieapp
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/opieapplet
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/opieinput
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/opiemenu
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/opienet
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/opietoday
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/prc-tool
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/qmakeapp
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/qmakesimple
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/qtopiaapp
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/win32gui
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/cpp/app_templates/win32hello
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/languages/perl
----------------------------
Backport:
Don't require a whitespace between function name and the opening bracket



Dir: kdevelop/parts/appwizard
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/parts/appwizard/common
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/parts/appwizard/common/gnu
----------------------------
Patch by Lukáš Tinkl <lukas kde.org> that solves MANY issues in the new application wizard



Dir: kdevelop/parts/documentation
----------------------------
Fix the forkbomb created by KDevAssistant  when it was told to use
KDevAssistant to remotely open documentation...

BUG: 90334



Dir: kdevelop/parts/filecreate
----------------------------
Don't overwrite existing files

BUG: 90446



Dir: kdevelop/parts/filelist
----------------------------
-make the incremental qlistview search work in the filelist part

Alex



Dir: kdevelop/parts/fileselector
----------------------------
Backport fix mem leak
